require 'rails_helper'

RSpec.describe SystemModule do
  subject { create(:system_module) }

  # Validation tests
  it { is_expected.to validate_presence_of(:name) }

  # Association tests
  it { is_expected.to have_many(:access_roles).dependent(:destroy) }

  # FriendlyId tests
  it 'has a slug' do
    expect(subject).to respond_to(:slug)
  end

  # Test uniqueness of the slug
  it 'has a unique slug' do
    module1 = create(:system_module, name: 'Example Module')
    module2 = create(:system_module, name: 'Example Module')

    expect(module1.slug).not_to eq(module2.slug)
  end
end
