require 'rails_helper'

RSpec.describe Language, type: :model do
  it 'has a valid factory' do
    language = FactoryBot.create(:language)
    expect(language).to be_valid
  end

  describe "validations" do
    it { should validate_presence_of(:name) }
    # Add any other validations you have for the Language model here
  end

  describe "associations" do
    it { should have_many(:translations).dependent(:destroy) }
    # If Language has any other associations, you can test them here
  end

  describe "friendly_id" do
    it "should use slug based on name" do
      language = FactoryBot.create(:language, name: "Test Language")
      expect(language.slug).to eq("test-language")
    end
  end
end
