require 'rails_helper'

RSpec.describe Warehouse do
  describe "validations" do
    it { should validate_presence_of(:name) }
    # Add any other validations you have for the Warehouse model here
  end

  describe "friendly_id" do
    it "should use slug based on name" do
      warehouse = FactoryBot.create(:warehouse, name: "Test Warehouse")
      expect(warehouse.slug).to eq("test-warehouse")
    end
  end
end
