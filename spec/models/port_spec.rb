require 'rails_helper'

RSpec.describe Port do
  describe 'associations' do
    it { is_expected.to belong_to(:country).inverse_of(:ports) }
    it { is_expected.to have_many(:origin_criteria).class_name('Criterium').with_foreign_key(:origin_id).inverse_of(:origin).dependent(:destroy) }
    it { is_expected.to have_many(:destiny_criteria).class_name('Criterium').with_foreign_key(:destiny_id).inverse_of(:destiny).dependent(:destroy) }
    it { is_expected.to have_many(:origin_maritime_rates).class_name('MaritimeRate').with_foreign_key(:origin_id).inverse_of(:origin).dependent(:destroy) }
    it { is_expected.to have_many(:destination_maritime_rates).class_name('MaritimeRate').with_foreign_key(:destination_id).inverse_of(:destination).dependent(:destroy) }
    it { is_expected.to have_many(:origin_shipments).class_name('MaritimeShipment').with_foreign_key(:origin_id).dependent(:destroy).inverse_of(:origin) }
    it { is_expected.to have_many(:destination_shipments).class_name('MaritimeShipment').with_foreign_key(:destination_id).dependent(:destroy).inverse_of(:destination) }
    it { is_expected.to have_many(:maritime_tracking_events).with_foreign_key('location_id').dependent(:destroy).inverse_of(:location) }
    it { is_expected.to have_many(:transshipments).inverse_of(:port).dependent(:destroy) }

    describe 'validations' do
      it { is_expected.to validate_presence_of(:name) }
    end

    describe 'enums' do
      it { is_expected.to define_enum_for(:transportation_method).with_values(aerial: 0, land: 1, maritime: 2, aduanal: 3) }
    end

    describe 'methods' do
      it 'returns coordinates' do
        port = create(:port, longitude: 10, latitude: 20)
        expect(port.coordinates).to eq([10, 20])
      end

      it 'returns feature' do
        port = create(:port, id: 1, identifier: 'port_1', name: 'Port 1', longitude: 10, latitude: 20)
        feature = {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [10, 20]
          },
          properties: {
            id: 1,
            identifier: 'port_1',
            name: 'Port 1'
          }
        }
        expect(port.to_feature).to eq(feature)
      end
    end
  end
end
