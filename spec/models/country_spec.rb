require 'rails_helper'

RSpec.describe Country do
  describe 'Associations' do
    it { is_expected.to have_many(:ports).dependent(:destroy).inverse_of(:country) }
    it { is_expected.to have_many(:states).dependent(:destroy).inverse_of(:country) }
  end

  describe 'FriendlyId' do
    it { is_expected.to have_db_column(:slug).of_type(:string) }
    it { is_expected.to have_db_index(:slug).unique(true) }
  end
end
