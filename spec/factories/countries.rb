FactoryBot.define do
  factory :country do
    code { Faker::Address.country_code }
    name { Faker::Address.country }
    phonecode { Faker::PhoneNumber.subscriber_number(length: 3) }
    prohibited { false }
  end
end
