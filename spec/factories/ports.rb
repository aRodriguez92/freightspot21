FactoryBot.define do
  factory :port do
    identifier { Faker::Alphanumeric.alpha(number: 8) }
    name { Faker::Address.unique.community }
    transportation_method { Port.transportation_methods.keys.sample }
    city { Faker::Address.city }
    country factory: %i[country]
    latitude { Faker::Address.latitude.to_d }
    longitude { Faker::Address.longitude.to_d }
  end
end
