FactoryBot.define do
  factory :warehouse do
    name { Faker::Company.name } # Use Faker to generate a random company name
  end
end
