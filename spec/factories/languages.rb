FactoryBot.define do
  factory :language do
    code { Faker::Lorem.characters(number: 2) } # Generate a 2-character code
    name { Faker::Nation.language } # Generate a random language name
    default { false }
    slug { Faker::Internet.slug(words: name, glue: '-') } # Generate a slug based on the name

    # If you want to include timestamps and deleted_at
    created_at { Faker::Time.backward(days: 365, period: :all) }
    updated_at { Faker::Time.backward(days: 365, period: :all) }
    deleted_at { nil }
  end
end
