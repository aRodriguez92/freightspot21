FactoryBot.define do
  factory :system_module do
    name { Faker::Lorem.word }
  end
end
