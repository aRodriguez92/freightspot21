# Registration Mailer
class RegistrationMailer < ApplicationMailer
  default from: 'freightspot20@gmail.com'

  def welcome_email(user, password)
    @user = user
    @password = password
    @url = 'http://localhost:3000/users/sign_in'
    mail(to: user.email.to_s, subject: 'Welcome to My Awesome Site', password: @password.to_s)
  end

  def confirmation_email(user, password, client)
    @user = user
    @password = password
    @url = 'http://localhost:3000/users/sign_in'
    @client = client
    mail(to: user.email.to_s, subject: 'Welcome to My Awesome Site', password: @password.to_s)
  end
end
