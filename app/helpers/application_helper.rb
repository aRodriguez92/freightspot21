# frozen_string_literal: true

module ApplicationHelper
  def class_if_current_page(entry:)
    selected = is_current_page?(entry)

    class_name = 'active'
    class_name += ' open' unless current_page?(entry[:href])
    class_name if selected
  end

  def i18n_from_title(prefix:, title:)
    "#{prefix}#{title.downcase.gsub(' ', '_')}"
  end

  private

  def is_current_page?(entry)
    if current_page?(entry[:href])
      true
    elsif entry[:children]
      entry[:children].any?(&method(:is_current_page?))
    else
      false
    end
  end

  def prepare_hash
    @permissions = {}
    @access.each do |access|
      if @permissions[access.system_module_id].present?
        @permissions[access.system_module_id] << access.access_id
      else
        @permissions[access.system_module_id] = [access.access_id]
      end
    end
  end

  def squish(string)
    string.delete(' ').underscore
  end

  def get_service_mode(type)
    service_mode = nil
    case (type)
    when 'land'
      service_mode = ServiceMode.find_by(code: 'FT')
    when 'maritime'
      service_mode = ServiceMode.find_by(code: 'FM')
    end
    service_mode
  end

  def phoneable_type(phone)
    name = ''
    case (phone.phoneable_type)
    when 'Agent'
      agent = Agent.find(phone.phoneable_id)
      name = agent.name
    when 'Client'
      client = Client.find(phone.phoneable_id)
      name = client.name
    when 'Contact'
      contact = Contact.find(phone.phoneable_id)
      name = contact.name
    when 'Middleman'
      middleman = Middleman.find(phone.phoneable_id)
      name = middleman.name
    when 'Sender'
      sender = Sender.find(phone.phoneable_id)
      name = sender.name
    when 'Supplier'
      supplier = Supplier.find(phone.phoneable_id)
      name = supplier.name
    else
      name = 'Not registration'
    end
    name
  end

  def phone_type(type)
    case (type)
    when 'Agent'
      t('activerecord.models.agent.one')
    when 'Client'
      t('activerecord.models.client.one')
    when 'Contact'
      t('activerecord.models.contact.one')
    when 'Middleman'
      t('activerecord.models.middleman.one')
    when 'Sender'
      t('activerecord.models.sender.one')
    when 'Supplier'
      t('activerecord.models.supplier.one')
    else
      'Not type found'
    end
  end

  def icon_phone(type)
    case (type)
    when 'mobile'
      'fa-mobile-alt'
    when 'permanent'
      'fa-phone'
    else
      'fa-phone'
    end
  end

  def label_status_shipment(status)
    case status
    when 'waiting_collection'
      'label-warning'
    when 'in_transit'
      'label-info'
    when 'delivery_route'
      'label-primary'
    when 'delivered'
      'label-success'
    else
      'label-dark'
    end
  end

  def badge_quote_status(status)
    case status
    when 'pending'
      'badge-secondary'
    when 'canceled'
      'badge-primary'
    when 'authorized'
      'badge-success'
    when 'declined'
      'badge-warning'
    when 'refused'
      'badge-info'
    when 'closed'
      'badge-danger'
    end
  end

  def route_or_scale?(number_of_transshiptments)
    if number_of_transshiptments > 1
      I18n.t('activerecord.attributes.maritime_quote.scale')
    else
      I18n.t('activerecord.attributes.maritime_quote.direct')
    end
  end
end
