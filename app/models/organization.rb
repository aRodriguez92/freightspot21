# == Schema Information
#
# Table name: organizations
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_organizations_on_deleted_at  (deleted_at)
#  index_organizations_on_slug        (slug) UNIQUE
#
class Organization < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :branches, inverse_of: :organization, dependent: :destroy
  has_many :maritime_quotes, inverse_of: :organization
end
