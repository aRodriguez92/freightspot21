# == Schema Information
#
# Table name: roles
#
#  id         :bigint           not null, primary key
#  code       :string
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_roles_on_deleted_at  (deleted_at)
#  index_roles_on_slug        (slug) UNIQUE
#
class Role < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :users, inverse_of: :role, dependent: :destroy
  has_many :access_roles, inverse_of: :role, dependent: :destroy
  has_many :accesses, through: :access_roles, inverse_of: :roles

  accepts_nested_attributes_for :access_roles

  validates :name, presence: true
end
