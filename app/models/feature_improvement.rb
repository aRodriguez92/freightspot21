# == Schema Information
#
# Table name: feature_improvements
#
#  id           :bigint           not null, primary key
#  category     :bigint
#  deleted_at   :datetime
#  description  :string
#  title        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  changelog_id :integer
#
# Indexes
#
#  index_feature_improvements_on_deleted_at  (deleted_at)
#
class FeatureImprovement < ApplicationRecord
  belongs_to :changelog, inverse_of: :feature_improvements

  enum category: { incoming: 0, enhanced: 1, fixed: 2, updated: 3,  }

  validates :title, presence: true
  validates :description, presence: true
end
