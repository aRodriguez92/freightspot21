# == Schema Information
#
# Table name: quote_comments
#
#  id                :bigint           not null, primary key
#  body              :text
#  deleted_at        :datetime
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  maritime_quote_id :bigint
#  user_id           :bigint
#
# Indexes
#
#  index_quote_comments_on_deleted_at         (deleted_at)
#  index_quote_comments_on_maritime_quote_id  (maritime_quote_id)
#  index_quote_comments_on_user_id            (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (maritime_quote_id => maritime_quotes.id)
#  fk_rails_...  (user_id => users.id)
#
class QuoteComment < ApplicationRecord
  after_create_commit :notify_recipient
  include CableReady::Broadcaster

  belongs_to :user
  belongs_to :maritime_quote, inverse_of: :quote_comments

  validates :body, presence: true

  after_save do
    cable_ready['quote_comments'].insert_adjacent_html(
      html: Admin::ApplicationController.render(self),
      position: 'afterbegin',
      selector: '#comments'
    )
    cable_ready.broadcast
  end

  private
  def notify_recipient
    CommentNotification.with(quote_comment_notification: self ).deliver_later(get_recipient)
  end

  def get_recipient
    case self.user.role.code
    when 'admin'
      Client.find(self.maritime_quote.client_id).contacts.map{|contact| contact.user}
    when 'client'
      # self.maritime_quote.employee.user
      self.maritime_quote.client.team.employees.first.user
    end

  end
end
