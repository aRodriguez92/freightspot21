# == Schema Information
#
# Table name: customs
#
#  id              :bigint           not null, primary key
#  active?         :boolean
#  code            :string
#  custom          :string
#  deleted_at      :datetime
#  fcl_expo_rate   :decimal(, )
#  fcl_impo_rate   :decimal(, )
#  lcl_expo_rate   :decimal(, )
#  lcl_impo_rate   :decimal(, )
#  rate            :decimal(, )
#  section         :string
#  slug            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  service_mode_id :bigint
#
# Indexes
#
#  index_customs_on_deleted_at       (deleted_at)
#  index_customs_on_service_mode_id  (service_mode_id)
#  index_customs_on_slug             (slug) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (service_mode_id => service_modes.id)
#
class Custom < ApplicationRecord
  belongs_to :service_mode, inverse_of: :customs

  validates :custom, :code, presence: true
end
