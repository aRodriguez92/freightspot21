# == Schema Information
#
# Table name: maritime_tracking_events
#
#  id                    :bigint           not null, primary key
#  deleted_at            :datetime
#  location_type         :bigint
#  tracking_event_at     :datetime
#  vehicle               :bigint
#  voyage_name           :string
#  voyage_number         :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  activity_id           :bigint
#  location_id           :bigint
#  maritime_shipments_id :bigint
#
# Indexes
#
#  index_maritime_tracking_events_on_activity_id            (activity_id)
#  index_maritime_tracking_events_on_deleted_at             (deleted_at)
#  index_maritime_tracking_events_on_location_id            (location_id)
#  index_maritime_tracking_events_on_maritime_shipments_id  (maritime_shipments_id)
#
# Foreign Keys
#
#  fk_rails_...  (activity_id => shipment_activities.id)
#  fk_rails_...  (location_id => ports.id)
#  fk_rails_...  (maritime_shipments_id => maritime_shipments.id)
#
class MaritimeTrackingEvent < ApplicationRecord
  belongs_to :maritime_shipment, inverse_of: :maritime_tracking_events
  belongs_to :location, class_name: 'Port', inverse_of: :maritime_tracking_events
  belongs_to :activity, class_name: 'ShipmentActivity', inverse_of: :maritime_tracking_events

  enum location_type: { origin_port: 0, transshipment: 1, destination_port: 2 }
  enum vehicle: { truck: 0, rails: 1, vessel: 2 }
end
