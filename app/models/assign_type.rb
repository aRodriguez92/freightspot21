# == Schema Information
#
# Table name: assign_types
#
#  id              :bigint           not null, primary key
#  deleted_at      :datetime
#  other           :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  address_id      :bigint           not null
#  address_type_id :bigint           not null
#
# Indexes
#
#  index_assign_types_on_address_id       (address_id)
#  index_assign_types_on_address_type_id  (address_type_id)
#  index_assign_types_on_deleted_at       (deleted_at)
#
# Foreign Keys
#
#  fk_rails_...  (address_id => addresses.id)
#  fk_rails_...  (address_type_id => address_types.id)
#
class AssignType < ApplicationRecord
  belongs_to :address, inverse_of: :assign_types
  belongs_to :address_type, inverse_of: :assign_types
end
