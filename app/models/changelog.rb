# == Schema Information
#
# Table name: changelogs
#
#  id             :bigint           not null, primary key
#  deleted_at     :datetime
#  description    :text
#  released_at    :datetime
#  version_number :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_changelogs_on_deleted_at  (deleted_at)
#
class Changelog < ApplicationRecord
  include Commentable

  has_many :feature_improvements, inverse_of: :changelog, dependent: :destroy

  accepts_nested_attributes_for :feature_improvements, reject_if: :all_blank, allow_destroy: true

  validates :version_number, presence: true, uniqueness: true, format: { with: /\A\d+\.\d+\.\d+\z/, message: I18n.t('activerecord.errors.models.changelog.attributes.version.format') }
  validates :released_at, presence: true
  validates :description, presence: true
end
