# == Schema Information
#
# Table name: translations
#
#  id          :bigint           not null, primary key
#  deleted_at  :datetime
#  key         :string
#  value       :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  language_id :bigint
#
# Indexes
#
#  index_translations_on_deleted_at   (deleted_at)
#  index_translations_on_language_id  (language_id)
#
# Foreign Keys
#
#  fk_rails_...  (language_id => languages.id)
#
class Translation < ApplicationRecord
  belongs_to :language, inverse_of: :translations
end
