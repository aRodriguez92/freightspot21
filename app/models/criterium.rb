# == Schema Information
#
# Table name: criteria
#
#  id                  :bigint           not null, primary key
#  advanced_parameters :boolean          default(FALSE)
#  amount              :decimal(, )
#  deleted_at          :datetime
#  name                :string
#  remarks             :text
#  status              :bigint           default("active")
#  validity_end        :datetime
#  validity_start      :datetime
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  container_id        :bigint
#  currency_id         :bigint
#  destiny_id          :bigint
#  origin_id           :bigint
#
# Indexes
#
#  index_criteria_on_container_id  (container_id)
#  index_criteria_on_currency_id   (currency_id)
#  index_criteria_on_deleted_at    (deleted_at)
#  index_criteria_on_destiny_id    (destiny_id)
#  index_criteria_on_origin_id     (origin_id)
#
# Foreign Keys
#
#  fk_rails_...  (container_id => containers.id)
#  fk_rails_...  (currency_id => currencies.id)
#  fk_rails_...  (destiny_id => ports.id)
#  fk_rails_...  (origin_id => ports.id)
#
class Criterium < ApplicationRecord
  belongs_to :container, inverse_of: :criteria, optional: true
  belongs_to :currency, inverse_of: :criteria
  belongs_to :destiny, class_name: 'Port', inverse_of: :destiny_criteria, optional: true
  belongs_to :origin, class_name: 'Port', inverse_of: :origin_criteria, optional: true

  enum status: { expired: 0, active: 1, inactive: 2 }
end
