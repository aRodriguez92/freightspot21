# == Schema Information
#
# Table name: warehouses
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_warehouses_on_deleted_at  (deleted_at)
#  index_warehouses_on_slug        (slug) UNIQUE
#
class Warehouse < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged

  validates :name, presence: true
end
