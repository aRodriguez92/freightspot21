# == Schema Information
#
# Table name: maritime_shipments
#
#  id                    :bigint           not null, primary key
#  arrival_time          :datetime
#  bar_code              :string
#  bl_number             :string
#  container_number      :string
#  deleted_at            :datetime
#  earliest_to_latest    :datetime
#  movement_type         :bigint           default("export")
#  number                :string
#  purchase_order_number :string
#  qr_code               :string
#  remarks               :text
#  status                :bigint           default("at_origin")
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  client_id             :bigint
#  destination_id        :bigint
#  incoterm_id           :bigint
#  maritime_quote_id     :bigint
#  operator_id           :bigint
#  origin_id             :bigint
#  sales_executive_id    :bigint
#  seller_id             :bigint
#  shipping_line_id      :bigint
#  team_id               :bigint
#
# Indexes
#
#  index_maritime_shipments_on_client_id           (client_id)
#  index_maritime_shipments_on_deleted_at          (deleted_at)
#  index_maritime_shipments_on_destination_id      (destination_id)
#  index_maritime_shipments_on_incoterm_id         (incoterm_id)
#  index_maritime_shipments_on_maritime_quote_id   (maritime_quote_id)
#  index_maritime_shipments_on_operator_id         (operator_id)
#  index_maritime_shipments_on_origin_id           (origin_id)
#  index_maritime_shipments_on_sales_executive_id  (sales_executive_id)
#  index_maritime_shipments_on_seller_id           (seller_id)
#  index_maritime_shipments_on_shipping_line_id    (shipping_line_id)
#  index_maritime_shipments_on_team_id             (team_id)
#
# Foreign Keys
#
#  fk_rails_...  (client_id => clients.id)
#  fk_rails_...  (destination_id => ports.id)
#  fk_rails_...  (incoterm_id => incoterms.id)
#  fk_rails_...  (maritime_quote_id => maritime_quotes.id)
#  fk_rails_...  (operator_id => employees.id)
#  fk_rails_...  (origin_id => ports.id)
#  fk_rails_...  (sales_executive_id => employees.id)
#  fk_rails_...  (seller_id => employees.id)
#  fk_rails_...  (shipping_line_id => suppliers.id)
#  fk_rails_...  (team_id => teams.id)
#
class MaritimeShipment < ApplicationRecord
  belongs_to :origin, class_name: 'Port', inverse_of: :origin_shipments
  belongs_to :destination, class_name: 'Port', inverse_of: :destination_shipments
  belongs_to :maritime_quote, inverse_of: :maritime_shipment, optional: true
  belongs_to :shipping_line, class_name: 'Supplier', inverse_of: :maritime_shipments
  belongs_to :seller, class_name: 'Employee', inverse_of: :maritime_shipments, optional: true
  belongs_to :operator, class_name: 'Employee', inverse_of: :maritime_shipments, optional: true
  belongs_to :sales_executive, class_name: 'Employee', inverse_of: :maritime_shipments, optional: true
  belongs_to :incoterm, inverse_of: :maritime_shipments
  belongs_to :client, inverse_of: :maritime_shipments
  belongs_to :team, inverse_of: :maritime_shipments

  # has_many :maritime_tracking_events, inverse_of: :maritime_shipment, dependent: :destroy
  # has_many :maritime_shipment_documents, inverse_of: :maritime_shipment, dependent: :destroy

  enum status: { at_origin: 0, in_transit: 1, reached_destination: 2, completed: 3, delayed: 4, on_hold: 5 }
  enum movement_type: { export: 0, import: 1, trade: 2 }

  scope :by_statuses, -> (*statuses) { where(status: statuses) }

  before_validation :generate_fs_number, on: :create

  private
  def generate_fs_number
    return if number.present?

    last_vessel = MaritimeShipment.order(:created_at).last&.number
    last_quote = MaritimeQuote.order(:created_at).last&.number

    last_serial_number = [last_vessel, last_quote].compact.max || "FS-1000"
    self.number = "FS-#{last_serial_number.split('-').last.to_i + 1}"
  end
end
