# == Schema Information
#
# Table name: transshipments
#
#  id               :bigint           not null, primary key
#  deleted_at       :datetime
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  maritime_rate_id :bigint           not null
#  port_id          :bigint           not null
#
# Indexes
#
#  index_transshipments_on_deleted_at        (deleted_at)
#  index_transshipments_on_maritime_rate_id  (maritime_rate_id)
#  index_transshipments_on_port_id           (port_id)
#
# Foreign Keys
#
#  fk_rails_...  (maritime_rate_id => maritime_rates.id)
#  fk_rails_...  (port_id => ports.id)
#
class Transshipment < ApplicationRecord
  belongs_to :maritime_rate, inverse_of: :transshipments
  belongs_to :port, inverse_of: :transshipments
end
