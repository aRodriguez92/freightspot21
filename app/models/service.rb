# == Schema Information
#
# Table name: services
#
#  id              :bigint           not null, primary key
#  code            :string
#  deleted_at      :datetime
#  description     :text
#  has_cost        :boolean          default(FALSE)
#  price           :decimal(10, 4)   default(0.0)
#  service_type    :integer
#  slug            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  currency_id     :bigint
#  service_mode_id :bigint
#  tax_id          :bigint
#
# Indexes
#
#  index_services_on_currency_id      (currency_id)
#  index_services_on_deleted_at       (deleted_at)
#  index_services_on_service_mode_id  (service_mode_id)
#  index_services_on_slug             (slug) UNIQUE
#  index_services_on_tax_id           (tax_id)
#
# Foreign Keys
#
#  fk_rails_...  (currency_id => currencies.id)
#  fk_rails_...  (service_mode_id => service_modes.id)
#  fk_rails_...  (tax_id => taxes.id)
#
class Service < ApplicationRecord
  extend FriendlyId
  friendly_id :code, use: :slugged

  belongs_to :currency, inverse_of: :services
  belongs_to :tax, inverse_of: :services, optional: true
  belongs_to :service_mode, inverse_of: :services

  enum service_type: { income: 0, expense: 1 }
end
