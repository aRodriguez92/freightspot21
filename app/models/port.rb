# == Schema Information
#
# Table name: ports
#
#  id                    :bigint           not null, primary key
#  city                  :string
#  deleted_at            :datetime
#  identifier            :string
#  inactive              :boolean          default(FALSE)
#  latitude              :decimal(, )      default(0.0)
#  longitude             :decimal(, )      default(0.0)
#  name                  :string
#  slug                  :string
#  transportation_method :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  country_id            :bigint
#
# Indexes
#
#  index_ports_on_country_id  (country_id)
#  index_ports_on_deleted_at  (deleted_at)
#  index_ports_on_slug        (slug) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (country_id => countries.id)
#
class Port < ApplicationRecord
  extend FriendlyId
  friendly_id :identifier, use: :slugged
  include CableReady::Broadcaster

  belongs_to :country, inverse_of: :ports
  has_many :origin_criteria, class_name: 'Criterium', foreign_key: 'origin_id', inverse_of: :origin, dependent: :destroy
  has_many :destiny_criteria, class_name: 'Criterium', foreign_key: 'destiny_id', inverse_of: :destiny, dependent: :destroy
  has_many :origin_maritime_rates, class_name: 'MaritimeRate', foreign_key: :origin_id, inverse_of: :origin, dependent: :destroy
  has_many :destination_maritime_rates, class_name: 'MaritimeRate', foreign_key: :destination_id, inverse_of: :destination, dependent: :destroy
  has_many :origin_shipments, class_name: 'MaritimeShipment', foreign_key: :origin_id, dependent: :destroy, inverse_of: :origin
  has_many :destination_shipments, class_name: 'MaritimeShipment', foreign_key: :destination_id, dependent: :destroy, inverse_of: :destination
  has_many :maritime_tracking_events, foreign_key: 'location_id', dependent: :destroy, inverse_of: :location
  has_many :transshipments, dependent: :destroy, inverse_of: :port

  validates :name, presence: true

  enum transportation_method: { aerial: 0, land: 1, maritime: 2, aduanal: 3 }

  after_update do
    broadcast_changes
  end

  after_save do
    broadcast_insert
  end

  after_destroy do
    broadcast_remove
  end

  def coordinates
    [longitude, latitude]
  end

  def to_feature
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: coordinates
      },
      properties: {
        id: id,
        identifier: identifier,
        name: name
      }
    }
  end

  private

  def broadcast_changes
    cable_ready['ports'].morph(
      selector: "##{ActionView::RecordIdentifier.dom_id(self)}",
      html: Admin::ApplicationController.render(self)
    )
    cable_ready.broadcast
  end

  def broadcast_insert
    cable_ready['ports'].insert_adjacent_html(
      html: Admin::ApplicationController.render(self),
      position: 'afterbegin',
      selector: '#ports'
    )
    cable_ready.broadcast
  end

  def broadcast_remove
    cable_ready['ports'].remove(selector: "##{ActionView::RecordIdentifier.dom_id(self)}")
    cable_ready.broadcast
  end
end
