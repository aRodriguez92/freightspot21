# == Schema Information
#
# Table name: phones
#
#  id             :bigint           not null, primary key
#  code           :string
#  deleted_at     :datetime
#  extension      :string
#  number         :string
#  phoneable_type :string
#  principal      :boolean          default(FALSE)
#  slug           :string
#  type_phone     :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  phoneable_id   :bigint
#
# Indexes
#
#  index_phones_on_deleted_at  (deleted_at)
#  index_phones_on_phoneable   (phoneable_type,phoneable_id)
#  index_phones_on_slug        (slug) UNIQUE
#
class Phone < ApplicationRecord
  belongs_to :phoneable, polymorphic: true, optional: true

  enum type_phone: { mobile: 0, permanent: 1 }

  def full_extension_phone
    "#{number} Ext. #{extension} "
  end
end
