# == Schema Information
#
# Table name: quote_optional_services
#
#  id                  :bigint           not null, primary key
#  applied_charge      :boolean          default(TRUE)
#  charge              :decimal(, )
#  deleted_at          :datetime
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  maritime_quote_id   :bigint
#  optional_service_id :bigint
#
# Indexes
#
#  index_quote_optional_services_on_deleted_at           (deleted_at)
#  index_quote_optional_services_on_maritime_quote_id    (maritime_quote_id)
#  index_quote_optional_services_on_optional_service_id  (optional_service_id)
#
# Foreign Keys
#
#  fk_rails_...  (maritime_quote_id => maritime_quotes.id)
#  fk_rails_...  (optional_service_id => optional_services.id)
#
class QuoteOptionalService < ApplicationRecord
  belongs_to :maritime_quote
  belongs_to :optional_service
  accepts_nested_attributes_for :optional_service
end
