# == Schema Information
#
# Table name: approvals
#
#  id                :bigint           not null, primary key
#  approvalable_type :string
#  approved          :boolean
#  deleted_at        :datetime
#  preview_changes   :jsonb            not null
#  recent_changes    :jsonb            not null
#  rejected_at       :datetime
#  requested_at      :datetime
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  approvalable_id   :bigint
#  request_user_id   :bigint
#  respond_user_id   :bigint
#
# Indexes
#
#  index_approvals_on_approvalable     (approvalable_type,approvalable_id)
#  index_approvals_on_deleted_at       (deleted_at)
#  index_approvals_on_request_user_id  (request_user_id)
#  index_approvals_on_respond_user_id  (respond_user_id)
#
# Foreign Keys
#
#  fk_rails_...  (request_user_id => employees.id)
#  fk_rails_...  (respond_user_id => contacts.id)
#
class Approval < ApplicationRecord
  belongs_to :approvalable, polymorphic: true, optional: true
  belongs_to :request_user, class_name: 'Employee', inverse_of: :approvals
  belongs_to :respond_user, class_name: 'Contact', inverse_of: :approvals

  store_accessor :preview_changes, :id, :number, :description, :declared_value, :quoted_at, :expired_at, :total_pieces, :total_weight, :total_volume, :total_chargeable_weight, :total_cost, :incoterm_id, :commodities
  store_accessor :recent_changes, :id, :number, :description, :declared_value, :quoted_at, :expired_at, :total_pieces, :total_weight, :total_volume, :total_chargeable_weight, :total_cost, :incoterm_id, :commodities
  attribute :commodities, :string, array: true

  def commodities
    Array.wrap(super)
  end

  def self.approved?(maritime_quote_id)
    where('approvalable_id = ? AND approved IS NULL', maritime_quote_id).first
  end
end
