# == Schema Information
#
# Table name: currencies
#
#  id         :bigint           not null, primary key
#  code       :string
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_currencies_on_deleted_at  (deleted_at)
#  index_currencies_on_slug        (slug) UNIQUE
#
class Currency < ApplicationRecord
  extend FriendlyId
  friendly_id :code, use: :slugged

  has_many :clients, inverse_of: :currency, dependent: :destroy
  has_many :middlemen, inverse_of: :currency, dependent: :destroy
  has_many :criteria, inverse_of: :currency, dependent: :destroy
  has_many :services, inverse_of: :currency, dependent: :destroy
  has_many :suppliers, inverse_of: :currency, dependent: :destroy
  has_many :maritime_rates, inverse_of: :currency, dependent: :destroy
end
