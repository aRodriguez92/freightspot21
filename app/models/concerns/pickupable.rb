module Pickupable
  extend ActiveSupport::Concern

  included do
    has_many :pickups, as: :pickupable, dependent: :destroy
    accepts_nested_attributes_for :pickups, allow_destroy: true
  end
end