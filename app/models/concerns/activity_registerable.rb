module ActivityRegisterable
  extend ActiveSupport::Concern

  included do
    has_many :activity_registers, as: :activity_registerable, dependent: :destroy
    accepts_nested_attributes_for :activity_registers, allow_destroy: true
  end
end
