module Approvalable
  extend ActiveSupport::Concern

  included do
    has_many :approvals, as: :approvalable, dependent: :destroy
    accepts_nested_attributes_for :approvals, allow_destroy: true
  end
end
