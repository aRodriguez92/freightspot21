module Favoritable
  extend ActiveSupport::Concern

  included do
    has_many :favorites, as: :favoritable, dependent: :destroy
    accepts_nested_attributes_for :favorites, allow_destroy: true
  end
end
