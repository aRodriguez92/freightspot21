class SidebarEntry
  class << self
    include Rails.application.routes.url_helpers
  end

  def self.all
    [
      {
        group_title: '',
        children: [
          {
            href: '#',
            title: 'Application Intel',
            icon: "#{Rails.configuration.icon_prefix} fa-info-circle",
            children: [
              {
                href: admin_intel_intel_analytics_dashboard_path,
                title: 'Analytics Dashboard'
              },
              {
                href: admin_intel_intel_marketing_dashboard_path,
                title: 'Marketing Dashboard'
              },
              {
                href: admin_intel_intel_introduction_path,
                title: 'Introduction'
              },
              {
                href: admin_intel_intel_privacy_path,
                title: 'Privacy'
              }
            ]
          }
        ]
      },
      {
        group_title: '',
        children: [
          {
            href: admin_suppliers_path,
            title: I18n.t('views.sidebar.suppliers'),
            icon: "#{Rails.configuration.icon_prefix} fa-user-headset"
          },
          {
            href: admin_clients_path,
            title: I18n.t('views.sidebar.clients'),
            icon: "#{Rails.configuration.icon_prefix} fa-user-tie"
          },
          {
            href: admin_maritime_shipments_path,
            title: I18n.t('views.sidebar.shipments'),
            icon: "#{Rails.configuration.icon_prefix} fa-ship"
          },
          {
            href: admin_quotes_path,
            title: I18n.t('views.sidebar.quotes'),
            icon: "#{Rails.configuration.icon_prefix} fa-file-invoice-dollar",
          },
          {
            href: '#',
            title: I18n.t('views.sidebar.rates'),
            icon: "#{Rails.configuration.icon_prefix} fa-dollar-sign",
            children: [
              {
                href: admin_maritime_fcl_rates_path,
                title: I18n.t('views.sidebar.maritime_fcl_rates'),
                icon: "#{Rails.configuration.icon_prefix} fa-ship"
              },
              {
                href: admin_maritime_lcl_rates_path,
                title: I18n.t('views.sidebar.maritime_lcl_rates'),
                icon: "#{Rails.configuration.icon_prefix} fa-ship"
              }
            ]
          },
          {
            href: admin_bookings_path,
            title: I18n.t('views.sidebar.bookings'),
            icon: "#{Rails.configuration.icon_prefix} fa-calendar-day",
          },
        ]
      },
      {
        group_title: 'Admin',
        children: [
          {
            href: admin_logs_path,
            title: I18n.t('views.sidebar.logs'),
            icon: "#{Rails.configuration.icon_prefix} fa-line-columns"
          },
          {
            href: admin_roles_path,
            title: I18n.t('views.sidebar.roles'),
            icon: "#{Rails.configuration.icon_prefix} fa-lock"
          },
          {
            href: admin_teams_path,
            title: I18n.t('views.sidebar.teams'),
            icon: "#{Rails.configuration.icon_prefix} fa-users"
          },
          {
            href: admin_users_path,
            title: I18n.t('views.sidebar.users'),
            icon: "#{Rails.configuration.icon_prefix} fa-id-card"
          },
          {
            href: '#',
            title: I18n.t('views.sidebar.catalogs'),
            icon: "#{Rails.configuration.icon_prefix} fa-book",
            children: [
              {
                href: admin_customs_path,
                title: I18n.t('views.sidebar.customs'),
                icon: "fab fa-intercom"
              },
              {
                href: admin_charges_path,
                title: I18n.t('views.sidebar.charges'),
                icon: "#{Rails.configuration.icon_prefix} fa-badge-dollar"
              },
              {
                href: admin_containers_path,
                title: I18n.t('views.sidebar.containers'),
                icon: "#{Rails.configuration.icon_prefix} fa-container-storage"
              },
              {
                href: admin_criteria_path,
                title: I18n.t('views.sidebar.criteria'),
                icon: "#{Rails.configuration.icon_prefix} ni ni-graph"
              },
              {
                href: admin_documents_path,
                title: I18n.t('views.sidebar.documents'),
                icon: "#{Rails.configuration.icon_prefix} fa-folders"
              },
              {
                href: admin_taxes_path,
                title: I18n.t('views.sidebar.taxes'),
                icon: "#{Rails.configuration.icon_prefix} fa-percentage"
              },
              {
                href: admin_membership_plans_path,
                title: I18n.t('views.sidebar.membership_plans'),
                icon: "#{Rails.configuration.icon_prefix} fa-columns"
              },
              {
                href: admin_services_path,
                title: I18n.t('views.sidebar.services'),
                icon: "#{Rails.configuration.icon_prefix} fa-shipping-fast"
              },
              {
                href: admin_ports_path,
                title: I18n.t('views.sidebar.ports'),
                icon: "#{Rails.configuration.icon_prefix} fa-anchor"
              },
              {
                href: admin_tariff_rates_path,
                title: I18n.t('views.sidebar.tariff_rates'),
                icon: "fab fa-intercom"
              },
              {
                href: admin_packaging_types_path,
                title: I18n.t('views.sidebar.packaging'),
                icon: "#{Rails.configuration.icon_prefix} fa-box"
              }
            ]
          },
          {
            href: '#',
            title: I18n.t('views.sidebar.theme_settings'),
            icon: "#{Rails.configuration.icon_prefix} fa-cog",
            children: [
              {
                href: admin_settings_settings_layout_options_path,
                title: I18n.t('views.sidebar.layout_options')
              },
            ]
          },
          {
            href: '',
            title: I18n.t('views.sidebar.user_profile'),
            icon: "#{Rails.configuration.icon_prefix} fa-user"
          },
        ]
      },
      {
        group_title: I18n.t('views.sidebar.system'),
        children: [
          {
            href: admin_dashboard_changelog_path,
            title: I18n.t('views.sidebar.changelog'),
            icon: "#{Rails.configuration.icon_prefix} fa-code-branch"
          }
        ]
      },
      {
        group_title: 'Super Admin',
        children: [
          {
            href: admin_changelogs_path,
            title: I18n.t('views.sidebar.changelog'),
            icon: "#{Rails.configuration.icon_prefix} fa-code"
          }
        ]
      },
    ]
  end
end
