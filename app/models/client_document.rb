# == Schema Information
#
# Table name: client_documents
#
#  id          :bigint           not null, primary key
#  deleted_at  :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  client_id   :bigint           not null
#  document_id :bigint           not null
#
# Indexes
#
#  index_client_documents_on_client_id    (client_id)
#  index_client_documents_on_deleted_at   (deleted_at)
#  index_client_documents_on_document_id  (document_id)
#
# Foreign Keys
#
#  fk_rails_...  (client_id => clients.id)
#  fk_rails_...  (document_id => documents.id)
#
class ClientDocument < ApplicationRecord
  belongs_to :client, inverse_of: :client_documents
  belongs_to :document, inverse_of: :client_documents
end
