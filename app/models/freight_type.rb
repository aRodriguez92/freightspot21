# == Schema Information
#
# Table name: freight_types
#
#  id              :bigint           not null, primary key
#  code            :string
#  deleted_at      :datetime
#  name            :string
#  slug            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  service_mode_id :bigint
#
# Indexes
#
#  index_freight_types_on_deleted_at       (deleted_at)
#  index_freight_types_on_service_mode_id  (service_mode_id)
#  index_freight_types_on_slug             (slug)
#
# Foreign Keys
#
#  fk_rails_...  (service_mode_id => service_modes.id)
#
class FreightType < ApplicationRecord
  belongs_to :service_mode

  has_many :maritime_rates, inverse_of: :freight_type, dependent: :destroy
  has_many :maritime_quotes, inverse_of: :freight_type, dependent: :destroy
end
