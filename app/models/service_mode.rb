# == Schema Information
#
# Table name: service_modes
#
#  id               :bigint           not null, primary key
#  code             :string
#  deleted_at       :datetime
#  name             :string
#  slug             :string
#  variable         :bigint
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  service_modes_id :bigint
#
# Indexes
#
#  index_service_modes_on_deleted_at        (deleted_at)
#  index_service_modes_on_service_modes_id  (service_modes_id)
#  index_service_modes_on_slug              (slug) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (service_modes_id => service_modes.id)
#
class ServiceMode < ApplicationRecord
  has_many :customs, dependent: :destroy, inverse_of: :service_mode
  has_many :multiservices, dependent: :destroy, inverse_of: :service_mode
  has_many :services, inverse_of: :service_mode, dependent: :destroy
  has_many :suppliers, through: :multiservices, inverse_of: :service_modes

  enum variable: { aerial: 0, land: 1, maritime: 2, customs: 3 }
end
