# == Schema Information
#
# Table name: languages
#
#  id         :bigint           not null, primary key
#  code       :string
#  default    :boolean          default(FALSE)
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_languages_on_deleted_at  (deleted_at)
#  index_languages_on_slug        (slug) UNIQUE
#
class Language < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :translations, inverse_of: :language, dependent: :destroy

  validates :name, presence: true
end
