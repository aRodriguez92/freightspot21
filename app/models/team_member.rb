# == Schema Information
#
# Table name: team_members
#
#  id          :bigint           not null, primary key
#  deleted_at  :datetime
#  role        :bigint           default("ejecutive")
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  employee_id :bigint
#  team_id     :bigint
#
# Indexes
#
#  index_team_members_on_deleted_at   (deleted_at)
#  index_team_members_on_employee_id  (employee_id)
#  index_team_members_on_team_id      (team_id)
#
# Foreign Keys
#
#  fk_rails_...  (employee_id => employees.id)
#  fk_rails_...  (team_id => teams.id)
#
class TeamMember < ApplicationRecord
  belongs_to :team, inverse_of: :team_members
  belongs_to :employee, inverse_of: :team_members

  enum role: { ejecutive: 0, lead: 1, senior: 2, junior: 3, temporal: 4 }
end
