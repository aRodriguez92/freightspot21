# == Schema Information
#
# Table name: employees
#
#  id            :bigint           not null, primary key
#  birth_date    :datetime
#  city          :string
#  deleted_at    :datetime
#  extension     :string
#  first_name    :string
#  job           :string
#  join_date     :datetime
#  last_name     :string
#  mobile        :string
#  origin        :string
#  phone         :string
#  slug          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  department_id :bigint
#  user_id       :bigint           not null
#
# Indexes
#
#  index_employees_on_deleted_at     (deleted_at)
#  index_employees_on_department_id  (department_id)
#  index_employees_on_slug           (slug) UNIQUE
#  index_employees_on_user_id        (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (department_id => departments.id)
#  fk_rails_...  (user_id => users.id)
#
class Employee < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_person_name

  belongs_to :department, inverse_of: :employees
  belongs_to :user, -> { with_deleted }, inverse_of: :employee
  has_many :approvals, inverse_of: :request_user, foreign_key: 'request_user_id', dependent: :destroy
  has_many :team_members, inverse_of: :employee, dependent: :destroy
  has_many :teams, through: :team_members, inverse_of: :employees
  has_many :maritime_shipments, foreign_key: 'seller_id', inverse_of: :seller, dependent: :destroy
  has_many :maritime_shipments, foreign_key: 'sales_executive_id', inverse_of: :sales_executive, dependent: :destroy
  has_many :maritime_shipments, foreign_key: 'operator_id', inverse_of: :operator, dependent: :destroy

  validates :name, presence: true
end
