# == Schema Information
#
# Table name: membership_plans
#
#  id          :bigint           not null, primary key
#  active      :boolean
#  code        :string
#  deleted_at  :datetime
#  description :text
#  name        :string
#  price       :decimal(, )
#  slug        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_membership_plans_on_deleted_at  (deleted_at)
#  index_membership_plans_on_slug        (slug)
#
class MembershipPlan < ApplicationRecord
  validates :code, presence: true
  validates :name, presence: true
end
