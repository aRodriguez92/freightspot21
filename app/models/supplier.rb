# == Schema Information
#
# Table name: suppliers
#
#  id                  :bigint           not null, primary key
#  advanced_parameters :boolean
#  authorized_credit   :boolean          default(FALSE)
#  caat_code           :string
#  carrier             :boolean          default(FALSE)
#  credit_limit        :decimal(10, 4)   default(0.0)
#  deleted_at          :datetime
#  has_credit          :boolean          default(FALSE)
#  iata_code           :string
#  inactive            :boolean          default(FALSE)
#  name                :string
#  rfc                 :string
#  scac_number         :string
#  slug                :string
#  status              :bigint           default("pending")
#  website             :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  cfdi_id             :bigint
#  currency_id         :bigint
#  parent_entity_id    :bigint
#  payment_method_id   :bigint
#  payment_term_id     :bigint
#  way_pay_id          :bigint
#
# Indexes
#
#  index_suppliers_on_cfdi_id            (cfdi_id)
#  index_suppliers_on_currency_id        (currency_id)
#  index_suppliers_on_deleted_at         (deleted_at)
#  index_suppliers_on_parent_entity_id   (parent_entity_id)
#  index_suppliers_on_payment_method_id  (payment_method_id)
#  index_suppliers_on_payment_term_id    (payment_term_id)
#  index_suppliers_on_slug               (slug)
#  index_suppliers_on_way_pay_id         (way_pay_id)
#
# Foreign Keys
#
#  fk_rails_...  (cfdi_id => cfdis.id)
#  fk_rails_...  (currency_id => currencies.id)
#  fk_rails_...  (parent_entity_id => suppliers.id)
#  fk_rails_...  (payment_method_id => payment_methods.id)
#  fk_rails_...  (payment_term_id => payment_terms.id)
#  fk_rails_...  (way_pay_id => way_pays.id)
#
class Supplier < ApplicationRecord
  include Filterable

  has_one_attached :contract
  has_one_attached :logo

  belongs_to :cfdi, inverse_of: :suppliers, optional: true
  belongs_to :currency, inverse_of: :suppliers, optional: true
  belongs_to :payment_method, inverse_of: :suppliers, optional: true
  belongs_to :payment_term, inverse_of: :suppliers, optional: true
  belongs_to :parent_entity, class_name: 'Supplier', inverse_of: :parent_entity, optional: true
  belongs_to :way_pay, inverse_of: :suppliers, optional: true

  has_many :addresses, as: :addressable, dependent: :destroy
  has_many :contacts, as: :contactable, dependent: :destroy
  has_many :documents, through: :client_documents, inverse_of: :clients
  has_many :multiservices, dependent: :destroy, inverse_of: :supplier
  has_many :maritime_rates, foreign_key: 'shipper_id', dependent: :destroy, inverse_of: :shipper
  has_many :maritime_rates, foreign_key: 'shipping_line_id', dependent: :destroy, inverse_of: :shipping_line
  has_many :maritime_shipments, foreign_key: 'shipping_line_id', dependent: :destroy, inverse_of: :shipping_line
  has_many :phones, as: :phoneable, dependent: :destroy
  has_many :service_modes, through: :multiservices, inverse_of: :suppliers

  accepts_nested_attributes_for :addresses, allow_destroy: true
  accepts_nested_attributes_for :contacts, allow_destroy: true

  enum status: { pending: 0, authorized: 1, denied: 2 }

  validates :name, presence: true

  def self.columns_names(columns_included)
    columns = column_names
    columns_excluded = columns - columns_included
    columns - columns_excluded
  end

  def self.all_columns_names
    columns_excluded = %w[id parent_entity_id advanced_parameters slug created_at updated_at deleted_at
                          currency_id cfdi_id way_pay_id payment_term_id payment_method_id middleman_id
                          inactive]
    columns = column_names
    columns - columns_excluded
  end

  def principal_contact
    principal_contact = Contact.new
    contacts.each do |contact|
      principal_contact = contact if contact.principal == true
    end
    principal_contact
  end
end
