# == Schema Information
#
# Table name: cargo_types
#
#  id         :bigint           not null, primary key
#  code       :string
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_cargo_types_on_deleted_at  (deleted_at)
#  index_cargo_types_on_slug        (slug)
#
class CargoType < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :maritime_rates, inverse_of: :cargo_type, dependent: :destroy
end
