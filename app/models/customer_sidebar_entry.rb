class CustomerSidebarEntry
  class << self
    include Rails.application.routes.url_helpers
  end

  def self.all
    [
      {
        group_title: '',
        children: [
          {
            href: customer_root_path,
            title: 'Dashboard',
            icon: "#{Rails.configuration.icon_prefix} fa-dice-d20"
          },
          {
            href: customer_statistics_path,
            title: I18n.t('views.sidebar.statistics'),
            icon: "#{Rails.configuration.icon_prefix} fa-chart-bar"
          },
          {
            href: customer_quotes_path,
            title: I18n.t('views.sidebar.quotes'),
            icon: "#{Rails.configuration.icon_prefix} fa-file-invoice-dollar",
          },
          # {
          #   href: customer_bookings_path,
          #   title: I18n.t('views.sidebar.bookings'),
          #   icon: "#{Rails.configuration.icon_prefix} fa-calendar-day",
          # },
          {
            href: customer_maritime_shipments_path,
            title: I18n.t('views.sidebar.shipments'),
            icon: "#{Rails.configuration.icon_prefix} fa-ship"
          },
          # {
          #   href: customer_billings_path,
          #   title: I18n.t('views.sidebar.billings'),
          #   icon: "#{Rails.configuration.icon_prefix} fa-file-invoice-dollar"
          # },
          # {
          #   href: customer_client_products_path,
          #   title: I18n.t('views.sidebar.products'),
          #   icon: "#{Rails.configuration.icon_prefix} fa-cubes"
          # },
          # {
          #   href: customer_reports_path,
          #   title: I18n.t('views.sidebar.reports'),
          #   icon: "#{Rails.configuration.icon_prefix} fa-chart-bar"
          # }
        ]
      },
      {
        group_title: 'Customer',
        children: [

        ]
      }
    ]
  end
end
