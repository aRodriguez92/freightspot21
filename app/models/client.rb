# == Schema Information
#
# Table name: clients
#
#  id                                     :bigint           not null, primary key
#  advanced_parameters                    :boolean
#  annual_international_freight_shipments :bigint
#  authorized_credit                      :boolean          default(FALSE)
#  credit_limit                           :decimal(10, 4)   default(0.0)
#  deleted_at                             :datetime
#  has_credit                             :boolean          default(FALSE)
#  inactive                               :boolean          default(FALSE)
#  name                                   :string
#  rfc                                    :string
#  slug                                   :string
#  status                                 :bigint           default("pending")
#  website                                :string
#  created_at                             :datetime         not null
#  updated_at                             :datetime         not null
#  cfdi_id                                :bigint
#  currency_id                            :bigint
#  middleman_id                           :bigint
#  parent_entity_id                       :bigint
#  payment_method_id                      :bigint
#  payment_term_id                        :bigint
#  team_id                                :bigint
#  way_pay_id                             :bigint
#
# Indexes
#
#  index_clients_on_cfdi_id            (cfdi_id)
#  index_clients_on_currency_id        (currency_id)
#  index_clients_on_deleted_at         (deleted_at)
#  index_clients_on_middleman_id       (middleman_id)
#  index_clients_on_parent_entity_id   (parent_entity_id)
#  index_clients_on_payment_method_id  (payment_method_id)
#  index_clients_on_payment_term_id    (payment_term_id)
#  index_clients_on_slug               (slug) UNIQUE
#  index_clients_on_team_id            (team_id)
#  index_clients_on_way_pay_id         (way_pay_id)
#
# Foreign Keys
#
#  fk_rails_...  (cfdi_id => cfdis.id)
#  fk_rails_...  (currency_id => currencies.id)
#  fk_rails_...  (middleman_id => middlemen.id)
#  fk_rails_...  (parent_entity_id => clients.id)
#  fk_rails_...  (payment_method_id => payment_methods.id)
#  fk_rails_...  (payment_term_id => payment_terms.id)
#  fk_rails_...  (team_id => teams.id)
#  fk_rails_...  (way_pay_id => way_pays.id)
#
class Client < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged
  include Filterable

  before_save :set_team

  belongs_to :cfdi, inverse_of: :clients, optional: true
  belongs_to :currency, inverse_of: :clients, optional: true
  belongs_to :middleman, inverse_of: :clients, optional: true
  belongs_to :payment_method, inverse_of: :clients, optional: true
  belongs_to :payment_term, inverse_of: :clients, optional: true
  belongs_to :parent_entity, class_name: 'Client', inverse_of: :parent_entity, optional: true
  belongs_to :way_pay, inverse_of: :clients, optional: true
  belongs_to :team, inverse_of: :clients, optional: true

  has_one :bank_account, inverse_of: :client, dependent: :destroy
  has_one_attached :contract
  has_one_attached :logo

  has_many :addresses, as: :addressable, dependent: :destroy
  has_many :client_documents, inverse_of: :client, dependent: :destroy
  has_many :client_products, inverse_of: :client, dependent: :destroy
  has_many :contacts, as: :contactable, dependent: :destroy
  has_many :phones, as: :phoneable, dependent: :destroy
  has_many :documents, through: :client_documents, inverse_of: :clients
  has_many :products, through: :client_products, inverse_of: :clients
  has_many :senders, as: :senderable, dependent: :destroy
  has_many :maritime_quotes, dependent: :destroy, inverse_of: :client
  has_many :maritime_shipments, dependent: :destroy, inverse_of: :client

  accepts_nested_attributes_for :phones, allow_destroy: true
  accepts_nested_attributes_for :addresses, allow_destroy: true
  accepts_nested_attributes_for :contacts, allow_destroy: true

  validates :name, presence: true

  enum status: { pending: 0, authorized: 1, denied: 2 }
  enum annual_international_freight_shipments: { '500': 0, '100': 1, '25': 2, none_first: 3, none_ship: 4 }

  def self.columns_names(columns_included)
    columns = column_names
    columns_excluded = columns - columns_included
    columns - columns_excluded
  end

  def self.all_columns_names
    columns_excluded = %w[id parent_entity_id advanced_parameters slug created_at updated_at
                          deleted_at currency_id cfdi_id way_pay_id team_id payment_term_id
                          payment_method_id middleman_id inactive]
    columns = column_names
    columns - columns_excluded
  end

  def principal_contact
    principal_contact = Contact.new
    contacts.each do |contact|
      principal_contact = contact if contact.principal == true
    end
    principal_contact
  end

  private

  def set_team
    team_default = Team.first
    if team_default.nil?
      employee = Employee.first
      new_team = Team.create!(name: 'Default')
      TeamMember.create!(team: new_team, employee: employee)
      self.team = new_team
    else
      self.team = team_default
    end
  end
end
