# == Schema Information
#
# Table name: maritime_rates
#
#  id                       :bigint           not null, primary key
#  deleted_at               :datetime
#  departure_date           :datetime
#  free_days                :integer
#  freight_rate             :decimal(, )
#  guarantee_letter         :boolean
#  number_of_transshipments :bigint
#  rate_validity_end        :datetime
#  rate_validity_start      :datetime
#  remarks                  :text
#  transit_time             :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  cargo_type_id            :bigint           not null
#  container_id             :bigint
#  currency_id              :bigint
#  destination_id           :bigint
#  freight_type_id          :bigint
#  origin_id                :bigint
#  shipper_id               :bigint
#  shipping_line_id         :bigint
#
# Indexes
#
#  index_maritime_rates_on_cargo_type_id     (cargo_type_id)
#  index_maritime_rates_on_container_id      (container_id)
#  index_maritime_rates_on_currency_id       (currency_id)
#  index_maritime_rates_on_deleted_at        (deleted_at)
#  index_maritime_rates_on_destination_id    (destination_id)
#  index_maritime_rates_on_freight_type_id   (freight_type_id)
#  index_maritime_rates_on_origin_id         (origin_id)
#  index_maritime_rates_on_shipper_id        (shipper_id)
#  index_maritime_rates_on_shipping_line_id  (shipping_line_id)
#
# Foreign Keys
#
#  fk_rails_...  (cargo_type_id => cargo_types.id)
#  fk_rails_...  (container_id => containers.id)
#  fk_rails_...  (currency_id => currencies.id)
#  fk_rails_...  (destination_id => ports.id)
#  fk_rails_...  (freight_type_id => freight_types.id)
#  fk_rails_...  (origin_id => ports.id)
#  fk_rails_...  (shipper_id => suppliers.id)
#  fk_rails_...  (shipping_line_id => suppliers.id)
#
class MaritimeRate < ApplicationRecord
  belongs_to :cargo_type, inverse_of: :maritime_rates
  belongs_to :container, inverse_of: :maritime_rates, optional: true
  belongs_to :freight_type, inverse_of: :maritime_rates
  belongs_to :origin, class_name: 'Port', inverse_of: :origin_maritime_rates
  belongs_to :destination, class_name: 'Port', inverse_of: :destination_maritime_rates
  belongs_to :shipper, class_name: 'Supplier', inverse_of: :maritime_rates, optional: true
  belongs_to :shipping_line, class_name: 'Supplier', inverse_of: :maritime_rates
  belongs_to :currency, inverse_of: :maritime_rates

  has_many :transshipments, dependent: :destroy, inverse_of: :maritime_rate
  has_many :maritime_quotes, inverse_of: :maritime_rate

  has_and_belongs_to_many :maritime_quotes, inverse_of: :maritime_rates

  scope :fcl_rates, -> { joins(:freight_type).where('freight_types.code =?', 'FCL') }
  scope :lcl_rates, -> { joins(:freight_type).where('freight_types.code =?', 'LCL') }

  def self.import(file, freight_type_code)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    puts "\n\nHEADER"
    puts header.to_s
    (2..spreadsheet.last_row).map do |i|
      row = [header, spreadsheet.row(i)].transpose.to_h
      origin = Port.find_by(identifier: row['Origin Port'])
      destination = Port.find_by(identifier: row['Destination Port'])
      cargo_type = CargoType.find_by(name: row['Cargo Type'])
      freight_type = FreightType.find_by(code: freight_type_code)
      shipping_line = Supplier.find_or_create_by!(name: row['Shipping Line'])
      container_type = Container.find_by(code: row['Container Type'])
      puts 'CONTAINER'
      puts row['Container Type']
      # shipper = Supplier.find_or_create_by!(name: row['Shipper'])
      # puts shipper
      currency = Currency.find_by(code: row['Ocean Freight - Currency'])
      begin
        if row['No. of Transshipments'].to_i == 0
          puts "======================================= TRANSHIPMENTS 0 ============================================"
          rate = MaritimeRate.new(origin: origin, destination: destination, shipper_id: nil, shipping_line_id: shipping_line.id,
                                  freight_type_id: freight_type.id, container_id: nil, cargo_type_id: cargo_type.id,
                                  number_of_transshipments: row['No. of Transshipments'], transit_time: row['Transit Time (Days)'],
                                  rate_validity_start: row['Rate Validity - Start (yyyy-mm-dd)'],
                                  rate_validity_end: row['Rate Validity - End (yyyy-mm-dd)'], container: container_type,
                                  departure_date: row['Departure Date (yyyy-mm-dd)'], freight_rate: row['Ocean Freight - Rate'], currency_id: currency.id,
                                  free_days:row['Free Days'], guarantee_letter: row['Guarantee Letter'], remarks: row['Remarks'])
          rate.save!
        else
          puts "======================================= TRANSHIPMENTS MAYOR QUE 0 ============================================"
          rate = MaritimeRate.new(origin: origin, destination: destination, shipper_id: nil, shipping_line_id: shipping_line.id,
                                  freight_type_id: freight_type.id, container_id: nil, cargo_type_id: cargo_type.id,
                                  number_of_transshipments: row['No. of Transshipments'], transit_time: row['Transit Time (Days)'],
                                  rate_validity_start: row['Rate Validity - Start (yyyy-mm-dd)'],
                                  rate_validity_end: row['Rate Validity - End (yyyy-mm-dd)'], container: container_type,
                                  departure_date: row['Departure Date (yyyy-mm-dd)'], freight_rate: row['Ocean Freight - Rate'], currency_id: currency.id,
                                  free_days:row['Free Days'], guarantee_letter: row['Guarantee Letter'], remarks: row['Remarks'])
          rate.save!
          if row['No. of Transshipments'].to_i == 1
            if row['Transshipment 1'] != ''
              port = Port.find_by(identifier: row['Transshipment 1'])
              transshipment1 = Transshipment.new(maritime_rate_id: rate.id, port: port)
              transshipment1.save
            end
          elsif row['No. of Transshipments'].to_i == 2
            if row['Transshipment 1'] != ''
              port = Port.find_by(identifier: row['Transshipment 1'])
              transshipment1 = Transshipment.new(maritime_rate_id: rate.id, port: port)
              transshipment1.save
            end
            if row['Transshipment 2'] != ''
              port = Port.find_by(identifier: row['Transshipment 2'])
              transshipment2 = Transshipment.new(maritime_rate_id: rate.id, port: port)
              transshipment2.save
            end
          elsif row['No. of Transshipments'].to_i == 3
            if row['Transshipment 1'] != ''
              port = Port.find_by(identifier: row['Transshipment 1'])
              transshipment1 = Transshipment.new(maritime_rate_id: rate.id, port: port)
              transshipment1.save
            end
            if row['Transshipment 2'] != ''
              port = Port.find_by(identifier: row['Transshipment 2'])
              transshipment2 = Transshipment.new(maritime_rate_id: rate.id, port: port)
              transshipment2.save
            end
            if row['Transshipment 3'] != ''
              port = Port.find_by(identifier: row['Transshipment 3'])
              transshipment3 = Transshipment.new(maritime_rate_id: rate.id, port: port)
              transshipment3.save
            end
          end
        end


      rescue Exception => e
        errors.add(:base, "Import error: #{e.inspect}")
      end
    end
  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when '.csv' then Csv.new(file.path, nil, :ignore)
    when '.xls' then Roo::Excel.new(file.path)
    when '.xlsx' then Roo::Excelx.new(file.path)
    else raise ArgumentError, "Unknown file type: #{file.original_filename}"
    end
  end

  def get_transshipments
    ports = ""
    self.transshipments.each { |t| ports = ports + "#{t.port.name} " }
    ports
  end

  def port_via
    if transshipments.size == 0
      I18n.t('views.customer.maritime_quotes.direct')
    else
      transshipments.size.to_s + " #{I18n.t('views.customer.maritime_quotes.transshipments')}"
    end
  end
end
