# == Schema Information
#
# Table name: shipment_activities
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_shipment_activities_on_deleted_at  (deleted_at)
#
class ShipmentActivity < ApplicationRecord
  has_many :maritime_tracking_events, foreign_key: 'activity_id', dependent: :destroy, inverse_of: :activity
end
