class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  acts_as_paranoid
  audited
  has_paper_trail

  # Add an instance method to application_record.rb / active_record.rb
  def all_blank?(attributes)
    puts "harararara"
    attributes.all? do |key, value|
      key == '_destroy' || value.blank? ||
        value.is_a?(Hash) && all_blank?(value)
    end
  end
end
