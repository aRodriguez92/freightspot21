# == Schema Information
#
# Table name: payment_terms
#
#  id              :bigint           not null, primary key
#  amount_days_pay :integer
#  deleted_at      :datetime
#  description     :string
#  inactive        :boolean          default(FALSE)
#  slug            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_payment_terms_on_deleted_at  (deleted_at)
#  index_payment_terms_on_slug        (slug) UNIQUE
#
class PaymentTerm < ApplicationRecord
  extend FriendlyId
  friendly_id :description, use: :slugged

  has_many :clients, inverse_of: :payment_term, dependent: :destroy
  has_many :suppliers, inverse_of: :payment_term, dependent: :destroy
end
