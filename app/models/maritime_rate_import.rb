# == Schema Information
#
# Table name: maritime_rate_imports
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_maritime_rate_imports_on_deleted_at  (deleted_at)
#
class MaritimeRateImport < ApplicationRecord
  attr_reader :file
  attr_accessor :errors

  def initialize(file)
    @file = file
    @errors = []
  end

  def import_data
    spreadsheet = open_spreadsheet
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = [header, spreadsheet.row(i)].transpose.to_h
      import_row(row, i)
    end
  end

  def open_spreadsheet
    case File.extname(file.original_filename)
    when '.csv' then Roo::CSV.new(file.path)
    when '.xls' then Roo::Excel.new(file.path)
    when '.xlsx' then Roo::Excelx.new(file.path)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  private

  def import_row(row, index)
    origin = Port.find_by(identifier: row['Origin Port'])
    destination = Port.find_by(identifier: row['Destination Port'])
    cargo_type = CargoType.find_by(name: row['Cargo Type'])
    shipping_line = Supplier.find_by(name: row['Shipping Line'])
    container_type = Container.find_by(code: row['Container Type'])
    currency = Currency.find_by(code: row['Ocean Freight - Currency'])
    freight_type = FreightType.find_by(code: row['Freight Type'])

    maritime_rate = MaritimeRate.new(origin: origin, destination: destination, shipping_line: shipping_line, freight_type: freight_type,
                                     container: container_type, cargo_type: cargo_type, number_of_transshipments: row['No. of Transshipments'],
                                     transit_time: row['Transit Time (Days)'], rate_validity_start: row['Rate Validity - Start (yyyy-mm-dd)'],
                                     rate_validity_end: row['Rate Validity - End (yyyy-mm-dd)'], departure_date: row['Departure Date (yyyy-mm-dd)'],
                                     freight_rate: row['Ocean Freight - Rate'], currency: currency, free_days: row['Free Days'],
                                     guarantee_letter: row['Guarantee Letter'], remarks: row['Remarks'])

    maritime_rate.transshipments = add_transshipments(row) if row['No. of Transshipments'] >= 1
    if maritime_rate.save
      # create_transshipments(maritime_rate, row)
    else
      maritime_rate.errors.full_messages.each do |message|
        @errors << "Row #{index}: #{message}"
      end
    end
  end

  def add_transshipments(row)
    transshipments = []
    number_of_transshipments = row['No. of Transshipments']
    (1..number_of_transshipments).each do |i|
      transshipments << Transshipment.new(port: Port.find_by(identifier: row["Transshipment #{i}"])) if row["Transshipment #{i}"].present?
    end
    transshipments
  end
end
