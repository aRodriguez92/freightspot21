# == Schema Information
#
# Table name: departments
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  branch_id  :bigint           not null
#
# Indexes
#
#  index_departments_on_branch_id   (branch_id)
#  index_departments_on_deleted_at  (deleted_at)
#  index_departments_on_slug        (slug) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (branch_id => branches.id)
#
class Department < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged

  belongs_to :branch, inverse_of: :departments

  has_many :employees, inverse_of: :department, dependent: :destroy
  has_many :teams, inverse_of: :department, dependent: :destroy
end
