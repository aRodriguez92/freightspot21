# == Schema Information
#
# Table name: multiservices
#
#  id              :bigint           not null, primary key
#  deleted_at      :datetime
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  service_mode_id :bigint           not null
#  supplier_id     :bigint           not null
#
# Indexes
#
#  index_multiservices_on_deleted_at       (deleted_at)
#  index_multiservices_on_service_mode_id  (service_mode_id)
#  index_multiservices_on_supplier_id      (supplier_id)
#
# Foreign Keys
#
#  fk_rails_...  (service_mode_id => service_modes.id)
#  fk_rails_...  (supplier_id => suppliers.id)
#
class Multiservice < ApplicationRecord
  belongs_to :supplier, inverse_of: :multiservices
  belongs_to :service_mode, inverse_of: :multiservices
end
