# == Schema Information
#
# Table name: system_modules
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_system_modules_on_deleted_at  (deleted_at)
#  index_system_modules_on_slug        (slug) UNIQUE
#
class SystemModule < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :access_roles, inverse_of: :system_module, dependent: :destroy
  validates :name, presence: true
end
