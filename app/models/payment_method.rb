# == Schema Information
#
# Table name: payment_methods
#
#  id          :bigint           not null, primary key
#  code        :string
#  deleted_at  :datetime
#  description :text
#  name        :string
#  slug        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_payment_methods_on_deleted_at  (deleted_at)
#  index_payment_methods_on_slug        (slug) UNIQUE
#
class PaymentMethod < ApplicationRecord
  extend FriendlyId
  friendly_id :code, use: :slugged

  has_many :clients, inverse_of: :payment_method, dependent: :destroy
  has_many :suppliers, inverse_of: :payment_method, dependent: :destroy
end
