# == Schema Information
#
# Table name: packaging_types
#
#  id         :bigint           not null, primary key
#  code       :string
#  definition :text
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_packaging_types_on_deleted_at  (deleted_at)
#  index_packaging_types_on_slug        (slug) UNIQUE
#
class PackagingType < ApplicationRecord
  extend FriendlyId
  friendly_id :code, use: :slugged

  has_many :maritime_quote_units, inverse_of: :packaging_type
  has_many :shipment_units, inverse_of: :packaging_type

  validates :code, presence: true
  validates :name, presence: true
end
