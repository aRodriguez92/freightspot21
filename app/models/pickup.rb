# == Schema Information
#
# Table name: pickups
#
#  id              :bigint           not null, primary key
#  city            :string
#  colony          :string
#  country         :string
#  deleted_at      :datetime
#  interior_number :string
#  outdoor_number  :string
#  pickupable_type :string
#  state           :string
#  street          :string
#  zip_code        :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  pickupable_id   :bigint
#
# Indexes
#
#  index_pickups_on_deleted_at  (deleted_at)
#  index_pickups_on_pickupable  (pickupable_type,pickupable_id)
#
class Pickup < ApplicationRecord
  belongs_to :pickupable, polymorphic: true, optional: true
end
