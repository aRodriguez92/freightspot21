# == Schema Information
#
# Table name: maritime_shipment_documents
#
#  id                   :bigint           not null, primary key
#  deleted_at           :datetime
#  document_type        :bigint
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  maritime_shipment_id :bigint
#
# Indexes
#
#  index_maritime_shipment_documents_on_deleted_at            (deleted_at)
#  index_maritime_shipment_documents_on_maritime_shipment_id  (maritime_shipment_id)
#
# Foreign Keys
#
#  fk_rails_...  (maritime_shipment_id => maritime_shipments.id)
#
class MaritimeShipmentDocument < ApplicationRecord
  belongs_to :maritime_shipment, inverse_of: :maritime_shipment_documents

  enum document_type: { msds: 0, packing_list: 1, custom_invoice: 2, insurance_copy: 3, pickup_request_form: 4, custom_invoice_pl: 5, other: 6 }
end
