# == Schema Information
#
# Table name: maritime_quote_units
#
#  id                :bigint           not null, primary key
#  chargeable_weight :decimal(20, 4)   default(0.0)
#  deleted_at        :datetime
#  height            :decimal(10, 4)   default(0.0)
#  length            :decimal(10, 4)   default(0.0)
#  number_units      :bigint           default(1)
#  volume            :decimal(20, 4)   default(0.0)
#  weight            :decimal(10, 4)   default(0.0)
#  width             :decimal(10, 4)   default(0.0)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  container_id      :bigint
#  maritime_quote_id :bigint
#  packaging_type_id :bigint
#  product_id        :bigint
#  service_id        :bigint
#
# Indexes
#
#  index_maritime_quote_units_on_container_id       (container_id)
#  index_maritime_quote_units_on_deleted_at         (deleted_at)
#  index_maritime_quote_units_on_maritime_quote_id  (maritime_quote_id)
#  index_maritime_quote_units_on_packaging_type_id  (packaging_type_id)
#  index_maritime_quote_units_on_product_id         (product_id)
#  index_maritime_quote_units_on_service_id         (service_id)
#
# Foreign Keys
#
#  fk_rails_...  (container_id => containers.id)
#  fk_rails_...  (maritime_quote_id => maritime_quotes.id)
#  fk_rails_...  (packaging_type_id => packaging_types.id)
#  fk_rails_...  (product_id => products.id)
#  fk_rails_...  (service_id => services.id)
#
class MaritimeQuoteUnit < ApplicationRecord
  before_save :set_volume
  before_save :fcl_or_lcl
  belongs_to :maritime_quote, inverse_of: :maritime_quote_units, optional: true
  belongs_to :packaging_type, inverse_of: :maritime_quote_units, optional: true
  belongs_to :product, inverse_of: :maritime_quote_units, optional: true
  belongs_to :container, inverse_of: :maritime_quote_units, optional: true

  def set_volume
    self.volume = calculate_volume
  end

  def calculate_volume
    (volume = length * width * height)
  end

  def length_m_to_cm
    length * 100
  end

  def width_m_to_cm
    width * 100
  end

  def height_m_to_cm
    height * 100
  end

  def fcl_or_lcl
    if maritime_quote.freight_type.code == 'FCL'
      puts "\n\n\nENTRO FCL"
      length = 0
      width = 0
      height = 0
      weight = 0
      volume = 0
    else
      puts "\n\n\nENTRO LCL"
      number_units = nil
      container_id = nil
    end
  end
end
