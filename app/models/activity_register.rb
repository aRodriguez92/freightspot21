# == Schema Information
#
# Table name: activity_registers
#
#  id                         :bigint           not null, primary key
#  activity                   :text
#  activity_registerable_type :string
#  deleted_at                 :datetime
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  activity_registerable_id   :bigint
#  user_id                    :bigint
#
# Indexes
#
#  index_activity_registers_on_activity_registerable  (activity_registerable_type,activity_registerable_id)
#  index_activity_registers_on_deleted_at             (deleted_at)
#  index_activity_registers_on_user_id                (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class ActivityRegister < ApplicationRecord
  belongs_to :activity_registerable, polymorphic: true
  belongs_to :user
end
