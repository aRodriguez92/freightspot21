# == Schema Information
#
# Table name: bank_accounts
#
#  id             :bigint           not null, primary key
#  account_number :string
#  bank           :string
#  deleted_at     :datetime
#  owner          :string
#  slug           :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  client_id      :bigint           not null
#
# Indexes
#
#  index_bank_accounts_on_client_id   (client_id)
#  index_bank_accounts_on_deleted_at  (deleted_at)
#  index_bank_accounts_on_slug        (slug) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (client_id => clients.id)
#
class BankAccount < ApplicationRecord
  extend FriendlyId
  friendly_id :account_number, use: :slugged

  belongs_to :client, -> { with_deleted }, inverse_of: :bank_account
end
