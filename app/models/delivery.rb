# == Schema Information
#
# Table name: deliveries
#
#  id               :bigint           not null, primary key
#  city             :string
#  colony           :string
#  country          :string
#  deleted_at       :datetime
#  deliverable_type :string
#  interior_number  :string
#  outdoor_number   :string
#  state            :string
#  street           :string
#  zip_code         :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  deliverable_id   :bigint
#
# Indexes
#
#  index_deliveries_on_deleted_at   (deleted_at)
#  index_deliveries_on_deliverable  (deliverable_type,deliverable_id)
#
class Delivery < ApplicationRecord
  belongs_to :deliverable, polymorphic: true, optional: true
end
