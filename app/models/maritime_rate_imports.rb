# == Schema Information
#
# Table name: maritime_rate_imports
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_maritime_rate_imports_on_deleted_at  (deleted_at)
#
class MaritimeRateImports < ApplicationRecord
  attr_reader :file

  def initialize(uploaded_file)
    puts "INITIALIZE"
    @file = uploaded_file
  end

  def save
    puts "SAVE"
    if imported_maritime_rates.map(&:valid?).all?
      # If all rows are valid, then save all of them to the database
      imported_maritime_rates.each(&:save)
      true
    else
      imported_maritime_rates.each_with_index do |maritime_rate, index|
        add_maritime_rate_errors(maritime_rate,index)
      end
      false
    end

  end

  def add_maritime_rate_errors(maritime_rate, index)
    puts "ADD MARITIME RATE ERRRORS"
    maritime_rate.errors.full_messages.each do |message|
      next if message == "Last name has already been taken"
      errors.add :base, "Row #{index}: #{message}"
      puts errors.full_messages.to_s
    end

  end

  def imported_maritime_rates
    puts "IMPORTED MARITIME RATES"
    @imported_maritime_rates ||= load_file
  end

  def load_file
    puts "LOAD FILE"
    raise "Please upload a valid file" if file.nil?

    mappings = {
      'Origin Port' => "origin", 'Destination Port' => "destination", 'Shipper' => 'shipper_id', 'Shipping Line' => "shipping_line_id",
      'Container Type' => 'container_id', 'Cargo Type' => 'cargo_type_id', 'No. of Transshipments' => 'number_of_transshipments',
      'Transit Time (Days)' => 'transit_time', 'Rate Validity - Start (yyyy-mm-dd)' => 'rate_validity_start', 'Rate Validity - End (yyyy-mm-dd)' => 'rate_validity_end',
      'Departure Date (yyyy-mm-dd)' => 'departure_date', 'Ocean Freight - Currency' => 'currency_id', 'Ocean Freight - Rate' => 'freight_rate', 'Free Days' => '',
      'Guarantee Letter' => 'guarantee_letter', 'Remarks' => 'remarks'
    }

    @imported_maritime_rates = []

    spreadsheet = open_spreadsheet
    header = spreadsheet.row(1)
    ## We are iterating from row 2 because we have left row one for header
    puts "spreadsheet"
    puts spreadsheet
    (2..spreadsheet.last_row).each do |i|
      maritime_rate = MaritimeRate.new
      puts "INICIA ROW"
      row = [header, spreadsheet.row(i)].transpose.to_h

      origin = Port.find_by(identifier: row['Origin Port'])
      destination = Port.find_by(identifier: row['Destination Port'])
      cargo_type = CargoType.find_by(name: row['Cargo Type'])
      shipping_line = Supplier.find_or_create_by!(name: row['Shipping Line'])
      puts "\n\ncontainer:"
      puts ""
      container_type = Container.find_by(code: row['Container Type'])
      currency = Currency.find_by(code: row['Ocean Freight - Currency'])
      freight_type = FreightType.find_by(code: row['Freight Type'])

      maritime_rate.origin = origin
      maritime_rate.destination = destination
      maritime_rate.shipping_line = shipping_line
      maritime_rate.freight_type = freight_type
      maritime_rate.container = container_type
      maritime_rate.cargo_type = cargo_type
      maritime_rate.number_of_transshipments = row['No. of Transshipments']
      maritime_rate.transit_time = row['Transit Time (Days)']
      maritime_rate.rate_validity_start = row['Rate Validity - Start (yyyy-mm-dd)']
      maritime_rate.rate_validity_end = row['Rate Validity - End (yyyy-mm-dd)']
      maritime_rate.departure_date = row['Departure Date (yyyy-mm-dd)']
      maritime_rate.freight_rate = row['Ocean Freight - Rate']
      maritime_rate.currency = currency
      maritime_rate.free_days = row['Free Days']
      maritime_rate.guarantee_letter = row['Guarantee Letter']
      maritime_rate.remarks = row['Remarks']

      if row['No. of Transshipments'].to_i == 1
        if row['Transshipment 1'] != ''
          port = Port.find_by(identifier: row['Transshipment 1'])
          transshipment1 = Transshipment.new(maritime_rate_id: maritime_rate, port: port)
          maritime_rate.transshipments << transshipment1
        end
      elsif row['No. of Transshipments'].to_i == 2
        if row['Transshipment 1'] != ''
          port = Port.find_by(identifier: row['Transshipment 1'])
          transshipment1 = Transshipment.new(maritime_rate_id: maritime_rate, port: port)
          maritime_rate.transshipments << transshipment1
        end
        if row['Transshipment 2'] != ''
          port = Port.find_by(identifier: row['Transshipment 2'])
          transshipment2 = Transshipment.new(maritime_rate_id: maritime_rate, port: port)
          maritime_rate.transshipments << transshipment2
        end
      elsif row['No. of Transshipments'].to_i == 3
        if row['Transshipment 1'] != ''
          port = Port.find_by(identifier: row['Transshipment 1'])
          transshipment1 = Transshipment.new(maritime_rate_id: maritime_rate.id, port: port)
          maritime_rate.transshipments << transshipment1
        end
        if row['Transshipment 2'] != ''
          port = Port.find_by(identifier: row['Transshipment 2'])
          transshipment2 = Transshipment.new(maritime_rate_id: maritime_rate.id, port: port)
          maritime_rate.transshipments << transshipment2
        end
        if row['Transshipment 3'] != ''
          port = Port.find_by(identifier: row['Transshipment 3'])
          transshipment3 = Transshipment.new(maritime_rate_id: maritime_rate.id, port: port)
          maritime_rate.transshipments << transshipment3
        end
      end

      @imported_maritime_rates << maritime_rate
    end

    @imported_maritime_rates
  end

  def open_spreadsheet
    case File.extname(file.original_filename)
    when '.csv' then Csv.new(file.path, nil, :ignore)
    when '.xls' then Roo::Excel.new(file.path)
    when '.xlsx' then Roo::Excelx.new(file.path)
    else raise ArgumentError, "Unknown file type: #{file.original_filename}"
    end
  end
end
