# == Schema Information
#
# Table name: products
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_products_on_deleted_at  (deleted_at)
#
class Product < ApplicationRecord
  has_many :client_products, inverse_of: :product, dependent: :destroy
  has_many :clients, through: :client_products, inverse_of: :products, dependent: :destroy
  has_many :maritime_quote_units, inverse_of: :product
  has_many :shipment_units, inverse_of: :product
  validates :name, presence: true
end
