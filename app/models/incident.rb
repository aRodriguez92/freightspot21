# == Schema Information
#
# Table name: incidents
#
#  id                :bigint           not null, primary key
#  activity          :string
#  comment           :text
#  deleted_at        :datetime
#  incidentable_type :string           not null
#  slug              :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  incidentable_id   :bigint           not null
#
# Indexes
#
#  index_incidents_on_deleted_at    (deleted_at)
#  index_incidents_on_incidentable  (incidentable_type,incidentable_id)
#  index_incidents_on_slug          (slug) UNIQUE
#
class Incident < ApplicationRecord

  belongs_to :incidentable, polymorphic: true
end
