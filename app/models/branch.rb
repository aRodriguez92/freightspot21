# == Schema Information
#
# Table name: branches
#
#  id              :bigint           not null, primary key
#  deleted_at      :datetime
#  name            :string
#  slug            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  organization_id :bigint           not null
#
# Indexes
#
#  index_branches_on_deleted_at       (deleted_at)
#  index_branches_on_organization_id  (organization_id)
#  index_branches_on_slug             (slug) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (organization_id => organizations.id)
#
class Branch < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged

  belongs_to :organization, inverse_of: :branches

  has_many :departments, inverse_of: :branch, dependent: :destroy
end
