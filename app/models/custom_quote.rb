# == Schema Information
#
# Table name: custom_quotes
#
#  id           :bigint           not null, primary key
#  company_name :string
#  customer     :string
#  deleted_at   :datetime
#  email        :string
#  phone_number :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  employee_id  :bigint
#  rate_id      :bigint
#
# Indexes
#
#  index_custom_quotes_on_deleted_at   (deleted_at)
#  index_custom_quotes_on_employee_id  (employee_id)
#  index_custom_quotes_on_rate_id      (rate_id)
#
# Foreign Keys
#
#  fk_rails_...  (employee_id => employees.id)
#  fk_rails_...  (rate_id => maritime_rates.id)
#
class CustomQuote < ApplicationRecord
end
