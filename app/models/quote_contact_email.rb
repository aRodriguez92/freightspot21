# == Schema Information
#
# Table name: quote_contact_emails
#
#  id         :bigint           not null, primary key
#  code       :string
#  company    :string
#  deleted_at :datetime
#  email      :string
#  first_name :string
#  last_name  :string
#  phone      :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_quote_contact_emails_on_deleted_at  (deleted_at)
#  index_quote_contact_emails_on_slug        (slug)
#
class QuoteContactEmail < ApplicationRecord
  has_person_name

  validates :email, :company, :first_name, :last_name, :phone,  presence: true
  validates :email, 'valid_email_2/email': { blacklist: true }

  after_save :generate_code

  def generate_code
    update_column(:code, (0...8).map { (rand(65..90)).chr }.join)
  end
end
