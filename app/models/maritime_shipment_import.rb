# == Schema Information
#
# Table name: maritime_shipment_imports
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_maritime_shipment_imports_on_deleted_at  (deleted_at)
#
class MaritimeShipmentImport < ApplicationRecord
  attr_reader :file
  attr_accessor :errors, :success

  def initialize(file)
    super() # Agrega super para inicializar el estado de la clase padre.
    @file = file
    @errors = []
    @success = ''
  end

  def import_data
    spreadsheet = open_spreadsheet
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = [header, spreadsheet.row(i)].transpose.to_h
      import_row(row, i)
    end
  end

  def open_spreadsheet
    case File.extname(file.original_filename)
    when '.csv' then Roo::CSV.new(file.path)
    when '.xls' then Roo::Excel.new(file.path)
    when '.xlsx' then Roo::Excelx.new(file.path)
    else raise StandardError, "Unknown file type: #{file.original_filename}" # Utiliza raise con una clase de excepción explícita.
    end
  end

  private

  def import_row(row, index)
    attributes = {
      origin: Port.find_by(identifier: row['Origin']),
      destination: Port.find_by(identifier: row['Destination']),
      shipping_line: Supplier.find_by(name: row['Shipping Line']),
      client: Client.find_by(name: row['Client']),
      team: Team.find_by(name: row['Team']),
      incoterm: Incoterm.find_by(code: row['Incoterm']),
      bl_number: row['BL Number'],
      container_number: row['Container Number'],
      arrival_time: row['Arrival Time'],
      remarks: row['Remarks'],
      earliest_to_latest: row['Earliest to Latest'],
      movement_type: row['Movement Type'],
      purchase_order_number: row['Purchase Order Number']
    }

    maritime_shipment = MaritimeShipment.new(attributes)

    if maritime_shipment.save
      @success = I18n.t('controllers.maritime_shipments.create')
    else
      maritime_shipment.errors.full_messages.each do |message|
        @errors << "Row #{index}: #{message}"
      end
    end
  end
end
