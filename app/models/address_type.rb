# == Schema Information
#
# Table name: address_types
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  inactive   :boolean          default(FALSE)
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_address_types_on_deleted_at  (deleted_at)
#  index_address_types_on_slug        (slug)
#
class AddressType < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :assign_types, dependent: :destroy, inverse_of: :address_type
  has_many :addresses, through: :assign_types, inverse_of: :address_types
end
