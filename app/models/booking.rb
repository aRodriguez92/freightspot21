# == Schema Information
#
# Table name: bookings
#
#  id                :bigint           not null, primary key
#  deleted_at        :datetime
#  movement_type     :bigint           default(1)
#  status            :bigint           default("at_origin")
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  maritime_quote_id :bigint           not null
#
# Indexes
#
#  index_bookings_on_maritime_quote_id  (maritime_quote_id)
#
# Foreign Keys
#
#  fk_rails_...  (maritime_quote_id => maritime_quotes.id)
#
class Booking < ApplicationRecord
  belongs_to :maritime_quote, inverse_of: :booking
  enum status: { on_hold: 0, at_origin: 1, in_transit: 2, transshipment: 3, reached_destination: 4, completed: 5, delayed: 6 }
end
