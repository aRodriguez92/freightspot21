# == Schema Information
#
# Table name: favorites
#
#  id               :bigint           not null, primary key
#  deleted_at       :datetime
#  favoritable_type :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  favoritable_id   :bigint
#  user_id          :bigint           not null
#
# Indexes
#
#  index_favorites_on_deleted_at   (deleted_at)
#  index_favorites_on_favoritable  (favoritable_type,favoritable_id)
#  index_favorites_on_user_id      (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class Favorite < ApplicationRecord
  belongs_to :favoritable, polymorphic: true, optional: true
  belongs_to :user, inverse_of: :favorites
end
