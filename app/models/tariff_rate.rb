# == Schema Information
#
# Table name: tariff_rates
#
#  id          :bigint           not null, primary key
#  deleted_at  :datetime
#  description :text
#  export_tax  :decimal(, )
#  hs_code     :string
#  import_tax  :decimal(, )
#  nico        :string
#  umt         :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_tariff_rates_on_deleted_at  (deleted_at)
#
class TariffRate < ApplicationRecord
end
