# == Schema Information
#
# Table name: countries
#
#  id         :bigint           not null, primary key
#  code       :string
#  deleted_at :datetime
#  name       :string
#  phonecode  :string
#  prohibited :boolean          default(FALSE)
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_countries_on_deleted_at  (deleted_at)
#  index_countries_on_slug        (slug) UNIQUE
#
class Country < ApplicationRecord
  extend FriendlyId
  friendly_id :code, use: :slugged

  has_many :ports, inverse_of: :country, dependent: :destroy
  has_many :states, inverse_of: :country, dependent: :destroy
end
