# == Schema Information
#
# Table name: cfdis
#
#  id          :bigint           not null, primary key
#  code        :string
#  deleted_at  :datetime
#  description :text
#  moral       :boolean          default(FALSE)
#  name        :string
#  physical    :boolean          default(FALSE)
#  slug        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_cfdis_on_deleted_at  (deleted_at)
#  index_cfdis_on_slug        (slug) UNIQUE
#
class Cfdi < ApplicationRecord
  extend FriendlyId
  friendly_id :code, use: :slugged

  has_many :clients, inverse_of: :cfdi, dependent: :destroy
  has_many :suppliers, inverse_of: :cfdi, dependent: :destroy
end
