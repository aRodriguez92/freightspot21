# == Schema Information
#
# Table name: containers
#
#  id          :bigint           not null, primary key
#  code        :string
#  deleted_at  :datetime
#  description :text
#  name        :string
#  slug        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_containers_on_deleted_at  (deleted_at)
#  index_containers_on_slug        (slug) UNIQUE
#
class Container < ApplicationRecord
  extend FriendlyId
  friendly_id :code, use: :slugged

  has_many :container_dimensions, inverse_of: :container, dependent: :destroy
  has_many :criteria, inverse_of: :container, dependent: :destroy
  has_many :maritime_rates, inverse_of: :container, dependent: :destroy
  has_many :maritime_quote_units, inverse_of: :container

  accepts_nested_attributes_for :container_dimensions

  validates :name, presence: true
  validates :code, presence: true

  def internal_dimensions_metric
    "#{internal_width_value_with_code} x #{internal_height_value_with_code} x #{internal_length_value_with_code}"
  end

  def external_dimensions_metric
    "#{external_width_value_with_code} x #{external_height_value_with_code} x #{external_length_value_with_code}"
  end

  def internal_length_value_with_code
    internal_length.metric_value.to_s + (internal_length.metric_measurement.nil? ? '' : internal_length.metric_measurement.code)
  end

  def internal_width_value_with_code
    internal_width.metric_value.to_s + (internal_width.metric_measurement.nil? ? '' : internal_width.metric_measurement.code)
  end

  def internal_height_value_with_code
    internal_height.metric_value.to_s + (internal_height.metric_measurement.nil? ? '' : internal_height.metric_measurement.code)
  end

  def external_length_value_with_code
    external_length.metric_value.to_s + (external_length.metric_measurement.nil? ? '' : external_length.metric_measurement.code)
  end

  def external_width_value_with_code
    external_width.metric_value.to_s + (external_width.metric_measurement.nil? ? '' : external_width.metric_measurement.code)
  end

  def external_height_value_with_code
    external_height.metric_value.to_s + (external_height.metric_measurement.nil? ? '' : external_height.metric_measurement.code)
  end

  def internal_length
    ContainerDimension.find_by(name: 'Internal Length', container_id: id)
  end

  def internal_width
    ContainerDimension.find_by(name: 'Internal Width', container_id: id)
  end

  def internal_height
    ContainerDimension.find_by(name: 'Internal Height', container_id: id)
  end

  def external_length
    ContainerDimension.find_by(name: 'External Length', container_id: id)
  end

  def external_width
    ContainerDimension.find_by(name: 'External Width', container_id: id)
  end

  def external_height
    ContainerDimension.find_by(name: 'External Height', container_id: id)
  end
end
