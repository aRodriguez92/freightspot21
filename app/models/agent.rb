# == Schema Information
#
# Table name: agents
#
#  id                :bigint           not null, primary key
#  deleted_at        :datetime
#  inactive          :boolean          default(FALSE)
#  name              :string
#  rfc               :string
#  slug              :string
#  status            :bigint           default(0)
#  website           :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  cfdi_id           :bigint
#  currency_id       :bigint
#  payment_method_id :bigint
#  payment_term_id   :bigint
#  way_pay_id        :bigint
#
# Indexes
#
#  index_agents_on_cfdi_id            (cfdi_id)
#  index_agents_on_currency_id        (currency_id)
#  index_agents_on_deleted_at         (deleted_at)
#  index_agents_on_payment_method_id  (payment_method_id)
#  index_agents_on_payment_term_id    (payment_term_id)
#  index_agents_on_way_pay_id         (way_pay_id)
#
# Foreign Keys
#
#  fk_rails_...  (cfdi_id => cfdis.id)
#  fk_rails_...  (currency_id => currencies.id)
#  fk_rails_...  (payment_method_id => payment_methods.id)
#  fk_rails_...  (payment_term_id => payment_terms.id)
#  fk_rails_...  (way_pay_id => way_pays.id)
#
class Agent < ApplicationRecord
  include Addressable
  # include Contactable
  # include Phoneable
  extend FriendlyId

  friendly_id :name, use: :slugged

  has_one_attached :logo

  belongs_to :currency, inverse_of: :agents
  belongs_to :cfdi, inverse_of: :agents, optional: true
  belongs_to :payment_method, inverse_of: :agents, optional: true
  belongs_to :payment_term, inverse_of: :agents, optional: true
  belongs_to :way_pay, inverse_of: :agents, optional: true

  validates :name, presence: true

  def principal_address?
    principal_address = false
    addresses.each do |address|
      principal_address = true if address.principal == true
    end
    principal_address
  end

  def principal_phone?
    principal_phone = false
    phones.each do |phone|
      principal_phone = true if phone.principal == true
    end
    principal_phone
  end

  # Function that obtains the data from the main contact
  def principal_contact
    principal_contact = Contact.new
    contacts.each do |contact|
      principal_contact = contact if contact.principal == true
    end
    principal_contact
  end

  # function that obtains the data from the contact's main telephone number
  def principal_phone
    principal_phone = Phone.new
    principal_contact.phones.each do |phone|
      principal_phone = phone if phone.principal == true
    end
    principal_phone
  end

  # function that obtains the data from the main address of the contact
  def principal_address
    principal_address = Address.new
    principal_contact.addresses.each do |address|
      principal_address = address if address.principal == true
    end
    principal_address
  end

  def principal_email
    principal_contact.email
  end

  # function that gets the email from the main contact
  def email_contact
    principal_contact.email
  end

  # function that gets the full name of the primary contact
  def full_name_contact
    principal_contact.name
  end

  # function that gets the main phone number of the principal contact.
  def phone
    principal_phone.number.blank? ? principal_phone.mobile_number.to_s.strip : principal_phone.number.to_s.strip
  end

  # function that obtains the number with extension of the main telephone of the principal contact.
  def phone_with_extension
    extension + phone
  end

  def extension
    principal_phone.extension.presence || ''
  end

  # function that obtains the country code number of the main telephone of the principal contact.
  def phone_with_country_code
    phone
  end

  def country_code
    principal_phone.code.blank? ? '' : "#{principal_phone.code} "
  end

  # function that gets the complete information of the main address of the principal contact.
  def full_address
    street + number_street + colony + zip_code_with_cp
  end

  # function that obtains the postal code of the main full address of the principal contact.
  def zip_code_with_cp
    principal_address.zip_code.blank? ? '' : "CP #{principal_address.zip_code}"
  end

  # function that obtains the postal code of the main address of the principal contact.
  def zip_code
    principal_address.zip_code.presence || ''
  end

  # function that gets the street from the main address of the principal contact.
  def street
    principal_address.street.blank? ? '' : "#{principal_address.street} "
  end

  # function that gets the street number from the main address of the principal contact.
  def number_street
    principal_address.interior_number.blank? ? '' : "N° #{principal_address.interior_number}, "
  end

  # function that gets the colony from the main address of the principal contact.
  def colony
    principal_address.colony.blank? ? '' : "Col. #{principal_address.colony} "
  end

  # function that obtains the city of the main address of the principal contact.
  delegate :city, to: :principal_address
end
