# == Schema Information
#
# Table name: teams
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_teams_on_deleted_at  (deleted_at)
#  index_teams_on_slug        (slug) UNIQUE
#
class Team < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :team_members, inverse_of: :team, dependent: :destroy
  has_many :employees, through: :team_members, inverse_of: :teams
  has_many :clients, inverse_of: :team, dependent: :destroy
  has_many :maritime_shipments, dependent: :destroy, inverse_of: :team

  validates :name, presence: true

  accepts_nested_attributes_for :team_members, reject_if: :all_blank, allow_destroy: true

  def members
    employees.size
  end
end
