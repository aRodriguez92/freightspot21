# == Schema Information
#
# Table name: addresses
#
#  id               :bigint           not null, primary key
#  addressable_type :string
#  city             :string
#  colony           :string
#  country          :string
#  deleted_at       :datetime
#  interior_number  :string
#  name             :string
#  outdoor_number   :string
#  principal        :boolean          default(FALSE)
#  slug             :string
#  street           :string
#  zip_code         :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  addressable_id   :bigint
#  state_id         :bigint
#
# Indexes
#
#  index_addresses_on_addressable  (addressable_type,addressable_id)
#  index_addresses_on_deleted_at   (deleted_at)
#  index_addresses_on_slug         (slug)
#  index_addresses_on_state_id     (state_id)
#
# Foreign Keys
#
#  fk_rails_...  (state_id => states.id)
#
class Address < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged

  belongs_to :addressable, polymorphic: true, optional: true
  belongs_to :state, inverse_of: :addresses, optional: true

  has_many :assign_types, dependent: :destroy, inverse_of: :address
  has_many :address_types, through: :assign_types, inverse_of: :addresses
  has_many :phones, as: :phoneable, dependent: :destroy
  accepts_nested_attributes_for :phones, allow_destroy: true

  validates :name, presence: true
  validates :street, presence: true
  validates :interior_number, presence: true
  validates :country, presence: true

  def street_with_number
    "#{street} ##{interior_number}"
  end

  def full_information
    if country.capitalize == 'Mexico'
      street_name.capitalize + number_street + colony_with_text.capitalize + zip_code_with_cp.capitalize + state_with_text.capitalize + country.capitalize
    else
      street_name.capitalize + number_street + zip_code_with_cp.capitalize + state_with_text.capitalize + country.capitalize
    end
  end

  # function that gets the street from the main address of the principal contact.
  def street_name
    street.blank? ? '' : "#{street} "
  end

  # function that gets the street number from the main address of the principal contact.
  def number_street
    interior_number.blank? ? '' : "N° #{interior_number}, "
  end

  # function that gets the colony from the main address of the principal contact.
  def colony_with_text
    colony.blank? ? '' : "Col. #{colony}, "
  end

  def zip_code_with_cp
    zip_code.blank? ? '' : "CP #{zip_code}, "
  end

  def state_with_text
    state.blank? ? '' : "#{state.name}, "
  end

  def country_with_text
    country.blank? ? '' : country.to_s
  end

  def principal_phone
    principal_phone = Phone.new
    phones.each do |phone|
      principal_phone = phone if phone.principal == true
    end
    principal_phone
  end

  def mobile_phone
    phone
  end

  def landline
    extension + phone
  end

  # function that gets the main phone number of the principal contact.
  def phone
    principal_phone.number.blank? ? '' : principal_phone.number.to_s.strip
  end

  def extension
    principal_phone.extension.blank? ? '' : "(#{principal_phone.extension.to_s.strip}) "
  end
end
