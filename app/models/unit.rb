# == Schema Information
#
# Table name: units
#
#  id          :bigint           not null, primary key
#  code        :string
#  deleted_at  :datetime
#  name        :string
#  slug        :string
#  unit_system :bigint
#  unit_type   :bigint
#  value       :integer          default(0)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_units_on_deleted_at  (deleted_at)
#  index_units_on_slug        (slug) UNIQUE
#
class Unit < ApplicationRecord
  has_many :container_dimensions, inverse_of: :imperial_measurement, foreign_key: :imperial_measurement_id, dependent: :destroy
  has_many :container_dimensions, inverse_of: :metric_measurement, foreign_key: :metric_measurement_id, dependent: :destroy

  enum unit_type: { longitude: 0, weight: 1, volume: 2 }
  enum unit_system: { imperial: 0, metric: 1 }
end
