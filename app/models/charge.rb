# == Schema Information
#
# Table name: charges
#
#  id         :bigint           not null, primary key
#  active     :boolean          default(TRUE)
#  code       :string
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_charges_on_deleted_at  (deleted_at)
#  index_charges_on_slug        (slug)
#
class Charge < ApplicationRecord
  extend FriendlyId
  friendly_id :code, use: :slugged

  validates :code, presence: true
  validates :name, presence: true
end
