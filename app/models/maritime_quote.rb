# == Schema Information
#
# Table name: maritime_quotes
#
#  id                      :bigint           not null, primary key
#  danger_item             :boolean          default(FALSE)
#  declared_value          :decimal(30, 4)   default(0.0)
#  deleted_at              :datetime
#  description             :text
#  expired_at              :datetime
#  free_day_of_delay       :integer
#  frequency               :bigint
#  guarantee_letter        :boolean          default(FALSE)
#  is_active_shipment      :boolean          default(FALSE)
#  number                  :string
#  operation_type          :bigint
#  quoted_at               :datetime
#  reason_of_declined      :string
#  service_mode            :bigint
#  service_type            :bigint
#  slug                    :string
#  status                  :bigint           default("pending")
#  total_chargeable_weight :decimal(20, 4)   default(0.0)
#  total_cost              :decimal(20, 4)   default(0.0)
#  total_pieces            :decimal(10, 4)   default(0.0)
#  total_volume            :decimal(20, 4)   default(0.0)
#  total_weight            :decimal(10, 4)   default(0.0)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  client_id               :bigint
#  destination_route_id    :integer
#  freight_type_id         :bigint
#  incoterm_id             :bigint
#  maritime_rate_id        :bigint
#  organization_id         :bigint
#  origin_route_id         :integer
#  payment_term_id         :bigint
#
# Indexes
#
#  index_maritime_quotes_on_client_id         (client_id)
#  index_maritime_quotes_on_deleted_at        (deleted_at)
#  index_maritime_quotes_on_freight_type_id   (freight_type_id)
#  index_maritime_quotes_on_incoterm_id       (incoterm_id)
#  index_maritime_quotes_on_maritime_rate_id  (maritime_rate_id)
#  index_maritime_quotes_on_organization_id   (organization_id)
#  index_maritime_quotes_on_payment_term_id   (payment_term_id)
#  index_maritime_quotes_on_slug              (slug)
#
# Foreign Keys
#
#  fk_rails_...  (client_id => clients.id)
#  fk_rails_...  (freight_type_id => freight_types.id)
#  fk_rails_...  (incoterm_id => incoterms.id)
#  fk_rails_...  (maritime_rate_id => maritime_rates.id)
#  fk_rails_...  (organization_id => organizations.id)
#  fk_rails_...  (payment_term_id => payment_terms.id)
#
class MaritimeQuote < ApplicationRecord
  include Approvalable
  include ActivityRegisterable
  include Deliverable
  include Pickupable
  before_save :calculate_totals, :calculate_insured_merchandise, :calculate_cost
  before_save :update_maritime_quote_units
  # before_save :set_description

  belongs_to :client, inverse_of: :maritime_quotes, optional: true
  belongs_to :freight_type, inverse_of: :maritime_quotes, optional: true
  belongs_to :incoterm, inverse_of: :maritime_quotes, optional: true
  belongs_to :organization, inverse_of: :maritime_quotes, optional: true
  belongs_to :maritime_rate, inverse_of: :maritime_quotes, optional: true

  has_many :maritime_quote_units, inverse_of: :maritime_quote, dependent: :destroy
  has_many :quote_optional_services, inverse_of: :maritime_quote, dependent: :destroy
  has_many :quote_comments, inverse_of: :maritime_quote, dependent: :destroy
  has_many :optional_services, through: :quote_optional_services
  has_many :incidents, as: :incidentable

  has_one :maritime_shipment, inverse_of: :maritime_quote
  has_one :booking, inverse_of: :maritime_quote

  has_and_belongs_to_many :maritime_rates, inverse_of: :maritime_quotes

  enum frequency: { diary: 0, weekly: 1, biweekly: 2, monthly: 3, yearly: 4 }
  enum operation_type: { national: 0, export: 1, import: 2 }
  enum status: { pending: 0, canceled: 1, authorized: 2, declined: 3, refused: 4, closed: 5, booked: 6 }
  enum service_type: { door_door: 0, door_port: 1, port_port: 2, port_door: 3 }

  accepts_nested_attributes_for :maritime_quote_units, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :quote_optional_services

  before_validation :generate_fs_number, on: :create

  scope :fcl_maritime_quotes, -> { joins(:freight_type).where('freight_types.code =?', 'FCL') }
  scope :lcl_maritime_quotes, -> { joins(:freight_type).where('freight_types.code =?', 'LCL') }

  def total_costs_charge
    charges_cost = Charge.where('type_charge = ? AND chargeable_id = ?', 1, id)
    total_charges_cost = 0
    charges_cost.each do |cc|
      total_charges_cost += cc.total
    end
    total_charges_cost
  end

  def total_incomes_charge
    charges_cost = Charge.where('type_charge = ? AND chargeable_id = ?', 0, id)
    total_charges_cost = 0
    charges_cost.each do |cc|
      total_charges_cost += cc.total
    end
    total_charges_cost
  end

  def editable?
    is_active_shipment
  end

  def export_or_import?
    if maritime_rate.origin.country.name == 'Mexico'
      I18n.t('activerecord.attributes.maritime_quote.export')
    else
      I18n.t('activerecord.attributes.maritime_quote.import')
    end
  end

  def some_optional_service_nil?
    quote_optional_services.each do |op|
      if op.charge.nil?
        return true
      end
    end
    false
  end

  private
  def generate_fs_number
    return if number.present?

    last_vessel = MaritimeShipment.order(:created_at).last&.number
    last_quote = MaritimeQuote.order(:created_at).last&.number

    last_serial_number = [last_vessel, last_quote].compact.max || "FS-1000"
    self.number = "FS-#{last_serial_number.split('-').last.to_i + 1}"
  end

  def calculate_totals
    self.total_pieces = 0
    self.total_weight = 0
    self.total_volume = 0
    maritime_quote_units.each do |quote_unit|
      self.total_pieces = total_pieces + quote_unit.number_units if quote_unit.number_units
      self.total_weight = total_weight + quote_unit.weight
      self.total_volume = total_volume + quote_unit.volume
    end
  end

  def calculate_cost
    total_cost = 0.0
    quote_optional_services.each do |service|
      total_cost = total_cost + service.charge unless service.charge.nil?
    end
    total_cost = total_cost + maritime_rate.freight_rate unless maritime_rate.nil?
    self.total_cost = total_cost
  end

  def calculate_insured_merchandise
    quote_optional_services.each do |service|
      if service.optional_service.name == 'insured_merchandise'
        insure_cost = (0.5 / 100) * declared_value;
        if insure_cost <= 75.00
          insure_cost = 75.00;
        end
        service.charge = insure_cost
      end

      if service.optional_service.name == 'customs_clearance'

        custom = Custom.find_by(custom: maritime_rate.destination.name)
        if custom
          if freight_type.code == 'FCL'
            if export_or_import? == 'Importación'
              customs_clearance_cost = custom.fcl_impo_rate
            else
              customs_clearance_cost = custom.fcl_expo_rate
            end
          else
            if service_type.operation_type == 'Importación'
              customs_clearance_cost = custom.lcl_impo_rate
            else
              customs_clearance_cost = custom.lcl_expo_rate
            end
          end
          service.charge = customs_clearance_cost
        end
      end
    end
  end

  def set_description
    self.description = ''
    maritime_quote_units.each do |quote_unit|
      description.concat(quote_unit.product ? quote_unit.product.name : '', 32)
      description.concat(quote_unit.container ? quote_unit.container.name : '', 32)
    end
  end

  def update_maritime_quote_units
    maritime_quote_units.each do |maritime_quote_unit|
      if freight_type.code == 'FCL'
        if maritime_quote_unit.container_id.nil?
          maritime_quote_unit.destroy
        else
          maritime_quote_unit.length = 0
          maritime_quote_unit.width = 0
          maritime_quote_unit.height = 0
          maritime_quote_unit.weight = 0
          maritime_quote_unit.volume = 0
        end
      else
        maritime_quote_unit.number_units = nil
        maritime_quote_unit.container_id = nil
      end
    end
  end

  def self.count_by_freight_type
    joins(:freight_type)
      .select('freight_types.id, freight_types.name, COUNT(maritime_quotes.freight_type_id) as freight_type_count')
      .group('freight_types.id')
  end

  def self.count_by_month
    MaritimeQuote.group_by_month(:created_at, format: '%b %Y').count
  end

  def self.count_by_year
    MaritimeQuote.group_by_year(:created_at, format: '%Y').count
  end

  def self.count_by_status
    MaritimeQuote.group(:status).count
  end
end
