# == Schema Information
#
# Table name: states
#
#  id         :bigint           not null, primary key
#  code       :string
#  deleted_at :datetime
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  country_id :bigint           not null
#
# Indexes
#
#  index_states_on_country_id  (country_id)
#  index_states_on_deleted_at  (deleted_at)
#  index_states_on_slug        (slug) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (country_id => countries.id)
#
class State < ApplicationRecord
  extend FriendlyId
  friendly_id :code, use: :slugged

  belongs_to :country, inverse_of: :states

  has_many :addresses, inverse_of: :state, dependent: :destroy
end
