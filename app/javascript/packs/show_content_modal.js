function show_content_modal(title, title_button, action) {
    if (action == 'create') {
        // $("#modal-dg").addClass("modal-lg" );
        $("#modal_title").html(title);
        $("#btn_submit_modal").show();
        $("#btn_close_modal").show();
        $("#btn_yes_modal").hide();
        $("#btn_no_modal").hide();
        $("#btn_submit_modal").html(title_button);
    } else if (action == 'update') {
        $("#modal_title").html(title);
        $("#btn_submit_modal").show()
        $("#btn_close_modal").show();
        $("#btn_yes_modal").hide();
        $("#btn_no_modal").hide();
        $("#btn_submit_modal").html(title_button);
    } else if (action == 'destroy') {
        $("#modal_title").html(title);
        $("#btn_submit_modal").hide();
        $("#btn_close_modal").hide();
        $("#btn_yes_modal").show();
        $("#btn_no_modal").show();
    } else {
        // $("#modal-dg").addClass("modal-lg" );
        $("#modal_title").html(title);
        $("#btn_close_modal").show();
        $("#btn_yes_modal").hide();
        $("#btn_no_modal").hide();
        $("#btn_submit_modal").hide();
        // $("#modal-dg").removeClass( "modal-lg" );
    }
}