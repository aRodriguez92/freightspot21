// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

import Rails from "@rails/ujs"
import * as ActiveStorage from "@rails/activestorage"
import "channels"

Rails.start()
ActiveStorage.start()

const moment = require('moment');
const jQuery = require('jquery');

// const Uppy = require('@uppy/core');
// const Spanish = require('@uppy/locales/lib/es_ES');
// const Dashboard = require('@uppy/dashboard');
// const ActiveStorageUpload = require('@excid3/uppy-activestorage-upload');
// const GoogleDrive = require('@uppy/google-drive');
// const Dropbox = require('@uppy/dropbox');
// const Box = require('@uppy/box');
// const Instagram = require('@uppy/instagram');
// const Facebook = require('@uppy/facebook');
// const OneDrive = require('@uppy/onedrive');
// const Webcam = require('@uppy/webcam');
// const ScreenCapture = require('@uppy/screen-capture');
// const ImageEditor = require('@uppy/image-editor');
// const Tus = require('@uppy/tus');
// const Url = require('@uppy/url');
// const DropTarget = require('@uppy/drop-target');
// const GoldenRetriever = require('@uppy/golden-retriever');
//
// require('@uppy/core/dist/style.css');
// require('@uppy/dashboard/dist/style.css');
require("@nathanvda/cocoon");
require('../../../vendor/assets/javascripts/freightspot/formplugins/intlTelInput/intlTelInput');
import "chartkick/chart.js"


// global.$ = global.Uppy = Uppy;
// window.$ = window.Uppy = Uppy;
// global.$ = global.Dashboard = Dashboard;
// window.$ = window.Dashboard = Dashboard;
// global.$ = global.ActiveStorageUpload = ActiveStorageUpload;
// window.$ = window.ActiveStorageUpload = ActiveStorageUpload;
// global.$ = global.GoogleDrive = GoogleDrive ;
// window.$ = window.GoogleDrive = GoogleDrive ;
// global.$ = global.Dropbox = Dropbox;
// window.$ = window.Dropbox = Dropbox;
// global.$ = global.Box = Box;
// window.$ = window.Box = Box;
// global.$ = global.Instagram = Instagram;
// window.$ = window.Instagram = Instagram;
// global.$ = global.Facebook = Facebook;
// window.$ = window.Facebook = Facebook;
// global.$ = global.OneDrive = OneDrive;
// window.$ = window.OneDrive = OneDrive;
// global.$ = global.Webcam = Webcam;
// window.$ = window.Webcam = Webcam;
// global.$ = global.ScreenCapture = ScreenCapture;
// window.$ = window.ScreenCapture = ScreenCapture;
// global.$ = global.ImageEditor = ImageEditor;
// window.$ = window.ImageEditor = ImageEditor;
// global.$ = global.Tus = Tus;
// window.$ = window.Tus = Tus;
// global.$ = global.Url = Url;
// window.$ = window.Url = Url;
// global.$ = global.DropTarget = DropTarget;
// window.$ = window.DropTarget = DropTarget;
// global.$ = global.GoldenRetriever = GoldenRetriever;
// window.$ = window.GoldenRetriever = GoldenRetriever;
// global.toastr = require("toastr");

// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)
import ApexCharts from 'apexcharts';
window.ApexCharts = ApexCharts; // return apex chart

// include jQuery in global and window scope (so you can access it globally)
// in your web browser, when you type $('.div'), it is actually refering to global.$('.div')
global.$ = global.jQuery = jQuery;
window.$ = window.jQuery = jQuery;

// include moment in global and window scope (so you can access it globally)
global.moment = moment;
window.moment = moment;

// global.Spanish = Spanish;
// window.Spanish = Spanish;

$(document).ready(function() {
    // alert ("Hola Mundo");
});