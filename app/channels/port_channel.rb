class PortChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'ports'
  end

  def unsubscribed; end
end
