class QuoteCommentChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'quote_comments'
  end

  def unsubscribed; end
end
