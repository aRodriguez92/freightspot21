# To deliver this notification:
#
# QuoteNotification.with(post: @post).deliver_later(current_user)
# QuoteNotification.with(post: @post).deliver(current_user)

class QuoteNotification < Noticed::Base
  # Add your delivery methods
  #
  deliver_by :database
  # deliver_by :email, mailer: "UserMailer"
  # deliver_by :slack
  # deliver_by :custom, class: "MyDeliveryMethod"
  deliver_by :action_cable

  # Add required params
  #
  param :quote_notification

  # Define helper methods to make rendering easier.
  #
  def message
    t(".message")
  end
  #
  def url
    post_path(params[:quote_notification])
  end
end
