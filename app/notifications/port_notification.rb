# To deliver this notification:
#

class PortNotification < Noticed::Base
  # Add your delivery methods
  #
  deliver_by :database, format: :to_database
  # deliver_by :action_cable
  # deliver_by :email, mailer: 'UserMailer'
  # deliver_by :slack

  def to_database
    {
      type: self.class.name,
      params: params
    }
  end

  # Add required params
  #
  param :port

  # Define helper methods to make rendering easier.
  #
  def message
    t('.message')
  end

  def url
    admin_root_path
  end
end
