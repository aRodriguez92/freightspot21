class QuoteContactEmailsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def create
    begin
      quote_contact_email = QuoteContactEmail.create!(quote_contact_email_params)
      #Aqui se creara ekl usuario
      password = Devise.friendly_token(8)
      role = Role.find_by(name: 'Cliente')
      user = User.create(username: quote_contact_email.first_name, email: quote_contact_email.email, password: password, role: role, accept_terms_of_service_and_privacy_policies: true)
      # se crea el cliente
      client = Client.create(name: quote_contact_email.company)
      # se crea el telefono
      phone = Phone.create(number: quote_contact_email.phone)
      # Se crea el contacto
      contact = Contact.create(first_name: quote_contact_email.first_name, last_name: quote_contact_email.last_name, email: quote_contact_email.email, user: user)
      client.contacts << contact
      client.save
      sign_in user
    rescue => e
      respond_to do |format|
        errors =  e.message.split(':')[1].split(',')
        quote_contact_email_errors = Array.new
        errors.each do |error|
          quote_contact_email_errors << error.strip.downcase.gsub( " ", "_")
        end
        format.html # index.html.erb
        format.xml { render :xml => quote_contact_email }
        format.json { render :json => quote_contact_email_errors }
      end
    end

  end

  private

  def quote_contact_email_params
    params.require(:quote_contact_email).permit(:first_name, :last_name, :company, :email, :phone)
  end
end
