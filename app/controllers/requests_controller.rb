class RequestsController < ApplicationController
  def quote
    @ports = Port.all
    rates ||= []
    @rates = MaritimeRate.where('origin_id = ? AND destination_id = ?', params[:origin], params[:destination])
    @rates.each do |tariff|
      rates << tariff
    end
    data = rates.flatten.sort_by { |rate| [rate.freight_rate, rate.transit_time] }
    @minimal_cost = data.first
    @cost_benefit = data.second
  end
end
