# Shared methods between the different controllers
module SharedMethods
  extend ActiveSupport::Concern

  def filter_states
    country_name = params[:country_id]
    country = Country.find_by(name: country_name)
    @states = State.where(country_id: country.id)
    respond_to do |format|
      format.json { render json: @states }
    end
  end

  def set_readonly
    @readonly = params[:readonly] == 'true'
  end

  def update_readonly
    @readonly = params[:readonly] == 'true'
    respond_to do |format|
      format.js
    end
  end

  def filter_address
    @address = Address.find(params[:address_id])
    respond_to do |format|
      format.json { render json: @address }
    end
  end
end
