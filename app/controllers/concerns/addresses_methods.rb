module AddressesMethods
  extend ActiveSupport::Concern

  included do
    before_action :set_address, only: %i[show_address edit_address update_address destroy_address]
  end

  def new_address
    @is_contact = params[:is_contact]
    @address = Address.new
  end

  def add_address
    @is_contact = false
    @create = false
    @address = Address.new(address_params)
    if @address.save
      @create = true
      addressable_data
      @is_contact = params[:is_contact]
      @response = t('controllers.addresses.create')
    else
      Rails.logger.debug @address.errors.messages
    end
  end

  def show_address; end

  def edit_address; end

  def update_address
    @update = false
    respond_to do |format|
      if @address.update(address_params)
        @update = true
        @response = t('controllers.addresses.update')
        addressable_data
        format.js { render layout: false, content_type: 'text/javascript' }
        format.html
      end
    end
  end

  def destroy_address
    @address.destroy!
    addressable_data
    @response  = t('controllers.addresses.destroy')
  end

  private

  def set_address
    @address = Address.find(params[:address_id])
  end

  def address_params
    params.require(:address).permit(:id, :addressable_type, :name, :addressable_id, :street, :interior_number,
                                    :outdoor_number, :city, :zip_code, :country, :state_id, :colony, :principal,
                                    address_type_ids: [])
  end

  def addressable_data
    case @address.addressable_type
    when 'Client'
      @client = Client.find(@address.addressable_id)
      @addresses = @client.addresses.order(:name)
    when 'Supplier'
      @supplier = Supplier.find(@address.addressable_id)
      @addresses = @supplier.addresses.order(:name)
    end
  end
end
