module PhonesMethods
  extend ActiveSupport::Concern

  included do
    before_action :set_phone, only: %i[show_phone edit_phone update_phone destroy_phone]
  end

  def new_phone
    @phone = Phone.new
    render_phone_response
  end

  def add_phone
    @phone = Phone.new(phone_params)
    if @phone.save
      @response = t('controllers.phones.create')
      client_or_supplier
      render_phone_response
    else
      render :new_phone, content_type: 'text/javascript'
    end
  end

  def show_phone; end

  def edit_phone; end

  def update_phone
    if @phone.update(phone_params)
      @response = t('controllers.phones.update')
      client_or_supplier
      render_phone_response
    else
      render :edit_phone, content_type: 'text/javascript'
    end
  end

  def destroy_phone; end

  private

  def set_phone
    @phone = Phone.find(params[:phone_id])
  end

  def phone_params
    params.require(:phone).permit(:id, :phoneable_id, :phoneable_type, :number, :extension, :code, :type_phone,
                                  :mobile_number)
  end

  def client_or_supplier
    case @phone.phoneable_type
    when 'Client'
      @client = Client.find(@phone.phoneable_id)
      @senders = @client.senders
      @contact = @client.contacts.order('principal DESC')
      @phones = PhoneInfo::PhoneNumberFetcher.get_phone_numbers(@client)
    when 'Supplier'
      @supplier = Supplier.find(@phone.phoneable_id)
      @contact = @supplier.contacts.order('principal DESC')
      @phones = PhoneInfo::PhoneNumberFetcher.get_phone_numbers(@supplier)
    end
  end

  def render_phone_response
    respond_to do |format|
      format.js { render layout: false, content_type: 'text/javascript' }
      format.html
    end
  end
end
