module General
  extend ActiveSupport::Concern

  def approval_request(_request_user, _respond_user, _preview_json, _recent_changes)
    puts "\n\nDATA"
    puts "\n\nrespond user: #{_respond_user}"
    puts "\n\nrequest user: #{_request_user}"
    puts "\n\nDATA: #{_recent_changes.total_pieces}"
    puts "\n\nRecent Pieces: #{_recent_changes.maritime_quote_units.first.number_units}"

    _recent_json = _recent_changes.to_json(include: :maritime_quote_units)
    approval = Approval.new(
      approvalable_type: _recent_changes.class.name, approvalable_id: _recent_changes.id,
      request_user_id: _request_user.id, respond_user_id: _respond_user.id,
      preview_changes: _preview_json, recent_changes: _recent_json
    )

    if approval.save
      puts "Solicitud enviada"
      QuoteNotification.with(quote_notification: _recent_changes).deliver_later(Client.find(_recent_changes.client_id).contacts.map{|contact| contact.user})
    end
  end
end
