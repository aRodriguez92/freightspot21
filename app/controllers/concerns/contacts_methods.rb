module ContactsMethods
  extend ActiveSupport::Concern

  included do
    before_action :set_contact, only: %i[show_contact edit_contact update_contact destroy_contact]
  end

  def add_contact
    @create = false
    @contact = Contact.new(contact_params)
    if params[:address][:id].present?
      Rails.logger.debug "\n\n\nADDRESS ID PRESENTE"
      address = Address.find(params[:address][:id])
      @contact.addresses << clone_address(address)
    else
      Rails.logger.debug "\n\n\nNO PRESENTE"
    end
    respond_to do |format|
      if @contact.save
        @create = true
        contactable_data
        @response = t('controllers.contacts.create')
        format.js { render layout: false, content_type: 'text/javascript' }
        format.html
      else
        Rails.logger.debug 'ERRRRRRROOOOOOOOOOOOOOOOOORRRRRR'
      end
    end
  end

  def show_contact; end

  def edit_contact
    @contact.phones.build if @contact.phones.blank?
  end

  def update_contact
    @update = false
    respond_to do |format|
      if @contact.update(contact_params)
        contactable_data
        @update = true
        @response = t('controllers.contacts.update')
        format.js { render layout: false, content_type: 'text/javascript' }
        format.html
      end
    end
  end

  def destroy_contact
    @contact.destroy!
    contactable_data
    @response = t('controllers.contacts.destroy')
  end

  def clone_address(address)
    address_dup = Address.new
    address_dup.name = address.name
    address_dup.street = address.street
    address_dup.interior_number = address.interior_number
    address_dup.outdoor_number = address.outdoor_number
    address_dup.colony = address.colony
    address_dup.city = address.city
    address_dup.country = address.country
    address_dup.zip_code = address.zip_code
    address_dup.state_id = address.state_id
    address_dup.address_type_ids = address.address_type_ids
    address_dup.principal = address.principal
    address_dup
  end

  private

  def contact_params
    params.require(:contact).permit(:id, :contactable_id, :contactable_type, :name, :email, :area,
                                    :job, :birth_date, :principal, :photo,
                                    phones_attributes: phones_attributes)
  end

  def set_contact
    @contact = Contact.find(params[:contact_id])
  end

  def contactable_data
    case @contact.contactable_type
    when 'Client'
      @client = Client.find(@contact.contactable_id)
      @contacts = @client.contacts.order('principal DESC')
      generate_user if params[:action] == 'create'
    when 'Supplier'
      @supplier = Supplier.find(@contact.contactable_id)
      @contacts = @supplier.contacts.order('principal DESC')
    end
  end

  def generate_user
    rol = Role.find_by(name: 'Cliente')
    password = Devise.friendly_token.first(8)
    user = User.create!(username: @contact.name.mentionable, password: password, email: @contact.email, role_id: rol.id,
                        accept_terms_of_service_and_privacy_policies: true)
    RegistrationMailer.welcome_email(user, password).deliver
  end
end
