module SupplierActions
  extend ActiveSupport::Concern

  included do
    before_action :set_supplier, only: %i[show edit update destroy update_readonly
                                          new_address add_address edit_address update_address destroy_address
                                          new_phone show_phone destroy_phone edit_phone update_phone
                                          edit_contact update_contact destroy_contact]
    # ... (otras acciones)
  end

  def index
    @suppliers = Supplier.order(:name)
    set_default_columns
    @column_names = columns_name
    @all_columns = @suppliers.all_columns_names
  end

  def show
    @address = Address.new
    @phone = Phone.new
    @contact = Contact.new
    @supplier.contacts.build.addresses.build
    @is_contact = params[:is_contact]
    @addresses = @supplier.addresses.order(:name)
    @contacts = @supplier.contacts.order('principal DESC')
    @phones = phone_list_supplier(@supplier, @contacts)
  end

  def new
    @supplier = Supplier.new
    @supplier.addresses.build.phones.build
    @supplier.contacts.build.addresses.build
  end

  def edit; end

  def create
    @supplier = Supplier.new(supplier_params)
    if @supplier.save!
      redirect_to admin_suppliers_path
      flash[:success] = t('controllers.suppliers.create')
    else
      render :new
    end
  end

  def update
    if @supplier.update(supplier_params)
      @address = Address.new
      @phone = Phone.new
      @contact = Contact.new
      @sender = Sender.new
      @response = t('controllers.suppliers.update')
      render :show do |page|
        page.replace(@supplier, partial: 'suppliers/personal_information')
      end
    else
      render :show
    end
  end

  def destroy; end

  private

  def set_supplier
    @supplier = Supplier.find(params[:id])
  end

  def set_default_columns
    assign_columns(%w[name website rfc status])
  end

  def assign_columns(columns)
    @column_names = @suppliers.columns_names(columns)
  end

  def columns_name
    @column_names
  end
end