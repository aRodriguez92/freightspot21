class ApplicationController < ActionController::Base
  before_action :set_locale

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:email, :accept_terms_of_service_and_privacy_policies,
                                                       { contact_attributes: contact_attributes }])
    devise_parameter_sanitizer.permit(:sign_in, keys: %i[email password])
  end

  def contact_attributes
    %i[id contactable_type name email user_id]
  end

  def set_locale
    I18n.locale = params[:locale] || locale_from_header || I18n.default_locale
  end

  def locale_from_header
    request.env.fetch('HTTP_ACCEPT_LANGUAGE', '').scan(/[a-z]{2}/).first
  end
end
