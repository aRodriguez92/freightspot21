# frozen_string_literal: true

module Users
  # Registrations Controller
  class RegistrationsController < Devise::RegistrationsController
    before_action :set_role, only: %i[create update edit]

    def new
      build_resource({})
      yield resource if block_given?
      resource.build_contact
      respond_with resource
    end

    def create
      build_resource(sign_up_params)
      password = Devise.friendly_token(8)
      resource.username = resource.contact.name.mentionable unless resource.contact.name.nil?
      resource.contact.first_name = resource.contact.name.split(' ')[0] unless resource.contact.name.nil?
      resource.contact.last_name = resource.contact.name.split(' ')[1] unless resource.contact.name.nil?
      resource.contact.principal = true
      resource.contact.email = resource.email
      resource.password = password
      resource.password_confirmation = password
      resource.role = @role
      resource.approved = true
      resource_saved = resource.save
      yield resource if block_given?
      if resource_saved
        if resource.active_for_authentication?
          set_flash_message :notice, :signed_up if is_flashing_format?
          # sign_up(resource_name, resource)
          redirect_to user_steps_path(resource: resource, resource_name: resource_name, password: password)
        else
          set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_flashing_format?
          expire_data_after_sign_in!
          respond_with resource, location: after_inactive_sign_up_path_for(resource)
        end
      else
        clean_up_passwords resource
        @validatable = devise_mapping.validatable?
        @minimum_password_length = resource_class.password_length.min if @validatable
        respond_with resource
      end
    end

    private

    def after_sign_in_path_for(resource)
      case resource.role.slug
      when 'administrador'
        admin_root_path
      else
        customer_root_path
      end
    end

    def set_role
      @role = Role.find_by(name: 'Cliente')
    end

    def sign_up_params
      params.require(:user).permit(:email, :accept_terms_of_service_and_privacy_policies,
                                   :password, :password_confirmation, :last_name,
                                   contact_attributes: contact_attributes)
    end

    def phones_attributes
      %i[id _destroy phoneable_type phoneable_id code principal type_phone number]
    end

    def contact_attributes
      [:id, :_destroy, :contactable_type, :contactable_id, :name, :user_id, :principal, :email,
       phones_attributes: phones_attributes]
    end

    def phones_params
      params.require(:phones).permit(:id, :phoneable_id, :phoneable_type, :number, :extension, :code,
                                     :type_phone, :principal)
    end
  end
end
