# frozen_string_literal: true

module Users
  # Sessions Controller
  class SessionsController < Devise::SessionsController
    def after_sign_in_path_for(resource)
      case resource.role.code
      when 'client', 'user'
        customer_root_path
      when 'admin', 'super'
        admin_root_path
      else
        super
      end
    end
  end
end
