class Admin::ShipmentsController < Admin::ApplicationController
  before_action :set_shipment, only: %i[edit show update destroy]
  def index
    @total_shipments =  MaritimeShipment.count
    @maritime_shipments = if params[:status].present?
                            MaritimeShipment.where(status: params[:status])
                          else
                            MaritimeShipment.all
                          end
    @total_on_hold = @maritime_shipments.where(status: :on_hold).count
    @total_in_transit = @maritime_shipments.where(status: :in_transit).count
    @total_at_origin = @maritime_shipments.where(status: :at_origin).count
    @total_reached_destination = @maritime_shipments.where(status: :reached_destination).count
    @total_completed = @maritime_shipments.where(status: :completed).count
    @total_delayed = @maritime_shipments.where(status: :delayed).count
  end

  def show; end

  def new
    @shipment = MaritimeShipment.new
  end

  private

  def set_shipment
    @shipment = MaritimeShipment.find(params[:id])
  end
end
