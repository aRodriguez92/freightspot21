class Admin::CommentsController < Admin::ApplicationController
  protect_from_forgery with: :null_session
  respond_to :js, :html, :json

  def create
    commentable_type = params[:comment][:commentable_type]
    @object_klass = commentable_type.to_s.constantize

    @data = @object_klass.find(params[:comment][:commentable_id])
    @comment = @data.comments.new(comment_params)
    @user = @comment.user
    if @comment.save
      # redirecto_to admin_dashboard_changelog_path
      # format.json { render json: { comment: @comment, user: @user } }
      if commentable_type == 'Changelog'
        redirect_to admin_dashboard_changelog_path
      end
    else
      # format.html { render :show, status: :unprocessable_entity }
      # format.json { render json: @comment.errors, status: :unprocessable_entity }
    end
  end

  private
  def comment_params
    params.require(:comment).permit(:id, :commentable_id, :commentable_type, :user_id, :body)
  end
end
