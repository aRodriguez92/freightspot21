class Admin::CustomsController < Admin::ApplicationController
  before_action :set_custom, only: %i[show edit update destroy]

  def index
    @customs = Custom.where(active?: true)
  end

  def show; end

  def new
    @custom = Custom.new
  end

  def edit; end

  def create
    @custom = Custom.new(custom_params)
    if @custom.save
      redirect_to admin_customs_path
      flash[:success] = t('controllers.customs.create')
    else
      render :new
    end
  end

  def update
    if @custom.update(custom_params)
      redirect_to admin_customs_path
      flash[:success] = t('controllers.customs.update')
    else
      render :edit
    end
  end

  def destroy
    if @custom.destroy
      flash[:danger] = t('controllers.customs.destroy')
      redirect_to admin_customs_path
    else
      render :show
    end
  end

  private

  def custom_params
    params.require(:custom).permit(:code, :custom, :rate, :fcl_expo_rate, :fcl_impo_rate, :lcl_expo_rate, :lcl_impo_rate, :active?, :service_mode_id)
  end

  def set_custom
    @custom = Custom.find(params[:id])
  end
end
