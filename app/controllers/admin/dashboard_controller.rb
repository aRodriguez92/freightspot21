class Admin::DashboardController < Admin::ApplicationController
  def index
    redirect_to admin_intel_intel_analytics_dashboard_path
  end

  def changelog
    @changelogs = Changelog.all.order(version_number: :desc)
  end
end
