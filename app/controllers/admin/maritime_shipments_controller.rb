class Admin::MaritimeShipmentsController < Admin::ApplicationController
  before_action :set_shipment, only: %i[edit show update destroy]
  def index
    @total_shipments =  MaritimeShipment.count
    @maritime_shipments = if params[:status].present?
                            MaritimeShipment.by_statuses(params[:status])
                          elsif params[:status] == 'total_shipments'
                            MaritimeShipment.all
                          else
                            MaritimeShipment.all
                          end
    @total_on_hold = MaritimeShipment.all.where(status: :on_hold).count
    @total_in_transit = MaritimeShipment.all.where(status: :in_transit).count
    @total_at_origin = MaritimeShipment.all.where(status: :at_origin).count
    @total_reached_destination = MaritimeShipment.all.where(status: :reached_destination).count
    @total_completed = MaritimeShipment.all.where(status: :completed).count
    @total_delayed = MaritimeShipment.all.where(status: :delayed).count
  end

  def show; end

  def new
    @shipment = MaritimeShipment.new
  end

  def edit; end

  def create
    @shipment = MaritimeShipment.new(maritime_shipment_params)
    origin = Port.find(params[:maritime_shipment][:origin_id])
    destination = Port.find(params[:maritime_shipment][:destination_id])
    @shipment.origin = origin
    @shipment.destination_id = destination.id
    if @shipment.save
      flash[:success] = t('controllers.shipments.create')
      redirect_to admin_maritime_shipments_path
    else
      Rails.logger.debug @shipment.errors.full_messages.to_s
      render :new
    end
  end

  def update; end

  def destroy; end

  def download_sample
    send_file(
      Rails.public_path.join('multiple_shipment_upload.xlsx'),
      filename: 'multiple_shipment_upload.xlsx',
      type: 'application/vnd.ms-excel'
    )
  end

  def import
    if params[:file].present?
      @importer = MaritimeShipmentImport.new(params[:file])
      @importer.import_data
      if @importer.errors.any?
        flash[:danger] = @importer.errors
      else
        flash[:success] = t('controllers.maritime_shipments.create')
      end
    else
      flash[:error] = t('views.shared.upload.invalid')
    end
    redirect_to admin_maritime_shipments_path
  end

  private

  def set_shipment
    @shipment = MaritimeShipment.find(params[:id])
  end

  def maritime_shipment_params
    params.require(:maritime_shipment).permit(:bl_number, :container_number, :number,
                                              :origin_id, :destination_id, :incoterm_id,
                                              :client_id, :team_id, :shipping_line_id, :operator_id,
                                              :sales_executive_id, :seller_id, :arrival_time,
                                              :earliest_to_latest)
  end
end
