class Admin::ContainersController < Admin::ApplicationController
  before_action :set_container, only: %i[edit show update destroy]
  before_action :set_specifications, only: %i[new edit]

  def index
    @containers = Container.all.order(:name)
  end

  def show; end

  def new
    @container = Container.new
    @specifications.each do |s|
      @container.container_dimensions.build(name: s)
    end
  end

  def edit; end

  def create
    @container = Container.new(container_params)
    if @container.save
      redirect_to admin_containers_path
      flash[:success] = t('controllers.containers.create')
    else
      render :new
    end
  end

  def update
    if @container.update(container_params)
      redirect_to admin_container_path(@container.id)
      flash[:success] = t('controllers.containers.update')
    else
      render :update
    end
  end

  def destroy
    @container.destroy!
    flash[:error] = t('controllers.containers.destroy')
    redirect_to admin_containers_path
  end

  private

  def container_params
    params.require(:container).permit(:id, :code, :name, :description,
                                      container_dimensions_attributes: container_dimensions_attributes)
  end

  def container_dimensions_attributes
    %i[id name metric_value metric_measurement_id imperial_value imperial_measurement_id
       container_id]
  end

  def set_container
    @container = Container.friendly.find(params[:id])
  end

  def set_specifications
    @specifications = ['Tare Weight', 'Payload Capacity', 'Maximum Gross Weight', 'Cubic Capacity',
                       'Internal Length', 'Internal Width', 'Internal Height', 'External Length',
                       'External Width', 'External Height', 'Door Opening Width', 'Door Opening Height']
  end
end
