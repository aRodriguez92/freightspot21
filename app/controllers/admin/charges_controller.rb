class Admin::ChargesController < Admin::ApplicationController
  before_action :set_charge, only: %i[show edit update destroy]
  def index
    @charges = Charge.all.order(:name)
  end

  def show; end

  def new
    @charge = Charge.new
  end

  def edit; end

  def create
    @charge = Charge.new(charge_params)
    respond_to do |format|
      if @charge.save
        format.html { redirect_to admin_charges_path, notice: t('controllers.charges.create') }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @charge.update(charge_params)
        format.html { redirect_to admin_charge_path(@charge.id), notice: t('controllers.charges.update') }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @charge.destroy
        format.html { redirect_to admin_charges_path, notice: t('controllers.charges.destroy') }
      else
        format.html { render :show }
      end
    end
  end

  private

  def charge_params
    params.require(:charge).permit(:code, :name, :active)
  end

  def set_charge
    @charge = Charge.friendly.find(params[:id])
  end
end
