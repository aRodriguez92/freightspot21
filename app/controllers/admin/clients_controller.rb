class Admin::ClientsController < Admin::ApplicationController
  protect_from_forgery with: :null_session
  before_action :set_client, only: %i[show edit update destroy update_readonly
                                      new_address add_address edit_address update_address destroy_address
                                      new_phone show_phone destroy_phone edit_phone update_phone
                                      edit_contact update_contact destroy_contact
                                      add_sender edit_sender update_sender destroy_sender
                                      add_client_product edit_client_product update_client_product destroy_client_product]
  before_action :set_client_for_product, only: %i[new_client_product edit_client_product update_client_product]
  before_action :set_client_for_document, only: %i[new_client_document edit_client_document update_client_document destroy_document]
  before_action :set_sender, only: %i[show_sender edit_sender update_sender destroy_sender]
  before_action :set_client_product, only: %i[edit_client_product update_client_product destroy_client_product]
  before_action :set_client_document, only: %i[edit_client_document update_client_document destroy_client_document
                                               download_client_document]

  before_action :set_readonly, only: %i[show]
  include Utils::Search
  include SharedMethods
  include PhonesMethods
  include AddressesMethods
  include ContactsMethods

  skip_before_action :verify_authenticity_token

  def index
    @clients = Client.order(:name)
    set_default_columns
    @column_names = columns_name
    @all_columns = @clients.all_columns_names
  end

  def show
    @address = Address.new
    @phone = Phone.new
    @contact = Contact.new
    @sender = Sender.new
    @product = ClientProduct.new
    @client.contacts.build.addresses.build
    @is_contact = params[:is_contact]
    @addresses = @client.addresses.order(:name)
    @contacts = @client.contacts.order('principal DESC')
    @senders = @client.senders.order('name DESC')
    @phones = phone_list(@client, @contacts, @senders)
    @products = @client.client_products
    @client_documents = @client.client_documents
  end

  def new
    @client = Client.new
    @client.addresses.build.phones.build
    @client.contacts.build.addresses.build
  end

  def edit; end

  def create
    @client = Client.new(client_params)
    if @client.save!
      redirect_to admin_clients_path
      flash[:success] = t('controllers.clients.create')
    else
      render :new
    end
  end

  def update
    if @client.update(client_params)
      @address = Address.new
      @phone = Phone.new
      @contact = Contact.new
      @sender = Sender.new
      @product = ClientProduct.new
      @response = t('controllers.clients.update')
      render :show do |page|
        page.replace(@client, partial: 'clients/personal_information')
      end
    else
      render :show
    end
  end

  def add_sender
    @sender = Sender.new(sender_params)
    @client = Client.find(@sender.senderable_id)
    respond_to do |format|
      if @sender.save
        @senders = @client.senders
        @response = t('controllers.senders.create')
        @senders = @client.senders.order('name DESC')
        format.js { render layout: false, content_type: 'text/javascript' }
        format.html
      else
        format.js { render :new, content_type: 'text/javascript' }
      end
    end
  end

  def show_sender; end

  def edit_sender
    @sender.phones.build if @sender.phones.blank?
    @button = t('views.shared.buttons.update')
    @senders = @client.senders.order('name DESC')
  end

  def update_sender
    @update = false
    @client = Client.find(@sender.senderable_id)
    respond_to do |format|
      if @sender.update(sender_params)
        @senders = @client.senders.order('name desc')
        @update = true
        @response = t('controllers.senders.update')
        format.js { render layout: false, content_type: 'text/javascript' }
        format.html
      end
    end
  end

  def destroy_sender
    @client = Client.find(@sender.senderable_id)
    @sender.destroy!
    @senders = @client.senders.order('name DESC')
    @response = t('controllers.senders.destroy')
  end

  def new_client_product
    @button = t('views.shared.buttons.save')
    @product = ClientProduct.new
  end

  def add_client_product
    Rails.logger.debug { "aqui guardando #{client_product_params}" }
    Rails.logger.debug params
    @product = ClientProduct.new(client_product_params)
    respond_to do |format|
      if @product.save!
        @products = @client.client_products if @client.client_products.present?
        @response = t('controllers.client_products.create')
        format.js { render layout: false, content_type: 'text/javascript' }
        format.html
      end
    end
  end

  def edit_client_product; end

  def update_client_product
    @update = false
    respond_to do |format|
      if @product.update(client_product_params)
        @products = @client.client_products
        @update = true
        @response = t('controllers.client_products.update')
        format.js { render layout: false, content_type: 'text/javascript' }
        format.html
      end
    end
  end

  def destroy_client_product
    @product.destroy!
    client = Client.find(@product.client_id)
    @products = client.client_products
    @response = t('controllers.client_products.destroy')
  end

  def destroy_client_document
    @client = Client.find(@client_document.client_id)
    @client_document.destroy!
    @response = t('controllers.client_documents.destroy')
    @client_documents = @client.client_documents
  end

  def update_readonly
    if params[:readonly] == 'true'
      @readonly = true
    else
      @readonly = false
    end
    respond_to do |format|
      format.js
    end
  end

  private

  def set_readonly
    if params[:readonly] == 'true'
      @readonly = true
    else
      @readonly = false
    end
  end

  def client_params
    params.require(:client).permit(:name, :rfc, :website, :parent_entity_id, :currency_id, :payment_term_id,
                                   :pay_like, :cfdi_id, :way_pay_id, :credit_limit, :foreign, :contract, :has_credit,
                                   :payment_method_id, :authorized_credit, :logo, :status, :middleman_id, :team_id,
                                   :advanced_parameters,  service_mode_ids: [],
                                                          addresses_attributes: addresses_attributes,
                                                          contacts_attributes: contacts_attributes)
  end

  def sender_params
    params.require(:sender).permit(:id, :senderable_type, :senderable_id, :contact, :name, :rfc, :email,
                                   :address, sender_type_ids: [], phones_attributes: phones_attributes)
  end

  def client_product_params
    params.require(:client_product).permit(:client_id, :product_id, :tariff_fraction)
  end

  def addresses_attributes
    [:id, :addressable_id, :addressable_type, :principal, :name, :street, :interior_number, :outdoor_number, :colony, :zip_code, :city, :country,
     :state_id, :address_type_ids, { phones_attributes: phones_attributes }]
  end

  def contacts_attributes
    [:id, :contactable_id, :contactable_type, :name, :email, :birth_date, :area, :job, :principal, { addresses_attributes: addresses_attributes }]
  end

  def phones_attributes
    %i[id phoneable_type phoneable_id code number extension type_phone principal]
  end

  def set_client
    @client = Client.friendly.find(params[:id])
  end

  def set_sender
    @sender = Sender.find(params[:sender_id])
  end

  def set_client_for_product
    @client = Client.find(params[:id])
  end

  def set_client_product
    @product = ClientProduct.find(params[:product_id])
  end

  def set_client_document
    @client_document = ClientDocument.find(params[:document_id])
  end

  def set_client_for_document
    @client = Client.find(params[:id])
  end

  def set_default_columns
    assign_columns(%w[name website rfc status])
  end

  def assign_columns(columns)
    @column_names = @clients.columns_names(columns)
  end

  def columns_name
    @column_names
  end
end
