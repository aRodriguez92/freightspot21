class Admin::RolesController < Admin::ApplicationController
  before_action :set_role, only: %i[edit show update destroy assign]
  before_action :set_accesses, only: %i[show assign]

  def index
    @roles = Role.all.order(:name)
  end

  def show; end

  def new
    @role = Role.new
  end

  def edit; end

  def create
    @role = Role.new(role_params)
    if @role.save
      flash[:success] = t('controllers.roles.create')
      redirect_to admin_roles_path
    else
      render :new
    end
  end

  def update
    if @role.update(role_params)
      redirect_to admin_role_path(@role.id)
      flash[:success] = t('controllers.roles.update')
    else
      render :new
    end
  end

  def destroy
    if @role.destroy
      flash[:danger] = t('controllers.user.destroy')
      redirect_to admin_roles_path
    else
      render :show
    end
  end

  def assign; end

  private

  def role_params
    params.require(:role).permit(:name)
  end

  def set_role
    @role = Role.friendly.find(params[:id])
  end

  def set_accesses
    @access = AccessRole.where(role_id: @role.id)
  end
end
