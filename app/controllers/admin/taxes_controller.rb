class Admin::TaxesController < Admin::ApplicationController
  before_action :set_tax, only: %i[edit show update destroy]

  def index
    @taxes = Tax.all.order(:name)
  end

  def show; end

  def new
    @tax = Tax.new
  end

  def edit; end

  def create
    @tax = Tax.new(tax_params)
    if @tax.save
      flash[:success] = t('controllers.taxes.create')
      redirect_to admin_taxes_path
    else
      render :new
    end
  end

  def update
    if @tax.update(tax_params)
      flash[:success] = t('controllers.taxes.update')
      redirect_to admin_tax_path(@tax.id)
    else
      render :edit
    end
  end

  def destroy
    if @tax.destroy
      flash[:danger] = t('controllers.taxes.destroy')
      redirect_to admin_taxes_path
    else
      render :show
    end
  end

  private

  def tax_params
    params.require(:tax).permit(:code, :name, :value)
  end

  def set_tax
    @tax = Tax.friendly.find(params[:id])
  end
end
