class Admin::ServicesController < Admin::ApplicationController
  before_action :set_service, only: %i[edit show update destroy]

  # load_and_authorize_resource

  def index
    @services = Service.all
  end

  def show; end

  def new
    @service = Service.new
  end

  def edit; end

  def create
    @service = Service.new(service_params)
    if @service.save
      flash[:success] = I18n.t('controllers.services.create')
      redirect_to admin_services_path
    else
      render :new
    end
  end

  def update
    if @service.update(service_params)
      flash[:success] = I18n.t('controllers.services.update')
      redirect_to admin_services_path
    else
      render :update
    end
  end

  def destroy
    @service.destroy!
    flash[:error] = I18n.t('controllers.services.destroy')
    redirect_to admin_services_path
  end

  private

  def service_params
    params.require(:service).permit(:code, :description, :price, :has_cost, :service_type, :currency_id,
                                    :tax_id, :service_mode_id)
  end

  def set_service
    @service = Service.friendly.find(params[:id])
  end
end
