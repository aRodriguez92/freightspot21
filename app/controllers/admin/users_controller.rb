class Admin::UsersController < Admin::ApplicationController
  before_action :set_user, only: %i[edit show update destroy]
  before_action :allow_without_password, only: [:update]

  def index
    @users = User.order(:username)
  end

  def show; end

  def new
    @user = User.new
    @user.build_employee
  end

  def edit; end

  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        format.html { redirect_to admin_users_path, notice: t('controllers.user.create') }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    form = params[:form]
    if @user.update(user_params)
      flash[:success] = t('controllers.user.update')
      redirect_to admin_user_path(@user.id) if form == 'Employee'
    else
      render :show
    end
  end

  def destroy
    if @user.destroy
      flash[:danger] = t('controllers.user.destroy')
      redirect_to admin_users_path
    else
      render :show
    end
  end

  private

  def user_params
    params.require(:user).permit(:avatar, :username, :name, :email, :password, :password_confirmation,
                                 :admin, :role_id, employee_attributes: employee_attributes)
  end

  def employee_attributes
    %i[
      name last_name birth_date phone extension origin city job join_date mobile id user_id department_id
    ]
  end

  def set_user
    @user = User.friendly.find(params[:id])
  end

  def allow_without_password
    return unless params[:user][:password].blank? && params[:user][:password_confirmation].blank?

    params[:user].delete(:password)
    params[:user].delete(:password_confirmation)
  end
end
