class Admin::ApplicationController < ActionController::Base
  add_flash_types :danger, :info, :warning, :success, :messages

  before_action :authenticate_admin!
  before_action :set_paper_trail_whodunnit
  before_action :set_locale

  layout 'admin'

  def change_language
    locale = params[:locale].to_sym
    session[:locale] = locale if I18n.available_locales.include?(locale)
    redirect_back(fallback_location: admin_root_path)
  end

  protected

  def authenticate_admin!
    unless current_user.present? && !current_user.is_customer?
      flash[:warning] = t('views.home.not_authorization')
      redirect_to new_user_session_path
    end
  end

  def set_locale
    I18n.locale = session[:locale] || I18n.default_locale
  end
end
