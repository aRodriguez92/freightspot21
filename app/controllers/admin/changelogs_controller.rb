class Admin::ChangelogsController < Admin::ApplicationController
  before_action :set_changelog, only: %i[edit show update destroy]

  def index
    @changelogs = Changelog.all.order(version_number: :desc)
  end

  def show; end

  def new
    @changelog = Changelog.new
    @changelog.feature_improvements.build
  end

  def edit; end

  def create
    @changelog = Changelog.new(changelog_params)
    if @changelog.save
      flash[:success] = t('controllers.changelogs.create')
      redirect_to admin_changelogs_path
    else
      render :new
    end
  end

  def update
    if @changelog.update(changelog_params)
      redirect_to admin_changelog_path(@changelog.id)
      flash[:success] = t('controllers.changelogs.update')
    else
      render :update
    end
  end

  def destroy
    if @changelog.destroy
      flash[:danger] = t('controllers.changelogs.destroy')
      redirect_to admin_changelogs_path
    else
      render :show
    end
  end

  private

  def changelog_params
    params.require(:changelog).permit(:id, :version_number, :released_at, :description,
                                      feature_improvements_attributes: feature_improvements_attributes)
  end

  def feature_improvements_attributes
    %i[id changelog_id title description category]
  end

  def set_changelog
    @changelog = Changelog.find(params[:id])
  end
end
