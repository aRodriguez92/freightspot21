class Admin::MaritimeRatesController < Admin::ApplicationController
  def index
    @maritime_fcl_rates = MaritimeRate.all
  end

  def show; end

  def new
    @maritime_fcl_rate = MaritimeRate.new
  end

  def edit; end

  def create; end

  def update; end

  def destroy; end

  def download_sample
    send_file(
      Rails.root.join('public/rates_upload_sample.xlsx'),
      filename: 'rates_upload_sample.xlsx',
      type: 'application/vnd.ms-excel'
    )
  end

  def import
    MaritimeRate.import(params[:file])
  end

  private

  def maritime_rate_params
    params.require(:maritime_rate).permit(:carrier_id)
  end

  def set_maritime_rate
    @rate = Rate.find(params[:id])
  end
end
