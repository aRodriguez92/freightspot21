class Admin::MembershipPlansController < Admin::ApplicationController
  before_action :set_membership_plan, only: %i[show edit update destroy]
  def index
    @membership_plans = MembershipPlan.all
  end

  def show; end

  def new
    @membership_plan = MembershipPlan.new
  end

  def edit; end

  def create
    @membership_plan = MembershipPlan.new(membership_plan_params)
    if @membership_plan.save
      flash[:notice] = t('controllers.membership_plans.create')
      redirect_to admin_membership_plans_path
    else
      render :new
    end
  end

  def update
    if @membership_plan.update(membership_plan_params)
      flash[:notice] = t('controllers.membership_plans.destroy')
      redirect_to admin_membership_plan_path(@membership_plan.id)
    else
      render :edit
    end
  end

  def destroy
    if @membership_plan.destroy
      flash[:success] = t('controllers.membership_plans.destroy')
      redirect_to admin_membership_plans_path
    else
      render :show
    end
  end

  private

  def membership_plan_params
    params.require(:membership_plan).permit(:code, :name, :price, :description, :active)
  end

  def set_membership_plan
    @membership_plan = MembershipPlan.find(params[:id])
  end
end
