class Admin::TeamsController < Admin::ApplicationController
  before_action :set_team, only: %i[show edit update destroy]
  def index
    @teams = Team.order(:name)
  end

  def show; end

  def new
    @team = Team.new
    @team.team_members.build
  end

  def edit; end

  def create
    @team = Team.new(team_params)
    if @team.save
      redirect_to admin_teams_path
    else
      render :new
    end
  end

  def update
    if @team.update(team_params)
      redirect_to admin_team_path(@team.id)
    else
      render :edit
    end
  end

  def destroy
    if @team.destroy
      redirect_to admin_teams_path
    else
      render :show
    end
  end

  private

  def team_params
    params.require(:team).permit(:name, :_destroy, team_members_attributes: team_members_attributes)
  end

  def team_members_attributes
    %i[id employee_id role _destroy]
  end

  def set_team
    @team = Team.friendly.find(params[:id])
  end
end
