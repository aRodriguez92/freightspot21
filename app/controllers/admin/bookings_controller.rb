class Admin::BookingsController < Admin::ApplicationController
  before_action :set_booking, only: %i[edit show update destroy]

  def index
    @bookings = Booking.all
  end

  def show; end

  def new
    @booking = Booking.new
  end

  def edit; end

  def update
    return unless @booking.update(booking_params)

    flash[:success] = t('controllers.bookings.update')
  end

  def destroy
    @booking.destroy!
    flash[:error] = t('controllers.bookings.destroy')
    redirect_to admin_bookings_path
  end

  private

  def booking_params
    params.require(:booking).permit(:id, :status, :movement_type)
  end

  def set_booking
    @booking = Booking.find(params[:id])
  end
end
