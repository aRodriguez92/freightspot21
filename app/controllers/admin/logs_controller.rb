class Admin::LogsController < Admin::ApplicationController
  before_action :set_log, only: %i[show restore]

  def index
    @logs = Audited.audit_class.all
  end

  def show; end

  def restore
    Rails.logger.debug 'PARAMS:'
    Rails.logger.debug { "PARAMS: #{params}" }
    model = @log.auditable_type.constantize
    Rails.logger.debug model
    if model.restore(@log.auditable_id)
      redirect_to admin_logs_path
    else
      render :show
    end
  end

  private

  def set_log
    @log = Audited.audit_class.find(params[:id])
  end
end
