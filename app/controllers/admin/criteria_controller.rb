class Admin::CriteriaController < Admin::ApplicationController
  before_action :set_criterium, only: %i[edit show update destroy]

  # load_and_authorize_resource

  def index
    @criteria = Criterium.all
  end

  def show; end

  def new
    @criterium = Criterium.new
  end

  def edit; end

  def create
    @criterium = Criterium.new(criterium_params)
    if @criterium.save
      redirect_to admin_criteria_path
      flash[:success] = t('controllers.criteria.create')
    else
      render :new
    end
  end

  def update
    if @criterium.update(criterium_params)
      redirect_to admin_criteria_path
      flash[:success] = t('controllers.criteria.update')
    else
      render :edit
    end
  end

  def destroy
    @criterium.destroy!
    flash[:success] = t('controllers.criteria.destroy')
    redirect_to admin_criteria_path
  end

  private

  def criterium_params
    params.require(:criterium).permit(:id, :name, :amount, :currency_id, :validity_start, :validity_end, :remarks,
                                      :advanced_parameters, :origin_id, :destiny_id, :container_id, :status)
  end

  def set_criterium
    @criterium = Criterium.find(params[:id])
  end
end
