class Admin::AccessRolesController < Admin::ApplicationController
  def add_access
    AccessRole.create!(role_id: params[:role_id], access_id: params[:access_id],
                       system_module_id: params[:system_module_id])
    render json: { response: t('controllers.access_roles.create'), type: 'success' }
  end

  def remove_access
    access_role = AccessRole.find_by(role_id: params[:role_id],
                                     access_id: params[:access_id],
                                     system_module_id: params[:system_module_id])
    access_role.destroy!
    render json: { response: t('controllers.access_roles.destroy'), type: 'destroy' }
  end
end
