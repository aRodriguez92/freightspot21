class Admin::PackagingTypesController < Admin::ApplicationController
  before_action :set_packaging, only: %i[edit show update destroy]

  def index
    @packaging_types = PackagingType.all.order(:name)
  end

  def show; end

  def new
    @packaging_type = PackagingType.new
  end

  def edit; end

  def create
    @packaging_type = PackagingType.new(packaging_type_params)
    if @packaging_type.save
      flash[:success] = t('controllers.packaging_types.create')
      redirect_to admin_packaging_types_path
    else
      render :new
    end
  end

  def update
    if @packaging_type.update(packaging_type_params)
      flash[:success] = t('controllers.packaging_types.update')
      redirect_to admin_packaging_type_path(@packaging_type.id)
    else
      render :edit
    end
  end

  def destroy
    if @packaging_type.destroy
      flash[:success] = t('controllers.packaging_types.destroy')
      redirect_to admin_packaging_types_path
    else
      render :show
    end
  end

  private

  def packaging_type_params
    params.require(:packaging_type).permit(:code, :name, :definition)
  end

  def set_packaging
    @packaging_type = PackagingType.friendly.find(params[:id])
  end
end
