class Admin::MaritimeLclRatesController < Admin::ApplicationController
  before_action :set_maritime_lcl_rate, only: %i[show edit update destroy]

  def index
    @maritime_lcl_rates = MaritimeRate.lcl_rates
  end

  def show; end

  def new
    @maritime_lcl_rate = MaritimeRate.new
  end

  def edit; end

  def create
    @maritime_lcl_rate = MaritimeRate.new(maritime_lcl_rate_params)
    @freight_type = FreightType.find_by(code: 'LCL')
    @maritime_lcl_rate.freight_type_id = @freight_type.id
    respond_to do |format|
      if @maritime_lcl_rate.save
        format.html { redirect_to admin_maritime_lcl_rate_path(@maritime_lcl_rate.id), notice: t('controllers.maritime_rates.create') }
        format.json { render :show, status: :created, location: @maritime_lcl_rate }
      else
        format.html { render :new }
        format.json { render json: @maritime_lcl_rate.errors, status: :unprocessable_entity }
      end
    end
  end

  def update; end

  def destroy; end

  def download_sample
    send_file(
      Rails.root.join('public/lcl_rates_upload_sample.xlsx'),
      filename: 'lcl_rates_upload_sample.xlsx',
      type: 'application/vnd.ms-excel'
    )
  end

  def import
    @maritime_lcl_rate_import = MaritimeRateImport.new(params[:file])
    if @maritime_lcl_rate_import.save
      redirect_to admin_maritime_lcl_rates_path, success: t('controllers.maritime_rates.create')
    else
      flash[:danger] = @maritime_lcl_rate_import.errors.full_messages
      redirect_to admin_maritime_lcl_rates_path
    end
  end

  private

  def maritime_lcl_rate_params
    params.require(:maritime_rate).permit(:origin_id, :destination_id, :shipper_id, :shipping_line_id, :cargo_type_id, :transit_time,
                                          :rate_validity_start, :rate_validity_end, :departure_date, :freight_rate, :currency_id, :free_days,
                                          :guarantee_letter, :remarks)
  end

  def set_maritime_lcl_rate
    @maritime_lcl_rate = MaritimeRate.find(params[:id])
  end
end
