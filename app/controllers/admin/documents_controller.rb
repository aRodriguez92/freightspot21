class Admin::DocumentsController < Admin::ApplicationController
  before_action :set_document, only: %i[show edit update destroy]

  def index
    @documents = Document.all.order(:name)
  end

  def show; end

  def new
    @document = Document.new
  end

  def edit; end

  def create
    @document = Document.new(document_params)
    if @document.save
      flash[:success] = t('controllers.documents.create')
      redirect_to admin_documents_path
    else
      render :new
    end
  end

  def update
    if @document.update(document_params)
      flash[:success] = t('controllers.documents.update')
      redirect_to admin_document_path(@document.id)
    else
      render :edit
    end
  end

  def destroy
    if @document.destroy
      flash[:danger] = t('controllers.documents.destroy')
      redirect_to admin_documents_path
    else
      render :show
    end
  end

  private

  def document_params
    params.require(:document).permit(:id, :name, :obligatory, :document_type, :phase, :active, :supplier,
                                     :client)
  end

  def set_document
    @document = Document.find(params[:id])
  end
end
