class Admin::QuotesController < Admin::ApplicationController
  skip_before_action :verify_authenticity_token
  include General
  before_action :set_quote, only: %i[show edit update destroy authorize declined]
  # tet
  def index
    all_quotes = MaritimeQuote.all
    @quotes = get_quotes_for_team(all_quotes)
    @notification_quote_comment = Notification.all
  end

  def show
    @comment = QuoteComment.new
  end

  def edit
    @optional_services = OptionalService.all.group_by(&:category)
    incoterms = Incoterm.all
    principal_incoterms = %w[FOB DPU EXW]
    @incoterms = Array.new
    incoterms.each do |incoterm|
      @incoterms << incoterm if principal_incoterms.include? incoterm.code
    end
    @quote.quote_optional_services.each do |quote_optional_service|
      if quote_optional_service.optional_service.name == 'customs_clearance'
        custom = Custom.find_by(custom: @quote.maritime_rate.destination.name)
        if custom
          if @quote.freight_type.code == 'FCL'
            if @quote.export_or_import? == 'Importación'
              @custom_cost = custom.fcl_impo_rate
            else
              @custom_cost = custom.fcl_expo_rate
            end
          else
            if @quote.service_type.operation_type == 'Importación'
              @custom_cost = custom.lcl_impo_rate
            else
              @custom_cost = custom.lcl_expo_rate
            end
          end
        end
      end
      end
    end

  def update
    preview_changes = MaritimeQuote.find(@quote.id)
    _preview_json =  preview_changes.to_json(include: :maritime_quote_units)
    if @quote.update(quote_params)
      approval_request(current_user, @quote.client.principal_contact, _preview_json, @quote)
      ActivityRegister.create(activity_registerable_type: 'MaritimeQuote', activity_registerable_id: @quote.id, user_id: current_user.id, 
activity: "#{current_user.username} ha solicitado la aprobación de los cambios de la cotización")
      flash[:success] = t('controllers.quotes.update')
      redirect_to admin_quote_path(@quote.id)
    else
      flash[:error] = 'Review the data'
      # print the errors to the development log
      Rails.logger.info(@quote.errors.messages.inspect)
    end
  end

  def destroy
    @quote.incidents << Incident.create(activity: :destroy_quote, comment: params[:destroy_reason])
    @quote.destroy
  end

  def get_quotes_for_team(quotes)
    quotes_for_team ||= []
    quotes.each do |quote|
      if quote.client.team.nil?
      else
        quote.client.team.employees.each do |employee|
          if employee.id == current_user.employee.id
            quotes_for_team << quote
          end
        end
      end
    end
    quotes_for_team
  end

  def authorize
    booking = Booking.new
    booking.maritime_quote_id = @quote.id
    if booking.save
      QuoteNotification.with(quote_notification: @quote).deliver_later(Client.find(@quote.client_id).contacts.map{|contact| contact.user})
      @quote.update(status: :authorized)
      flash[:success] = t('controllers.quotes.authorize')
      redirect_to admin_quotes_path
    else
      flash[:error] = 'Review the data'
    end
  end

  def declined
    @quote.update(quote_params)
    QuoteNotification.with(quote_notification: @quote).deliver_later(Client.find(@quote.client_id).contacts.map{|contact| contact.user})
    @response = t('controllers.quotes.decline')
  end

  private

  def set_quote
    @quote = MaritimeQuote.find(params[:id])
  end

  def initialize_shipment(quote)
    MaritimeShipment.new(quote.attributes.except!('quoted_at', 'maritime_rate_id', 'expired_at', 'payment_term_id',
                                          'organization_id', 'service_type',
                                          'is_active_shipment', 'authorized', 'id', 'status', 'reason_of_declined', 'origin_route_id', 'destination_route_id'))
  end

  def initialize_shipment_units(quote_units)
    shipment_units = []
    quote_units.each do |quote_unit|
      shipment_unit = ShipmentUnit.new(quote_unit.attributes.except('maritime_quote_id', 'service_id'))
      shipment_units << shipment_unit
    end
    shipment_units
  end

  def quote_params
    params.require(:maritime_quote).permit(:id, :operation_type, :service_mode, :organization_id,
                                           :quoted_at, :transportation_method, :frequency, :incoterm_id,
                                           :client_id, :service_type, :transportation_modality, :reason_of_declined,
                                           :operation_type, :danger_item, :guarantee_letter, :status,
                                           :freight_type_id, :description, :declared_value, :incoterm_id,
                                           :origin_route_id, :destination_route_id, :team_id,
                                           maritime_quote_units_attributes: maritime_quote_units_attributes,
                                           quote_optional_services_attributes: quote_optional_services_attributes,
                                           optional_service_ids: [])
  end

  def maritime_quote_units_attributes
    %i[id length height width weight volume number_units packaging_type_id product_id _destroy]
  end

  def quote_optional_services_attributes
    %i[id applied_charge charge]
  end


end
