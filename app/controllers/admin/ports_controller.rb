class Admin::PortsController < Admin::ApplicationController
  before_action :set_port, only: %i[show edit update destroy]

  def index
    @ports = Port.all.order(:name)
  end

  def show; end

  def new
    @port = Port.new
  end

  def edit; end

  def create
    @port = Port.new(port_params)
    respond_to do |format|
      if @port.save
        format.html { redirect_to admin_port_url(@port.id), notice: t('controllers.ports.create') }
        format.json { render :show, status: :created, location: @port }
      else
        format.html { render :new }
        format.json { render json: @port.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @port.update(port_params)
        format.html { redirect_to admin_port_url(@port.id), notice: t('controllers.ports.update') }
        format.json { render :show, status: :ok, location: @port }
      else
        format.html { render :edit }
        format.json { render json: @port.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    if @port.destroy
      flash[:danger] = t('controllers.ports.destroy')
      redirect_to admin_ports_path
    else
      render :show
    end
  end

  private

  def port_params
    params.require(:port).permit(:id, :identifier, :name, :country_id, :city, :transportation_method, :latitude, :longitude, :inactive)
  end

  def set_port
    @port = Port.friendly.find(params[:id])
  end
end
