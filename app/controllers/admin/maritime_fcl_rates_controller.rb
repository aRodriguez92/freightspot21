class Admin::MaritimeFclRatesController < Admin::ApplicationController
  before_action :set_maritime_fcl_rate, only: %i[show edit update destroy]

  def index
    @maritime_fcl_rates = MaritimeRate.fcl_rates
  end

  def show; end

  def new
    @maritime_fcl_rate = MaritimeRate.new
  end

  def create
    @maritime_fcl_rate = MaritimeRate.new(maritime_fcl_rate_params)
    @freight_type = FreightType.find_by(code: 'FCL')
    origin = Port.find(params[:maritime_rate][:origin_id])
    destination = Port.find(params[:maritime_rate][:destination_id])
    @maritime_fcl_rate.origin = origin
    @maritime_fcl_rate.destination = destination
    @maritime_fcl_rate.freight_type_id = @freight_type.id
    respond_to do |format|
      if @maritime_fcl_rate.save
        format.html { redirect_to admin_maritime_fcl_rate_path(@maritime_fcl_rate.id), notice: t('controllers.maritime_rates.create') }
        format.json { render :show, status: :created, location: @maritime_fcl_rate }
      else
        puts @maritime_fcl_rate.errors.full_messages.to_s
        format.html { render :new }
        format.json { render json: @maritime_fcl_rate.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit; end

  def update; end

  def destroy; end

  def download_sample
    send_file(
      Rails.public_path.join('fcl_rates_upload_sample.xlsx'),
      filename: 'fcl_rates_upload_sample.xlsx',
      type: 'application/vnd.ms-excel'
    )
  end

  def import
    if params[:file].present?
      @importer = MaritimeRateImport.new(params[:file])
      @importer.import_data
      if @importer.errors.any?
        puts 'SI errores'
        puts @importer.errors.to_s
        flash[:danger] = @importer.errors
      else
        flash[:success] = t('controllers.maritime_rates.create')
      end
    else
      flash[:error] = 'Please upload a valid file'
    end
    redirect_to admin_maritime_fcl_rates_path
  end

  private

  def maritime_fcl_rate_params
    params.require(:maritime_rate).permit(:origin_id, :destination_id, :shipper_id, :shipping_line_id, :container_id,
                                          :cargo_type_id, :transit_time, :rate_validity_start, :rate_validity_end, :departure_date, :freight_rate,
                                          :currency_id, :free_days, :guarantee_letter, :remarks)
  end

  def set_maritime_fcl_rate
    @maritime_fcl_rate = MaritimeRate.find(params[:id])
  end
end
