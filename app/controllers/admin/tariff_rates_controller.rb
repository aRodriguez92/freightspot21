class Admin::TariffRatesController < Admin::ApplicationController
  before_action :set_tariff_rate, only: %i[edit show update destroy]

  def index
    @tariff_rates = TariffRate.all
  end

  def show; end

  def new
    @tariff_rate = TariffRate.new
  end

  def edit; end

  def create
    @tariff_rate = TariffRate.new(tariff_rate_params)
    respond_to do |format|
      if @tariff_rate.save
        format.html { redirect_to admin_tariff_rate_url(@tariff_rate.id), notice: t('controllers.tariff_rates.create') }
        format.json { render :show, status: :created, location: @tariff_rate }
      else
        format.html { render :new }
        format.json { render json: @tariff_rate.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @tariff_rate.update(tariff_rate_params)
        format.html { redirect_to admin_tariff_rate_url(@tariff_rate.id), notice: t('controllers.tariff_rates.update') }
        format.json { render :show, status: :ok, location: @tariff_rate }
      else
        format.html { render :edit }
        format.json { render json: @tariff_rate.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    if @tariff_rate.destroy
      flash[:danger] = t('controllers.tariff_rates.destroy')
      redirect_to admin_tariff_rates_path
    else
      render :show
    end
  end

  private

  def tariff_rate_params
    params.require(:tariff_rate).permit(:id, :hs_code, :nico, :description, :umt, :import_tax, :export_tax)
  end

  def set_tariff_rate
    @tariff_rate = TariffRate.find(params[:id])
  end
end
