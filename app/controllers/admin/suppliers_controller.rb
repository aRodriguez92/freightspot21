class Admin::SuppliersController < Admin::ApplicationController
  protect_from_forgery with: :null_session
  before_action :set_supplier, only: %i[show edit update destroy update_readonly
                                        new_address add_address edit_address update_address destroy_address
                                        new_phone show_phone destroy_phone edit_phone update_phone
                                        edit_contact update_contact destroy_contact]

  before_action :set_readonly, only: %i[show]
  include Utils::Search
  include SharedMethods
  include PhonesMethods
  include AddressesMethods
  include ContactsMethods

  skip_before_action :verify_authenticity_token

  def index
    @suppliers = Supplier.order(:name)
    set_default_columns
    @column_names = columns_name
    @all_columns = @suppliers.all_columns_names
  end

  def show
    @address = Address.new
    @phone = Phone.new
    @contact = Contact.new
    @supplier.contacts.build.addresses.build
    @is_contact = params[:is_contact]
    @addresses = @supplier.addresses.order(:name)
    @contacts = @supplier.contacts.order('principal DESC')
    @phones = PhoneInfo::PhoneNumberFetcher.get_phone_numbers(@supplier)
  end

  def new
    @supplier = Supplier.new
    @supplier.addresses.build.phones.build
    @supplier.contacts.build.addresses.build
  end

  def edit; end

  def create
    @supplier = Supplier.new(supplier_params)
    if @supplier.save!
      redirect_to admin_suppliers_path
      flash[:success] = t('controllers.suppliers.create')
    else
      render :new
    end
  end

  def update
    if @supplier.update(supplier_params)
      @address = Address.new
      @phone = Phone.new
      @contact = Contact.new
      @sender = Sender.new
      @response = t('controllers.suppliers.update')
      render :show do |page|
        page.replace(@supplier, partial: 'suppliers/personal_information')
      end
    else
      render :show
    end
  end

  def destroy; end

  def update_readonly
    @readonly = params[:readonly] == 'true'
    respond_to do |format|
      format.js
    end
  end

  private

  def set_readonly
    @readonly = params[:readonly] == 'true'
  end

  def supplier_params
    params.require(:supplier).permit(:name, :rfc, :website, :parent_entity_id, :currency_id, :payment_term_id,
                                     :pay_like, :cfdi_id, :way_pay_id, :credit_limit, :foreign, :contract, :has_credit,
                                     :payment_method_id, :authorized_credit, :logo, :status, :middleman_id, :team_id,
                                     :advanced_parameters, service_mode_ids: [],
                                                           addresses_attributes: addresses_attributes,
                                                           contacts_attributes: contacts_attributes)
  end

  def addresses_attributes
    %i[id addressable_id addressable_type principal name street interior_number outdoor_number colony zip_code city
       country state_id address_type_ids { phones_attributes: phones_attributes }]
  end

  def contacts_attributes
    %i[id contactable_id contactable_type name email birth_date area job principal { addresses_attributes: addresses_attributes }]
  end

  def phones_attributes
    %i[id phoneable_type phoneable_id code number extension type_phone principal]
  end

  def set_supplier
    @supplier = Supplier.find(params[:id])
  end

  def set_default_columns
    assign_columns(%w[name website rfc status])
  end

  def assign_columns(columns)
    @column_names = @suppliers.columns_names(columns)
  end

  def columns_name
    @column_names
  end
end
