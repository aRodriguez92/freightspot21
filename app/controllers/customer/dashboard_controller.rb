class Customer::DashboardController < Customer::ApplicationController
  before_action :set_client, only: %i[dashboard]

  def dashboard
    @quotes = []
    favorites = Favorite.where('favoritable_type = ? AND user_id = ?', 'MaritimeQuote', current_user.id).last(10)
    favorites.each do |favorite|
      @quotes << MaritimeQuote.find(favorite.favoritable_id)
    end
    @bookings = Booking.joins(:maritime_quote).where(maritime_quotes: { client_id: @client.id })
    @cft = MaritimeQuote.count_by_freight_type
  end

  private

  def set_client
    @client = current_user.contact.contactable
  end
end
