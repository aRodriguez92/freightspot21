class Customer::MaritimeFclQuotesController < Customer::ApplicationController
  include Wicked::Wizard
  steps :optional_services

  before_action :initialize_quote, only: %i[new]
  before_action :set_quote, only: %i[show update tariffs]
  before_action :set_client, only: %i[index new create]

  def index
    @quotes = MaritimeQuote.fcl_maritime_quotes.where(client_id: @client.id)
  end

  def new
    @client = current_user.contact.contactable
    @ports = Port.where(transportation_method: 'maritime')
    @freight_types = []
  end

  def create
    @quote = MaritimeQuote.new(quote_params)
    maritime_fcl_rates = MaritimeRate.where(origin_id: @quote.origin_route_id.to_i, destination_id: @quote.destination_route_id.to_i).exists?
    unless maritime_fcl_rates.blank?
      if @quote.save
        ActivityRegister.create(activity_registerable_type: 'MaritimeQuote', activity_registerable_id: @quote.id, user_id: current_user.id, activity: "#{current_user.username} ha creado una cotización")
        @quote.client.team.employees.each do |employee|
          QuoteNotification.with(quote_notification: @quote).deliver(employee.user)
        end
        session[:quote_id] = @quote.id
        redirect_to tariffs_customer_maritime_fcl_quotes_path
      else
        render :new
      end
    else
      flash[:error] = ' No existen tarifas'
      render :new
    end
  end

  def show
    case step
    when :optional_services
      @quote = MaritimeQuote.find(params[:quote_id])
      @maritime_fcl_rate = MaritimeRate.find(params[:rate_id])
      @quote.maritime_rate = @maritime_fcl_rate
      params[:rate_ids].each do |rate_id|
        @quote.maritime_rates << MaritimeRate.find(rate_id)
      end
      @quote.save
      @optional_services = get_optional_services
      @quote.deliveries.build
      @quote.pickups.build
      @freight_rate = params[:freight_rate]
      @total_cost = params[:freight_rate]
      wizard_path(:optional_services)
    end
    if params[:id] == "wicked_finish"
      ActivityRegister.create(activity_registerable_type: 'MaritimeQuote', activity_registerable_id: @quote.id, user_id: current_user.id, activity: "#{current_user.username} ha añadido servicios adicionales a la cotización")
      # @quote.employee = Employee.find_by(job: 'Boss')
      @quote.status = :pending
      @quote.save
      redirect_to customer_quotes_path
    else
      render_wizard
    end
  end

  def update
    if @quote.update(quote_params)
      if step == :quote_units
        @quote.description = @quote.description ? @quote.description : ""
        @quote.maritime_quote_units.each do |quote_unit|
          @quote.description += quote_unit.product.name + "\n"
        end
      end
      render_wizard @quote
    else
      # print the errors to the development log
      Rails.logger.info(@quote.errors.messages.inspect)
    end
  end

  def tariffs
    @maritime_fcl_rates = Array.new
    @minimal_cost = Hash.new
    @cost_benefit = Hash.new
    @minimal_cost[:rate_ids] = Array.new
    @cost_benefit[:rate_ids] = Array.new

    if @quote.freight_type == FreightType.find_by_code('FCL')
      #flujo de fcl
      all_maritime_fcl_rates = MaritimeRate.
        where(origin_id: @quote.origin_route_id, destination_id: @quote.destination_route_id).
        where('rate_validity_end > ? ', 7.days.from_now).
        where(freight_type_id: @quote.freight_type).
        group_by { |x| [x.shipping_line_id, x.departure_date] }

      all_maritime_fcl_rates.each do |item|
        cost = 0.0
        maritime_fcl_rate = Hash.new
        maritime_fcl_rate[:rate_ids] = Array.new
        @quote.maritime_quote_units.each do |maritime_quote_unit|
          item[1].each do |maritime_rate|
            if maritime_quote_unit.container_id == maritime_rate.container_id
              cost = cost + (maritime_rate.freight_rate * maritime_quote_unit.number_units)
              maritime_fcl_rate[:rate_ids] << maritime_rate.id
            end
          end
        end
        if maritime_fcl_rate[:rate_ids].length == @quote.maritime_quote_units.length
          maritime_fcl_rate[:maritime_rate] = item[1].first
          maritime_fcl_rate[:maritime_rate].freight_rate = cost
          @maritime_fcl_rates << maritime_fcl_rate
        end
      end

    else
      maritime_fcl_rates = MaritimeRate.
        where(origin_id: @quote.origin_route_id, destination_id: @quote.destination_route_id).
        where('rate_validity_end > ? ', 7.days.from_now).
        where(freight_type_id: @quote.freight_type).uniq

      maritime_fcl_rates.each do |item|
        maritime_fcl_rate = Hash.new
        maritime_fcl_rate[:rate_ids] = Array.new
        maritime_fcl_rate[:maritime_rate] = item
        maritime_fcl_rate[:rate_ids] << item.id
        @maritime_fcl_rates << maritime_fcl_rate
      end
    end
    unless @maritime_fcl_rates.empty?
    @maritime_fcl_rates.flatten.sort_by{ |rate| rate[:maritime_rate].freight_rate}

    @minimal_cost[:maritime_rate] = @maritime_fcl_rates.first[:maritime_rate]
    @minimal_cost[:rate_ids] = @maritime_fcl_rates.first[:rate_ids]
    @cost_benefit[:maritime_rate] = @maritime_fcl_rates.second[:maritime_rate] if @maritime_fcl_rates.second
    @cost_benefit[:rate_ids] = @maritime_fcl_rates.second[:rate_ids] if @maritime_fcl_rates.second
    end

  end

  def search_service_type_by_incoterm
    puts "search by incoterm"
    if params[:operation_type] != 'national'
      service_type = {}
      @service_type = ServiceType.where(incoterm_id: params[:incoterm_id], operation_type: params[:operation_type]).first unless params[:incoterm_id].nil?
      service_type[:name] = t("enums.quote.type_service.#{@service_type.name}")
    end
    respond_to do |format|
      format.json { render json: service_type }
    end
  end

  def search_service_type_by_operation_type
    puts "Search by operation type"
    if params[:operation_type] != 'national'
      service_type = {}
      @service_type = ServiceType.where(incoterm_id: params[:incoterm_id], operation_type: params[:operation_type]).first unless params[:incoterm_id].nil?
      service_type[:name] = t("enums.quote.type_service.#{@service_type.name}")
    end
    respond_to do |format|
      format.json { render json: service_type }
    end
  end

  private

  def get_optional_services
    optional_services = OptionalService.all
    optional_services_options = Array.new
    optional_services.each do |optional_service|
      if @quote.maritime_rate.origin.country.code != 'MX' and optional_service.name == 'collection_at_origin'
      else

        optional_services_options << optional_service
      end
    end
    optional_services_options.group_by(&:category)

  end

  def finish_wizard_path
    redirect_to customer_quotes_path
  end

  def set_client
    @client = current_user.contact.contactable
  end

  def set_quote
    @quote = MaritimeQuote.find_by_id(session[:quote_id])
  end

  def quote_params
    params.require(:maritime_quote).permit(:id, :operation_type, :service_mode, :organization_id,
                                           :quoted_at, :transportation_method, :frequency, :incoterm_id,
                                           :client_id, :service_type, :transportation_modality, :delivery_address,
                                           :operation_type, :danger_item, :guarantee_letter, :total_cost,
                                           :freight_type_id, :description, :declared_value, :collection_address,
                                           :origin_route_id, :destination_route_id, :_destroy,
                                           maritime_quote_units_attributes: maritime_quote_units_attributes,
                                           deliveries_attributes: deliveries_attributes,
                                           pickups_attributes: pickups_attributes,
                                           optional_service_ids: [])
  end

  def maritime_quote_units_attributes
    %i[id length height width weight volume number_units packaging_type_id product_id container_id _destroy]
  end

  def deliveries_attributes
    %i[id street interior_number outdoor_number colony city state country zip_code]
  end

  def pickups_attributes
    %i[id street interior_number outdoor_number colony city state country zip_code]
  end

  def quote_optional_services
    %i[id optional_service_id applied_charge]
  end

  def initialize_quote
    @quote = MaritimeQuote.new
  end
end
