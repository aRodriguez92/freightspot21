class Customer::FavoritesController < Customer::ApplicationController
  def edit; end

  def update
    favorite = Favorite.where(favoritable_id: params[:favoritable_id] ,favoritable_type: params[:favoritable_type], user_id: current_user.id)
    if favorite == []
      # Create favorite
      Favorite.create!(favoritable_id: params[:favoritable_id] ,favoritable_type: params[:favoritable_type], user_id: current_user.id)
      @favorite_exists = true
    else
      # Delete favorite
      favorite.destroy_all
      @favorite_exists = false
    end

    if params[:favoritable_type] == 'MaritimeQuote'
      redirect_to customer_quotes_path
    end

    respond_to do |format|
      format.html {}
      format.js {}
    end
  end
end
