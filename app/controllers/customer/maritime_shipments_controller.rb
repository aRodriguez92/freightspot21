class Customer::MaritimeShipmentsController < Customer::ApplicationController
  before_action :set_shipment, only: %i[edit show update destroy]
  def index
    @total_shipments =  MaritimeShipment.count
    @maritime_shipments = if params[:status].present?
                            MaritimeShipment.by_statuses(params[:status])
                          elsif params[:status] == 'total_shipments'
                            MaritimeShipment.all
                          else
                            MaritimeShipment.all
                          end
    @total_on_hold = MaritimeShipment.all.where(status: :on_hold).count
    @total_in_transit = MaritimeShipment.all.where(status: :in_transit).count
    @total_at_origin = MaritimeShipment.all.where(status: :at_origin).count
    @total_reached_destination = MaritimeShipment.all.where(status: :reached_destination).count
    @total_completed = MaritimeShipment.all.where(status: :completed).count
    @total_delayed = MaritimeShipment.all.where(status: :delayed).count
  end

  def show; end

  private
  def set_shipment
    @shipment = MaritimeShipment.find(params[:id])
  end
end
