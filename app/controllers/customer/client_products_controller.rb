class Customer::ClientProductsController < Customer::ApplicationController
  before_action :set_client, only: %i[index new create edit update]
  before_action :set_client_product, only: %i[show edit update destroy]

  def index
    @client_products = ClientProduct.where(client_id: @client.id)
  end

  def show; end

  def new
    @client_product = ClientProduct.new
  end

  def edit; end

  def create
    @client_product = ClientProduct.new(client_product_params)
    if @client_product.save
      flash[:success] = 'Product was successfully created'
      redirect_to customer_client_products_path
    else
      render :new
    end
  end

  def update
    if @client_product.update(client_product_params)
      flash[:success] = t('controllers.client_products.update')
      redirect_to customer_client_products_path
    else
      render :edit
    end
  end

  def destroy
    if @client_product.destroy!
      flash[:success] = t('controllers.client_products.destroy')
      redirect_to customer_client_products_path
    else
      render :show
    end
  end

  private

  def set_client_product
    @client_product = ClientProduct.find(params[:id])
  end

  def set_client
    client_id = current_user.contact.contactable_id
    @client = Client.find(client_id)
  end

  def client_product_params
    params.require(:client_product).permit(:client_id, :product_id, :hs_code, :description, :tariff_fraction, :image)
  end
end
