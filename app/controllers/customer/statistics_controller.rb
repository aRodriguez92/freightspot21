class Customer::StatisticsController < Customer::ApplicationController
  def index; end

  def count_by_freight_type
    @cft = MaritimeQuote.count_by_freight_type
    respond_to do |format|
      format.json { render json: @cft }
    end
  end

  def count_by_month
    @cbm = MaritimeQuote.count_by_month
    respond_to do |format|
      format.json { render json: @cbm }
    end
  end

  def count_by_year
    @cby = MaritimeQuote.count_by_year
    respond_to do |format|
      format.json { render json: @cby }
    end
  end

  def count_by_status
    @cbs = MaritimeQuote.count_by_status
    respond_to do |format|
      format.json { render json: @cbs }
    end
  end
end
