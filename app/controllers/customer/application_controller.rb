class Customer::ApplicationController < ActionController::Base
  before_action :authenticate_customer!
  before_action :set_locale

  layout 'customer'

  def change_language
    locale = params[:locale].to_sym
    session[:locale] = locale if I18n.available_locales.include?(locale)
    redirect_back(fallback_location: admin_root_path)
  end

  protected

  def authenticate_customer!
    unless current_user.present? && current_user.is_customer?
      flash[:warning] = 'No tienes autorización para entrar en esa sección'
      redirect_to new_user_session_path
    end
  end

  def set_locale
    I18n.locale = session[:locale] || I18n.default_locale
  end
end
