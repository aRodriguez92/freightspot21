class Customer::QuotesController < Customer::ApplicationController
  before_action :initialize_quote, only: %i[new update]
  before_action :set_quote, only: %i[show tariffs approvals approve reject]
  before_action :set_client, only: %i[index new create]
  before_action :set_approval, only: %i[approvals approve reject]

  def index
    @quotes = MaritimeQuote.where(client_id: @client.id)
  end

  def new
    @client = current_user.contact.contactable
    maritime_rates = MaritimeRate.where('rate_validity_end >= ? ', 7.days.from_now)
    @origin_ports = [] | maritime_rates.map { |rate| rate.origin }.uniq
    @destination_ports = [] | MaritimeRate.where(origin_id: @quote.origin_route_id || maritime_rates.first.origin).where('rate_validity_end > ? ', 7.days.from_now).map { |rate| rate.destination }.uniq unless maritime_rates.blank?
    @freight_types = []
    @containers = Container.all
=begin
    maritimes = MaritimeRate.
      where(origin_id: @origin_ports.first, destination_id: @destination_ports.first).
      where('rate_validity_end > ? ', 7.days.from_now).
      joins(:container)

    puts "las tarifas"
    p maritimes

    maritimes.each do |rate|
      puts "seteando contenedores"
      p rate.container
      @containers << rate.container
    end
=end
  end

  def create
    @quote = Quote.new(quote_params)
    tariffs = Tariff.joins(:rates).where(rates: { origin_port_id: @quote.origin_route.origin_port_id,
                                                  discharge_port_id: @quote.destiny_route.destiny_port_id },
                                         tariffs: { service_mode: @quote.service_mode }).exists?
    unless tariffs.blank?
      if @quote.save
        session[:quote_id] = @quote.id
        redirect_to tariffs_customer_quotes_path
      else
        render :new
      end
    else
      flash[:error] = ' No existen tarifas'
      render :new
    end
  end

  def show
    @comment = QuoteComment.new
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "#{t('views.admin.quotes.show.title')} - #{@quote.number}", template: 'customer/quotes/quote.html.erb', formats: [:html], disposition: :attachment, layout: 'pdf'
      end
    end
  end

  def update
    @quote = MaritimeQuote.find(params[:maritime_quote][:id])
    if @quote.update(quote_params)
      redirect_to tariffs_customer_maritime_fcl_quotes_path
    else
      render :new
    end
  end

  def search_freight_types
    service_mode_variable = params[:service_mode]
    service_mode = ServiceMode.find_by_variable(service_mode_variable)
    @freight_types = FreightType.where('service_mode_id = ?', service_mode.id)
    respond_to do |format|
      format.json { render json: @freight_types }
    end
  end

  def search_service_type_by_operation_type
    if params[:operation_type] != 'national'
      service_type = {}
      @service_type = ServiceType.where(incoterm_id: params[:incoterm_id], operation_type: params[:operation_type]).first unless params[:incoterm_id].nil?
      service_type[:name] = t("enums.quote.type_service.#{@service_type.name}")
    end
    respond_to do |format|
      format.json { render json: service_type }
    end
  end

  def get_port_coordinates
    port_coordinates = {}
    port = Port.find(params[:port_id])
    port_coordinates[:latitude] = port.latitude
    port_coordinates[:longitude] = port.longitude
    respond_to do |format|
      format.json { render json: port_coordinates }
    end
  end

  def get_optional_service
    optional_service = Hash.new
    service = OptionalService.find(params[:optional_service_id])
    optional_service[:id] =  service.id
    optional_service[:name] =  service.name
    optional_service[:name_label] =  service.name_label
    optional_service[:category] = service.category
    optional_service[:cost] =  service.name == 'customs_clearance' ? get_customs_clearance_cost :  service.cost
    respond_to do |format|
      format.json { render :json => optional_service }
    end
  end

  def get_customs_clearance_cost
    @quote = MaritimeQuote.find(params[:quote_id])
    custom = Custom.find_by(custom: @quote.maritime_rate.destination.name)
    if custom
      if @quote.freight_type.code == 'FCL'
        if @quote.export_or_import? == 'Importación'
          customs_clearance_cost = custom.fcl_impo_rate
        else
          customs_clearance_cost = custom.fcl_expo_rate
        end
      else
        if @quote.service_type.operation_type == 'Importación'
          customs_clearance_cost = custom.lcl_impo_rate
        else
          customs_clearance_cost = custom.lcl_expo_rate
        end
      end
    end
    customs_clearance_cost
  end

  def get_destination_ports
    puts 'Si entro a buscar los puertos'
    destination_ports = MaritimeRate.where(origin_id: params[:origin_port]).where('rate_validity_end > ? ', 7.days.from_now)
                                    .map { |rate| rate.destination }.uniq
    respond_to do |format|
      format.json { render :json => destination_ports }
    end
  end

  def set_freight_type
    session['freight_type'] = params[:transportation_modality]
  end

  def approvals
    puts '  APROBARD: '
    puts "  APROBARD: #{@approval.id}"
    @recent_changes = JSON.parse(@approval.recent_changes, symbolize_names: true)
    @preview_changes = JSON.parse(@approval.preview_changes, symbolize_names: true)
    puts 'PREVIEW CHANGES'
    puts "PREVIEW CHANGES: #{@preview_changes}"
    puts 'RECENT CHANGES'
    puts "RECENT CHANGES: #{@recent_changes}"
  end

  def approve
    if @approval.update(approved: true)
      redirect_to customer_quotes_path
      ActivityRegister.create(activity_registerable_type: 'MaritimeQuote', activity_registerable_id: @quote.id, user_id: current_user.id, activity: "#{current_user.username} ha aprobado los cambios de la cotización")
    end
  end

  def reject
    json_preview_changes = @approval.preview_changes
    if @approval.update(approved: false)
      preview_changes_object(json_preview_changes)
      redirect_to customer_quotes_path
      ActivityRegister.create(activity_registerable_type: 'MaritimeQuote', activity_registerable_id: @quote.id, user_id: current_user.id, activity: "#{current_user.username} ha rechazado los cambios de la cotización")
    end
  end

  def preview_changes_object(json_preview_changes)
    preview_changes = JSON.parse(json_preview_changes, symbolize_names: true)
    preview_changes_class = MaritimeQuote.find(preview_changes[:id].to_i)
    preview_changes_class.description = preview_changes[:description]
    preview_changes_class.declared_value = preview_changes[:declared_value]
    preview_changes_class.total_pieces = preview_changes[:total_pieces]
    preview_changes_class.total_weight = preview_changes[:total_weight]
    preview_changes_class.total_volume = preview_changes[:total_volume]
    preview_changes_class.total_chargeable_weight = preview_changes[:total_chargeable_weight]
    preview_changes_class.total_cost = preview_changes[:total_cost]
    preview_changes_class.save
    preview_changes_class
  end

  private

  def finish_wizard_path
    redirect_to customer_quotes_path
  end

  def set_client
    @client = current_user.contact.contactable
  end

  def set_quote
    @quote = MaritimeQuote.find(params[:id])
  end

  def get_quote
    @quote
  end

  def set_approval
    puts 'SET APPROVAL'
    @approval = Approval.find(params[:approval_id])
  end

  def quote_params
    params.require(:maritime_quote).permit(:id, :operation_type, :service_mode, :organization_id,
                                           :quoted_at, :transportation_method, :frequency, :incoterm_id,
                                           :client_id, :service_type, :transportation_modality,
                                           :operation_type, :danger_item, :guarantee_letter,
                                           :freight_type_id, :description, :declared_value,
                                           :origin_route_id, :destination_route_id, :_destroy,
                                           maritime_quote_units_attributes: maritime_quote_units_attributes,
                                           optional_service_ids: [])
  end

  def maritime_quote_units_attributes
    %i[id length height width weight volume number_units packaging_type_id product_id container_id _destroy]
  end

  def quote_units_attributes
    %i[id length height width weight volume number_units packaging_type_id product_id container_id _destroy]
  end

  def quote_route_attributes
    %i[id discharge_port_id origin_port_id]
  end

  def origin_route_attributes
    %i[id quote_id origin_port_id origin latitude longitude]
  end

  def destiny_route_attributes
    %i[id quote_id destiny_port_id destiny latitude longitude]
  end

  def quote_optional_services
    %i[id optional_service_id applied_charge]
  end

  def initialize_quote
    if params[:id]
      @quote = MaritimeQuote.find(params[:id])
    else
      @quote = MaritimeQuote.new
      @quote.maritime_quote_units.build
    end
    p @quote
  end
end