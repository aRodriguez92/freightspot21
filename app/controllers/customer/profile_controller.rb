class Customer::ProfileController < Customer::ApplicationController
  before_action :set_user, only: %i[edit update]
  before_action :set_role, only: %i[edit]

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user.update(user_params)
  end

  private

  def user_params
    params.require(:user).permit(:avatar, :username, :name, :password, :password_confirmation)
  end

  def set_role
    @role = Role.find_by(name: 'Cliente')
  end

  def set_user
    @user = User.friendly.find(params[:id])
  end
end
