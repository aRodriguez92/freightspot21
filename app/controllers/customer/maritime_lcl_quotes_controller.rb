class Customer::MaritimeLclQuotesController < Customer::ApplicationController
  include Wicked::Wizard
  steps :optional_services

  before_action :initialize_quote, only: %i[new]
  before_action :set_quote, only: %i[show update tariffs]
  before_action :set_client, only: %i[index new create]

  def index
    @quotes = MaritimeQuote.lcl_maritime_quotes.where(client_id: @client.id)
  end

  def new
    @client = current_user.contact.contactable
    @ports = Port.where(transportation_method: 'maritime')
    @freight_types = []
  end

  def create
    @quote = MaritimeQuote.new(quote_params)
    maritime_fcl_rates = MaritimeRate.where(origin_id: @quote.origin_route_id.to_i, destination_id: @quote.destination_route_id.to_i).exists?
    unless maritime_fcl_rates.blank?
      if @quote.save
        ActivityRegister.create(activity_registerable_type: 'MaritimeQuote', activity_registerable_id: @quote.id, user_id: current_user.id, activity: "#{current_user.username} ha creado una cotización")
        @quote.client.team.employees.each do |employee|
          QuoteNotification.with(quote_notification: @quote).deliver(employee.user)
        end
        session[:quote_id] = @quote.id
        redirect_to tariffs_customer_maritime_fcl_quotes_path
      else
        render :new
      end
    else
      flash[:error] = ' No existen tarifas'
      render :new
    end
  end

  def show
    case step
    when :optional_services
      @quote = MaritimeQuote.find(params[:quote_id])
      @maritime_fcl_rate = MaritimeRate.find(params[:rate_id])
      @quote.maritime_rate = @maritime_fcl_rate
      @quote.save
      @optional_services = get_optional_services
      @total_cost = @quote.maritime_rate.freight_rate
      wizard_path(:optional_services)
    end
    if params[:id] == "wicked_finish"
      ActivityRegister.create(activity_registerable_type: 'MaritimeQuote', activity_registerable_id: @quote.id, user_id: current_user.id, activity: "#{current_user.username} ha añadido servicios adicionales a la cotización")
      # @quote.employee = Employee.find_by(job: 'Boss')
      @quote.status = :pending
      @quote.save
      redirect_to customer_quotes_path
    else
      render_wizard
    end
  end

  def update
    if @quote.update(quote_params)
      if step == :quote_units
        @quote.description = @quote.description ? @quote.description : ""
        @quote.maritime_quote_units.each do |quote_unit|
          @quote.description += quote_unit.product.name + "\n"
        end
      end
      render_wizard @quote
    else
      # print the errors to the development log
      Rails.logger.info(@quote.errors.messages.inspect)
    end
  end

  def tariffs
    rates ||= []
    #@maritime_fcl_rates = MaritimeRate.where('origin_id = ? AND destination_id = ?', @quote.origin_route_id, @quote.destination_route_id)
    @maritime_fcl_rates = MaritimeRate.
      where(origin_id: @quote.origin_route_id, destination_id: @quote.destination_route_id).
      where('rate_validity_end > ? ',  7.days.from_now)
    @maritime_fcl_rates.each do |tariff|
      rates << tariff
    end
    data = rates.flatten.sort_by { |rate| [rate.freight_rate, rate.transit_time] }
    @minimal_cost = data.first
    @cost_benefit = data.second
  end

  def search_service_type_by_incoterm
    puts "search by incoterm"
    if params[:operation_type] != 'national'
      service_type = {}
      @service_type = ServiceType.where(incoterm_id: params[:incoterm_id], operation_type: params[:operation_type]).first unless params[:incoterm_id].nil?
      service_type[:name] = t("enums.quote.type_service.#{@service_type.name}")
    end
    respond_to do |format|
      format.json { render json: service_type }
    end
  end

  def search_service_type_by_operation_type
    puts "Search by operation type"
    if params[:operation_type] != 'national'
      service_type = {}
      @service_type = ServiceType.where(incoterm_id: params[:incoterm_id], operation_type: params[:operation_type]).first unless params[:incoterm_id].nil?
      service_type[:name] = t("enums.quote.type_service.#{@service_type.name}")
    end
    respond_to do |format|
      format.json { render json: service_type }
    end
  end

  private

  def get_optional_services
    optional_services = OptionalService.all
    optional_services_options = Array.new
    optional_services.each do |optional_service|
      p optional_service.name
      if @quote.maritime_rate.origin.country.code != 'MX' and optional_service.name == 'collection_at_origin'
        puts "no se puede"
      else

        optional_services_options << optional_service
      end
    end
    optional_services_options.group_by(&:category)

  end

  def finish_wizard_path
    redirect_to customer_quotes_path
  end

  def set_client
    @client = current_user.contact.contactable
  end

  def set_quote
    @quote = MaritimeQuote.find_by_id(session[:quote_id])
  end

  def quote_params
    params.require(:maritime_quote).permit(:id, :operation_type, :service_mode, :organization_id,
                                           :quoted_at, :transportation_method, :frequency, :incoterm_id,
                                           :client_id, :service_type, :transportation_modality, :delivery_address,
                                           :operation_type, :danger_item, :guarantee_letter, :total_cost,
                                           :freight_type_id, :description, :declared_value, :collection_address,
                                           :origin_route_id, :destination_route_id, :_destroy,
                                           maritime_quote_units_attributes: maritime_quote_units_attributes,
                                           optional_service_ids: [])
  end

  def maritime_quote_units_attributes
    %i[id length height width weight volume number_units packaging_type_id product_id container_id]
  end

  def quote_optional_services
    %i[id optional_service_id applied_charge]
  end

  def initialize_quote
    @quote = MaritimeQuote.new
  end

end
