class Customer::QuoteCommentsController < Customer::ApplicationController
  protect_from_forgery with: :null_session
  respond_to :js, :html, :json

  def create
    puts "Aqui creando el comentario en customer"
    @quote = MaritimeQuote.find(params[:maritime_quote_id])
    @comment = @quote.quote_comments.new(quote_comment_params)
    @user = @comment.user
    respond_to do |format|
      if @comment.save
        format.json { render json: { comment: @comment, user: @user } }
      else
        format.html { render :show, status: :unprocessable_entity }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def quote_comment_params
    params.require(:quote_comment).permit(:maritime_quote_id, :user_id, :body)
  end
end
