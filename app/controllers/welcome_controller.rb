class WelcomeController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_freight_type, only: %i[search_rates optional_services]
  before_action :set_freight_types, only: %i[index search_rates]
  before_action :set_origin, only: %i[search_rates optional_services]
  before_action :set_origin_ports, only: %i[index search_rates]
  before_action :set_destination, only: %i[search_rates optional_services]
  before_action :set_destination_ports, only: %i[index search_rates]
  before_action :set_rate_id, only: %i[optional_services]
  before_action :set_containers, only: %i[index search_rates]
  before_action :set_container, only: %i[search_rates optional_services]
  before_action :set_dimensions, only: %i[search_rates optional_services]

  def index; end

  def search_rates
    @quote_contact_email = QuoteContactEmail.new
    rates ||= []
    if @freight_type.code == 'FCL'
      @rates = MaritimeRate.where('rate_validity_end >= ? AND freight_type_id = ? AND origin_id = ? AND destination_id = ? AND container_id = ?', 7.days.from_now, @freight_type.id, @origin.id, @destination.id, @container.id)
    else
      @rates = MaritimeRate.where('rate_validity_end >= ? AND freight_type_id = ? AND origin_id = ? AND destination_id = ?', 7.days.from_now, @freight_type.id, @origin.id, @destination.id)
    end
    @rates.each do |tariff|
      rates << tariff
    end
    data = rates.flatten.sort_by { |rate| [rate.freight_rate, rate.transit_time] }
    @minimal_cost = data.first
    @cost_benefit = data.second
  end

  def blank; end

  def optional_services
    client = current_user.contact.contactable
    if @freight_type.code == 'FCL'
      @quote = MaritimeQuote.find_or_create_by(freight_type_id: @freight_type.id, origin_route_id: @origin.id, maritime_rate_id: @rate.id, destination_route_id: @destination.id, client_id: client.id)
      quote_units = MaritimeQuoteUnit.find_or_create_by(maritime_quote_id: @quote.id, number_units: @quantity, container_id: @container.id)
    else
      @quote = MaritimeQuote.find_or_create_by(freight_type_id: @freight_type.id, origin_route_id: @origin.id, maritime_rate_id: @rate.id, destination_route_id: @destination.id, client_id: client.id)
      quote_units = MaritimeQuoteUnit.find_or_create_by(maritime_quote_id: @quote.id, length: @length, height: @height, width: @width, weight: @weight)
    end
    @optional_services = OptionalService.all.group_by(&:category)
    @total_cost = @quote.maritime_rate.freight_rate
    @quote.deliveries.build
    @quote.pickups.build
  end

  def get_destination_ports
    puts "Si entro a buscar los puertos"
    destination_ports = MaritimeRate.where(origin_id: params[:origin_id]).where('rate_validity_end > ? ',  7.days.from_now)
                                    .map { |rate| rate.destination  }.uniq
    respond_to do |format|
      format.json { render :json => destination_ports }
    end
  end

  private

  def set_origin_ports
    maritime_rates = MaritimeRate.where('rate_validity_end >= ? ', 7.days.from_now)
    @origin_ports = [] | maritime_rates.map(&:origin).uniq
  end

  def set_destination_ports
    maritime_rates = MaritimeRate.where('rate_validity_end >= ? ', 7.days.from_now)
    if maritime_rates.present?
      @destination_ports = [] | MaritimeRate.where(origin_id: maritime_rates.first.origin).where('rate_validity_end > ? ',
                                                                                                 7.days.from_now).map(&:destination).uniq
    end
  end

  def set_freight_types
    @freight_types = FreightType.joins(:service_mode).where("service_modes.name = 'Servicios Marítimos'")
  end

  def set_origin
    @origin = Port.find(params[:origin_id])
  end

  def set_destination
    @destination = Port.find(params[:destination_id])
  end

  def set_freight_type
    @freight_type = FreightType.find(params[:freight_type_id])
  end

  def set_rate_id
    @rate = MaritimeRate.find(params[:rate_id])
  end

  def set_containers
    @containers = Container.all
  end

  def set_container
    @quantity = params[:quantity]

    @container = Container.find(params[:container_id]) unless params[:container_id].blank?
  end

  def set_dimensions
    @length = params[:length]
    @width = params[:width]
    @height = params[:height]
    @weight = params[:weight]
  end
end
