class MaritimeQuotesController < ApplicationController
  before_action :set_maritime_quote, only: %i[update]

  def update
    if @quote.update(maritime_quote_params)
      ActivityRegister.create(activity_registerable_type: 'MaritimeQuote', activity_registerable_id: @quote.id, user_id: current_user.id, activity: "#{current_user.username} ha generado una cotización")
      flash[:succcess] = 'Cotizacion generada correctamente'
      redirect_to root_path
    else
      render 'welcome/optional_services'
    end
  end

  def optional_service
    optional_service = {}
    service = OptionalService.find(params[:optional_service_id])
    optional_service[:id] = service.id
    optional_service[:name] = service.name
    optional_service[:name_label] = service.name_label
    optional_service[:category] = service.category
    optional_service[:cost] = service.cost
    respond_to do |format|
      format.json { render json: optional_service }
    end
  end

  private

  def maritime_quote_params
    params.require(:maritime_quote).permit(:id, :frequency, :declared_value, :total_cost, optional_service_ids: [],
                                                                                                         pickups_attributes: pickups_attributes,
                                                                                                         deliveries_attributes: deliveries_attributes)
  end

  def set_maritime_quote
    @quote = MaritimeQuote.find(params[:id])
  end

  def pickups_attributes
    %i[id pickupable_type pickupable_id street interior_number outdoor_number colony city state country zip_code]
  end

  def deliveries_attributes
    %i[id deliverable_type deliverable_id street interior_number outdoor_number colony city state country zip_code]
  end
end
