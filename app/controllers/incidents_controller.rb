class IncidentsController < ApplicationController
  def create
    @incident = incident.new(incident_params)
    if @incident.save
      redirect_to [@incident.incidentable], notice: 'incident created'
    else
      render :new
    end
  end

  private

  def incident_params
    params.require(:incident).permit(:activity, :comment, :incidentable_id, :incidentable_type)
  end
end
