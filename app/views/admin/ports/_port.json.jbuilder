json.extract! port, :id, :identifier, :name, :transportation_method, :inactive, :city, :country_id, :latitude, :longitude, :created_at, :updated_at
json.url admin_port_url(port, format: :json)
