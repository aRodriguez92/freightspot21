json.extract! quote_comment, :id, :maritime_quote_id, :user_id, :body,:created_at, :updated_at
json.url admin_quote_url(quote_comment.maritime_quote_id, format: :json)
