namespace :import do
  desc 'Import tariff rates from csv'
  task tariff_rates: :environment do
    filename = Rails.root.join('db/csv/tariff_rates.csv')
    counter = 0
    CSV.foreach(filename) do |row|
      # puts row
      hs_code, nico, description = row
      hs_code = hs_code.tr('.', '')
      puts "hs code: #{hs_code}"
      puts "nico: #{nico}"
      puts "description: #{description}"
      tariff_rate = TariffRate.create(hs_code: hs_code, nico: nico, description: description)
      counter += 1 if tariff_rate.persisted?
      puts "\n"
    end

    puts "Imported #{counter} tariff rates"
  end

  desc 'Import ports from csv'
  task ports: :environment do
    filename = Rails.root.join('db/csv/ports.csv')
    counter = 0
    CSV.foreach(filename) do |row|
      p row
      identifier, name, transportation_method, country, latitude, longitude = row
      country_data = Country.find_by(name: country)
      port = Port.create(identifier: identifier, name: name, country_id: country_data.id, latitude: latitude,
                         longitude: longitude, transportation_method: transportation_method)
      counter += 1 if port.persisted?
      puts port.errors.full_messages.join(',') if port.errors.any?
    end

    puts "Imported #{counter} ports"
  end

  desc 'Import charges from csv'
  task charges: :environment do
    filename = Rails.root.join('db/csv/charges.csv')
    counter = 0
    CSV.foreach(filename) do |row|
      p row
      code, name, active = row
      charge = Charge.create_or_find_by(code: code, name: name, active: active)
      counter += 1 if charge.persisted?
      puts charge.errors.full_messages.join(',') if charge.errors.any?
    end
  end

  desc 'Import customs from csv'
  task customs: :environment do
    filename = Rails.root.join('db/csv/customs.csv')
    counter = 0
    CSV.foreach(filename) do |row|
      p row
      code, section, custom, active = row
      custom = Custom.create_or_find_by(code: code, section: section, custom: custom.capitalize, active?: active)
      counter += 1 if custom.persisted?
      puts custom.errors.full_messages.join(',') if custom.errors.any?
    end
  end
end
