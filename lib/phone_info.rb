# lib/contact_info.rb

module PhoneInfo
  class PhoneNumberFetcher
    def self.get_phone_numbers(obj)
      if obj.is_a?(Client)
        phone_numbers = obj.senders.reject(&:blank?).map { |sender| sender.phones.first }
        phone_numbers += obj.contacts.map { |contact| contact.phones.first }
        phone_numbers += obj.addresses.map { |address| address.phones.first }
        phone_numbers += obj.phones
      elsif obj.is_a?(Supplier)
        phone_numbers = obj.contacts.map { |contact| contact.phones.first }
        phone_numbers += obj.addresses.map { |address| address.phones.first }
        phone_numbers += obj.phones
      else
        raise ArgumentError, "Objeto no es de tipo Cliente o Proveedor"
      end
      phone_numbers
    end
  end
end
