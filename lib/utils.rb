# Create shared methods for controllers
module Utils
  # Methods for searching
  module Search
    def phone_list(address, contacts, senders)
      address.phones + contact_phones(contacts) + sender_phones(senders)
    end
    def phone_list_supplier(address, contacts)
      address.phones + contact_phones(contacts)
    end
    # Method that gets phone numbers from contacts
    def contact_phones(contacts)
      contacts.map { |contact| contact.phones.first }
    end

    # Method that gets phone numbers from senders
    def sender_phones(senders)
      senders.reject(&:blank?).map { |sender| sender.phones.first }
    end
  end
end
