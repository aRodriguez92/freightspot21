# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2023_09_25_065112) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "access_roles", force: :cascade do |t|
    t.bigint "access_id", null: false
    t.bigint "role_id", null: false
    t.bigint "system_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["access_id"], name: "index_access_roles_on_access_id"
    t.index ["deleted_at"], name: "index_access_roles_on_deleted_at"
    t.index ["role_id"], name: "index_access_roles_on_role_id"
    t.index ["system_module_id"], name: "index_access_roles_on_system_module_id"
  end

  create_table "accesses", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_accesses_on_deleted_at"
    t.index ["slug"], name: "index_accesses_on_slug", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "activity_registers", force: :cascade do |t|
    t.string "activity_registerable_type"
    t.bigint "activity_registerable_id"
    t.bigint "user_id"
    t.text "activity"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["activity_registerable_type", "activity_registerable_id"], name: "index_activity_registers_on_activity_registerable"
    t.index ["deleted_at"], name: "index_activity_registers_on_deleted_at"
    t.index ["user_id"], name: "index_activity_registers_on_user_id"
  end

  create_table "address_types", force: :cascade do |t|
    t.string "name"
    t.boolean "inactive", default: false
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_address_types_on_deleted_at"
    t.index ["slug"], name: "index_address_types_on_slug"
  end

  create_table "addresses", force: :cascade do |t|
    t.string "addressable_type"
    t.bigint "addressable_id"
    t.string "name"
    t.string "street"
    t.string "interior_number"
    t.string "outdoor_number"
    t.string "colony"
    t.string "city"
    t.string "country"
    t.bigint "state_id"
    t.string "zip_code"
    t.boolean "principal", default: false
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["addressable_type", "addressable_id"], name: "index_addresses_on_addressable"
    t.index ["deleted_at"], name: "index_addresses_on_deleted_at"
    t.index ["slug"], name: "index_addresses_on_slug"
    t.index ["state_id"], name: "index_addresses_on_state_id"
  end

  create_table "agents", force: :cascade do |t|
    t.string "name"
    t.string "website"
    t.string "rfc"
    t.bigint "payment_term_id"
    t.bigint "currency_id"
    t.bigint "cfdi_id"
    t.bigint "way_pay_id"
    t.bigint "payment_method_id"
    t.boolean "inactive", default: false
    t.bigint "status", default: 0
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["cfdi_id"], name: "index_agents_on_cfdi_id"
    t.index ["currency_id"], name: "index_agents_on_currency_id"
    t.index ["deleted_at"], name: "index_agents_on_deleted_at"
    t.index ["payment_method_id"], name: "index_agents_on_payment_method_id"
    t.index ["payment_term_id"], name: "index_agents_on_payment_term_id"
    t.index ["way_pay_id"], name: "index_agents_on_way_pay_id"
  end

  create_table "approvals", force: :cascade do |t|
    t.string "approvalable_type"
    t.bigint "approvalable_id"
    t.bigint "request_user_id"
    t.bigint "respond_user_id"
    t.jsonb "preview_changes", default: {}, null: false
    t.jsonb "recent_changes", default: {}, null: false
    t.boolean "approved"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.datetime "requested_at"
    t.datetime "rejected_at"
    t.index ["approvalable_type", "approvalable_id"], name: "index_approvals_on_approvalable"
    t.index ["deleted_at"], name: "index_approvals_on_deleted_at"
    t.index ["request_user_id"], name: "index_approvals_on_request_user_id"
    t.index ["respond_user_id"], name: "index_approvals_on_respond_user_id"
  end

  create_table "assign_types", force: :cascade do |t|
    t.bigint "address_id", null: false
    t.bigint "address_type_id", null: false
    t.string "other"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["address_id"], name: "index_assign_types_on_address_id"
    t.index ["address_type_id"], name: "index_assign_types_on_address_type_id"
    t.index ["deleted_at"], name: "index_assign_types_on_deleted_at"
  end

  create_table "audits", force: :cascade do |t|
    t.integer "auditable_id"
    t.string "auditable_type"
    t.integer "associated_id"
    t.string "associated_type"
    t.integer "user_id"
    t.string "user_type"
    t.string "username"
    t.string "action"
    t.text "audited_changes"
    t.integer "version", default: 0
    t.string "comment"
    t.string "remote_address"
    t.string "request_uuid"
    t.datetime "created_at"
    t.index ["associated_type", "associated_id"], name: "associated_index"
    t.index ["auditable_type", "auditable_id", "version"], name: "auditable_index"
    t.index ["created_at"], name: "index_audits_on_created_at"
    t.index ["request_uuid"], name: "index_audits_on_request_uuid"
    t.index ["user_id", "user_type"], name: "user_index"
  end

  create_table "bank_accounts", force: :cascade do |t|
    t.string "bank"
    t.string "account_number"
    t.string "owner"
    t.bigint "client_id", null: false
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["client_id"], name: "index_bank_accounts_on_client_id"
    t.index ["deleted_at"], name: "index_bank_accounts_on_deleted_at"
    t.index ["slug"], name: "index_bank_accounts_on_slug", unique: true
  end

  create_table "bookings", force: :cascade do |t|
    t.bigint "maritime_quote_id", null: false
    t.bigint "status", default: 1
    t.bigint "movement_type", default: 1
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["maritime_quote_id"], name: "index_bookings_on_maritime_quote_id"
  end

  create_table "branches", force: :cascade do |t|
    t.string "name"
    t.bigint "organization_id", null: false
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_branches_on_deleted_at"
    t.index ["organization_id"], name: "index_branches_on_organization_id"
    t.index ["slug"], name: "index_branches_on_slug", unique: true
  end

  create_table "cargo_types", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.datetime "deleted_at"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["deleted_at"], name: "index_cargo_types_on_deleted_at"
    t.index ["slug"], name: "index_cargo_types_on_slug"
  end

  create_table "cfdis", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.text "description"
    t.boolean "physical", default: false
    t.boolean "moral", default: false
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_cfdis_on_deleted_at"
    t.index ["slug"], name: "index_cfdis_on_slug", unique: true
  end

  create_table "changelogs", force: :cascade do |t|
    t.string "version_number"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "released_at"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_changelogs_on_deleted_at"
  end

  create_table "charges", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.boolean "active", default: true
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_charges_on_deleted_at"
    t.index ["slug"], name: "index_charges_on_slug"
  end

  create_table "client_documents", force: :cascade do |t|
    t.bigint "client_id", null: false
    t.bigint "document_id", null: false
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["client_id"], name: "index_client_documents_on_client_id"
    t.index ["deleted_at"], name: "index_client_documents_on_deleted_at"
    t.index ["document_id"], name: "index_client_documents_on_document_id"
  end

  create_table "client_products", force: :cascade do |t|
    t.string "hs_code"
    t.bigint "client_id"
    t.bigint "product_id"
    t.decimal "tariff_fraction"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["client_id"], name: "index_client_products_on_client_id"
    t.index ["deleted_at"], name: "index_client_products_on_deleted_at"
    t.index ["product_id"], name: "index_client_products_on_product_id"
  end

  create_table "clients", force: :cascade do |t|
    t.string "name"
    t.string "website"
    t.string "rfc"
    t.bigint "annual_international_freight_shipments"
    t.boolean "has_credit", default: false
    t.boolean "authorized_credit", default: false
    t.boolean "advanced_parameters"
    t.decimal "credit_limit", precision: 10, scale: 4, default: "0.0"
    t.bigint "payment_term_id"
    t.bigint "currency_id"
    t.bigint "cfdi_id"
    t.bigint "way_pay_id"
    t.bigint "payment_method_id"
    t.bigint "parent_entity_id"
    t.boolean "inactive", default: false
    t.bigint "status", default: 0
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.bigint "middleman_id"
    t.bigint "team_id"
    t.index ["cfdi_id"], name: "index_clients_on_cfdi_id"
    t.index ["currency_id"], name: "index_clients_on_currency_id"
    t.index ["deleted_at"], name: "index_clients_on_deleted_at"
    t.index ["middleman_id"], name: "index_clients_on_middleman_id"
    t.index ["parent_entity_id"], name: "index_clients_on_parent_entity_id"
    t.index ["payment_method_id"], name: "index_clients_on_payment_method_id"
    t.index ["payment_term_id"], name: "index_clients_on_payment_term_id"
    t.index ["slug"], name: "index_clients_on_slug", unique: true
    t.index ["team_id"], name: "index_clients_on_team_id"
    t.index ["way_pay_id"], name: "index_clients_on_way_pay_id"
  end

  create_table "comments", force: :cascade do |t|
    t.string "commentable_type"
    t.bigint "commentable_id"
    t.bigint "user_id", null: false
    t.text "body"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["commentable_type", "commentable_id"], name: "index_comments_on_commentable"
    t.index ["deleted_at"], name: "index_comments_on_deleted_at"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "contactable_type"
    t.bigint "contactable_id"
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.string "area"
    t.string "job"
    t.datetime "birth_date"
    t.boolean "principal", default: false
    t.bigint "user_id"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["contactable_type", "contactable_id"], name: "index_contacts_on_contactable"
    t.index ["deleted_at"], name: "index_contacts_on_deleted_at"
    t.index ["slug"], name: "index_contacts_on_slug", unique: true
    t.index ["user_id"], name: "index_contacts_on_user_id"
  end

  create_table "container_dimensions", force: :cascade do |t|
    t.string "name"
    t.decimal "metric_value"
    t.bigint "metric_measurement_id"
    t.decimal "imperial_value"
    t.bigint "imperial_measurement_id"
    t.bigint "container_id"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["container_id"], name: "index_container_dimensions_on_container_id"
    t.index ["deleted_at"], name: "index_container_dimensions_on_deleted_at"
    t.index ["imperial_measurement_id"], name: "index_container_dimensions_on_imperial_measurement_id"
    t.index ["metric_measurement_id"], name: "index_container_dimensions_on_metric_measurement_id"
    t.index ["slug"], name: "index_container_dimensions_on_slug"
  end

  create_table "containers", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.text "description"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_containers_on_deleted_at"
    t.index ["slug"], name: "index_containers_on_slug", unique: true
  end

  create_table "countries", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.string "phonecode"
    t.boolean "prohibited", default: false
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_countries_on_deleted_at"
    t.index ["slug"], name: "index_countries_on_slug", unique: true
  end

  create_table "criteria", force: :cascade do |t|
    t.string "name"
    t.decimal "amount"
    t.bigint "currency_id"
    t.datetime "validity_start"
    t.datetime "validity_end"
    t.text "remarks"
    t.boolean "advanced_parameters", default: false
    t.bigint "origin_id"
    t.bigint "destiny_id"
    t.bigint "container_id"
    t.bigint "status", default: 1
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["container_id"], name: "index_criteria_on_container_id"
    t.index ["currency_id"], name: "index_criteria_on_currency_id"
    t.index ["deleted_at"], name: "index_criteria_on_deleted_at"
    t.index ["destiny_id"], name: "index_criteria_on_destiny_id"
    t.index ["origin_id"], name: "index_criteria_on_origin_id"
  end

  create_table "currencies", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_currencies_on_deleted_at"
    t.index ["slug"], name: "index_currencies_on_slug", unique: true
  end

  create_table "custom_quotes", force: :cascade do |t|
    t.bigint "rate_id"
    t.bigint "employee_id"
    t.string "customer"
    t.string "email"
    t.string "phone_number"
    t.string "company_name"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["deleted_at"], name: "index_custom_quotes_on_deleted_at"
    t.index ["employee_id"], name: "index_custom_quotes_on_employee_id"
    t.index ["rate_id"], name: "index_custom_quotes_on_rate_id"
  end

  create_table "customs", force: :cascade do |t|
    t.string "code"
    t.string "custom"
    t.string "section"
    t.decimal "rate"
    t.decimal "fcl_expo_rate"
    t.decimal "fcl_impo_rate"
    t.decimal "lcl_expo_rate"
    t.decimal "lcl_impo_rate"
    t.boolean "active?", default: true
    t.bigint "service_mode_id"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_customs_on_deleted_at"
    t.index ["service_mode_id"], name: "index_customs_on_service_mode_id"
    t.index ["slug"], name: "index_customs_on_slug", unique: true
  end

  create_table "deliveries", force: :cascade do |t|
    t.string "deliverable_type"
    t.bigint "deliverable_id"
    t.string "street"
    t.string "interior_number"
    t.string "outdoor_number"
    t.string "colony"
    t.string "city"
    t.string "state"
    t.string "country"
    t.string "zip_code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_deliveries_on_deleted_at"
    t.index ["deliverable_type", "deliverable_id"], name: "index_deliveries_on_deliverable"
  end

  create_table "departments", force: :cascade do |t|
    t.string "name"
    t.bigint "branch_id", null: false
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["branch_id"], name: "index_departments_on_branch_id"
    t.index ["deleted_at"], name: "index_departments_on_deleted_at"
    t.index ["slug"], name: "index_departments_on_slug", unique: true
  end

  create_table "documents", force: :cascade do |t|
    t.string "name"
    t.string "document_type"
    t.string "phase"
    t.boolean "obligatory"
    t.boolean "active", default: true
    t.string "variable"
    t.boolean "client", default: true
    t.boolean "supplier", default: true
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_documents_on_deleted_at"
    t.index ["slug"], name: "index_documents_on_slug", unique: true
  end

  create_table "employees", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "phone"
    t.string "extension"
    t.string "mobile"
    t.string "origin"
    t.string "city"
    t.datetime "birth_date"
    t.string "job"
    t.datetime "join_date"
    t.bigint "user_id", null: false
    t.bigint "department_id"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_employees_on_deleted_at"
    t.index ["department_id"], name: "index_employees_on_department_id"
    t.index ["slug"], name: "index_employees_on_slug", unique: true
    t.index ["user_id"], name: "index_employees_on_user_id"
  end

  create_table "favorites", force: :cascade do |t|
    t.string "favoritable_type"
    t.bigint "favoritable_id"
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_favorites_on_deleted_at"
    t.index ["favoritable_type", "favoritable_id"], name: "index_favorites_on_favoritable"
    t.index ["user_id"], name: "index_favorites_on_user_id"
  end

  create_table "feature_improvements", force: :cascade do |t|
    t.integer "changelog_id"
    t.string "title"
    t.string "description"
    t.bigint "category"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_feature_improvements_on_deleted_at"
  end

  create_table "freight_types", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.bigint "service_mode_id"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_freight_types_on_deleted_at"
    t.index ["service_mode_id"], name: "index_freight_types_on_service_mode_id"
    t.index ["slug"], name: "index_freight_types_on_slug"
  end

  create_table "incidents", force: :cascade do |t|
    t.string "incidentable_type", null: false
    t.bigint "incidentable_id", null: false
    t.string "activity"
    t.text "comment"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_incidents_on_deleted_at"
    t.index ["incidentable_type", "incidentable_id"], name: "index_incidents_on_incidentable"
    t.index ["slug"], name: "index_incidents_on_slug", unique: true
  end

  create_table "incoterms", force: :cascade do |t|
    t.string "code"
    t.string "description"
    t.boolean "inactive", default: false
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_incoterms_on_deleted_at"
    t.index ["slug"], name: "index_incoterms_on_slug"
  end

  create_table "languages", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.boolean "default", default: false
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_languages_on_deleted_at"
    t.index ["slug"], name: "index_languages_on_slug", unique: true
  end

  create_table "maritime_quote_units", force: :cascade do |t|
    t.bigint "maritime_quote_id"
    t.bigint "packaging_type_id"
    t.bigint "product_id"
    t.bigint "service_id"
    t.bigint "container_id"
    t.bigint "number_units", default: 1
    t.decimal "length", precision: 10, scale: 4, default: "0.0"
    t.decimal "height", precision: 10, scale: 4, default: "0.0"
    t.decimal "width", precision: 10, scale: 4, default: "0.0"
    t.decimal "weight", precision: 10, scale: 4, default: "0.0"
    t.decimal "volume", precision: 20, scale: 4, default: "0.0"
    t.decimal "chargeable_weight", precision: 20, scale: 4, default: "0.0"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["container_id"], name: "index_maritime_quote_units_on_container_id"
    t.index ["deleted_at"], name: "index_maritime_quote_units_on_deleted_at"
    t.index ["maritime_quote_id"], name: "index_maritime_quote_units_on_maritime_quote_id"
    t.index ["packaging_type_id"], name: "index_maritime_quote_units_on_packaging_type_id"
    t.index ["product_id"], name: "index_maritime_quote_units_on_product_id"
    t.index ["service_id"], name: "index_maritime_quote_units_on_service_id"
  end

  create_table "maritime_quotes", force: :cascade do |t|
    t.string "number"
    t.text "description"
    t.decimal "declared_value", precision: 30, scale: 4, default: "0.0"
    t.datetime "quoted_at"
    t.datetime "expired_at"
    t.integer "free_day_of_delay"
    t.decimal "total_pieces", precision: 10, scale: 4, default: "0.0"
    t.decimal "total_weight", precision: 10, scale: 4, default: "0.0"
    t.decimal "total_volume", precision: 20, scale: 4, default: "0.0"
    t.decimal "total_chargeable_weight", precision: 20, scale: 4, default: "0.0"
    t.decimal "total_cost", precision: 20, scale: 4, default: "0.0"
    t.integer "origin_route_id"
    t.integer "destination_route_id"
    t.bigint "maritime_rate_id"
    t.bigint "client_id"
    t.bigint "payment_term_id"
    t.bigint "organization_id"
    t.bigint "incoterm_id"
    t.bigint "freight_type_id"
    t.boolean "danger_item", default: false
    t.boolean "guarantee_letter", default: false
    t.boolean "is_active_shipment", default: false
    t.bigint "frequency"
    t.bigint "operation_type"
    t.bigint "service_mode"
    t.bigint "service_type"
    t.bigint "status", default: 0
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.string "reason_of_declined"
    t.index ["client_id"], name: "index_maritime_quotes_on_client_id"
    t.index ["deleted_at"], name: "index_maritime_quotes_on_deleted_at"
    t.index ["freight_type_id"], name: "index_maritime_quotes_on_freight_type_id"
    t.index ["incoterm_id"], name: "index_maritime_quotes_on_incoterm_id"
    t.index ["maritime_rate_id"], name: "index_maritime_quotes_on_maritime_rate_id"
    t.index ["organization_id"], name: "index_maritime_quotes_on_organization_id"
    t.index ["payment_term_id"], name: "index_maritime_quotes_on_payment_term_id"
    t.index ["slug"], name: "index_maritime_quotes_on_slug"
  end

  create_table "maritime_quotes_rates", id: false, force: :cascade do |t|
    t.bigint "maritime_quote_id"
    t.bigint "maritime_rate_id"
    t.index ["maritime_quote_id"], name: "index_maritime_quotes_rates_on_maritime_quote_id"
    t.index ["maritime_rate_id"], name: "index_maritime_quotes_rates_on_maritime_rate_id"
  end

  create_table "maritime_rate_imports", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_maritime_rate_imports_on_deleted_at"
  end

  create_table "maritime_rates", force: :cascade do |t|
    t.bigint "origin_id"
    t.bigint "destination_id"
    t.bigint "shipper_id"
    t.bigint "shipping_line_id"
    t.bigint "freight_type_id"
    t.bigint "container_id"
    t.bigint "cargo_type_id", null: false
    t.bigint "number_of_transshipments"
    t.integer "transit_time"
    t.datetime "rate_validity_start"
    t.datetime "rate_validity_end"
    t.datetime "departure_date"
    t.decimal "freight_rate"
    t.bigint "currency_id"
    t.integer "free_days"
    t.boolean "guarantee_letter"
    t.text "remarks"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["cargo_type_id"], name: "index_maritime_rates_on_cargo_type_id"
    t.index ["container_id"], name: "index_maritime_rates_on_container_id"
    t.index ["currency_id"], name: "index_maritime_rates_on_currency_id"
    t.index ["deleted_at"], name: "index_maritime_rates_on_deleted_at"
    t.index ["destination_id"], name: "index_maritime_rates_on_destination_id"
    t.index ["freight_type_id"], name: "index_maritime_rates_on_freight_type_id"
    t.index ["origin_id"], name: "index_maritime_rates_on_origin_id"
    t.index ["shipper_id"], name: "index_maritime_rates_on_shipper_id"
    t.index ["shipping_line_id"], name: "index_maritime_rates_on_shipping_line_id"
  end

  create_table "maritime_shipment_documents", force: :cascade do |t|
    t.bigint "maritime_shipment_id"
    t.bigint "document_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_maritime_shipment_documents_on_deleted_at"
    t.index ["maritime_shipment_id"], name: "index_maritime_shipment_documents_on_maritime_shipment_id"
  end

  create_table "maritime_shipment_imports", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_maritime_shipment_imports_on_deleted_at"
  end

  create_table "maritime_shipments", force: :cascade do |t|
    t.string "bl_number"
    t.string "container_number"
    t.string "number"
    t.string "qr_code"
    t.string "bar_code"
    t.bigint "origin_id"
    t.bigint "destination_id"
    t.bigint "maritime_quote_id"
    t.bigint "incoterm_id"
    t.bigint "client_id"
    t.bigint "team_id"
    t.bigint "shipping_line_id"
    t.bigint "operator_id"
    t.bigint "sales_executive_id"
    t.bigint "seller_id"
    t.bigint "status", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.datetime "arrival_time"
    t.datetime "earliest_to_latest"
    t.bigint "movement_type", default: 0
    t.string "purchase_order_number"
    t.text "remarks"
    t.index ["client_id"], name: "index_maritime_shipments_on_client_id"
    t.index ["deleted_at"], name: "index_maritime_shipments_on_deleted_at"
    t.index ["destination_id"], name: "index_maritime_shipments_on_destination_id"
    t.index ["incoterm_id"], name: "index_maritime_shipments_on_incoterm_id"
    t.index ["maritime_quote_id"], name: "index_maritime_shipments_on_maritime_quote_id"
    t.index ["operator_id"], name: "index_maritime_shipments_on_operator_id"
    t.index ["origin_id"], name: "index_maritime_shipments_on_origin_id"
    t.index ["sales_executive_id"], name: "index_maritime_shipments_on_sales_executive_id"
    t.index ["seller_id"], name: "index_maritime_shipments_on_seller_id"
    t.index ["shipping_line_id"], name: "index_maritime_shipments_on_shipping_line_id"
    t.index ["team_id"], name: "index_maritime_shipments_on_team_id"
  end

  create_table "maritime_tracking_events", force: :cascade do |t|
    t.bigint "maritime_shipments_id"
    t.bigint "activity_id"
    t.bigint "location_type"
    t.bigint "location_id"
    t.datetime "tracking_event_at"
    t.bigint "vehicle"
    t.string "voyage_name"
    t.string "voyage_number"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["activity_id"], name: "index_maritime_tracking_events_on_activity_id"
    t.index ["deleted_at"], name: "index_maritime_tracking_events_on_deleted_at"
    t.index ["location_id"], name: "index_maritime_tracking_events_on_location_id"
    t.index ["maritime_shipments_id"], name: "index_maritime_tracking_events_on_maritime_shipments_id"
  end

  create_table "membership_plans", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.decimal "price"
    t.text "description"
    t.boolean "active"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_membership_plans_on_deleted_at"
    t.index ["slug"], name: "index_membership_plans_on_slug"
  end

  create_table "middlemen", force: :cascade do |t|
    t.string "name"
    t.string "website"
    t.string "rfc"
    t.bigint "payment_term_id"
    t.bigint "currency_id"
    t.bigint "cfdi_id"
    t.bigint "way_pay_id"
    t.bigint "payment_method_id"
    t.boolean "inactive", default: false
    t.bigint "status", default: 0
    t.string "slug"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["cfdi_id"], name: "index_middlemen_on_cfdi_id"
    t.index ["currency_id"], name: "index_middlemen_on_currency_id"
    t.index ["deleted_at"], name: "index_middlemen_on_deleted_at"
    t.index ["payment_method_id"], name: "index_middlemen_on_payment_method_id"
    t.index ["payment_term_id"], name: "index_middlemen_on_payment_term_id"
    t.index ["way_pay_id"], name: "index_middlemen_on_way_pay_id"
  end

  create_table "multiservices", force: :cascade do |t|
    t.bigint "supplier_id", null: false
    t.bigint "service_mode_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_multiservices_on_deleted_at"
    t.index ["service_mode_id"], name: "index_multiservices_on_service_mode_id"
    t.index ["supplier_id"], name: "index_multiservices_on_supplier_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.string "recipient_type", null: false
    t.bigint "recipient_id", null: false
    t.string "type", null: false
    t.jsonb "params"
    t.datetime "read_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_notifications_on_deleted_at"
    t.index ["read_at"], name: "index_notifications_on_read_at"
    t.index ["recipient_type", "recipient_id"], name: "index_notifications_on_recipient"
  end

  create_table "optional_services", force: :cascade do |t|
    t.string "name"
    t.decimal "cost"
    t.string "category"
    t.boolean "active", default: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_optional_services_on_deleted_at"
  end

  create_table "organizations", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_organizations_on_deleted_at"
    t.index ["slug"], name: "index_organizations_on_slug", unique: true
  end

  create_table "packaging_types", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.text "definition"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_packaging_types_on_deleted_at"
    t.index ["slug"], name: "index_packaging_types_on_slug", unique: true
  end

  create_table "payment_methods", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.text "description"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_payment_methods_on_deleted_at"
    t.index ["slug"], name: "index_payment_methods_on_slug", unique: true
  end

  create_table "payment_terms", force: :cascade do |t|
    t.string "description"
    t.integer "amount_days_pay"
    t.boolean "inactive", default: false
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_payment_terms_on_deleted_at"
    t.index ["slug"], name: "index_payment_terms_on_slug", unique: true
  end

  create_table "phones", force: :cascade do |t|
    t.string "phoneable_type"
    t.bigint "phoneable_id"
    t.string "code"
    t.string "number"
    t.string "extension"
    t.integer "type_phone"
    t.boolean "principal", default: false
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_phones_on_deleted_at"
    t.index ["phoneable_type", "phoneable_id"], name: "index_phones_on_phoneable"
    t.index ["slug"], name: "index_phones_on_slug", unique: true
  end

  create_table "pickups", force: :cascade do |t|
    t.string "pickupable_type"
    t.bigint "pickupable_id"
    t.string "street"
    t.string "interior_number"
    t.string "outdoor_number"
    t.string "colony"
    t.string "city"
    t.string "state"
    t.string "country"
    t.string "zip_code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_pickups_on_deleted_at"
    t.index ["pickupable_type", "pickupable_id"], name: "index_pickups_on_pickupable"
  end

  create_table "ports", force: :cascade do |t|
    t.string "identifier"
    t.string "name"
    t.integer "transportation_method"
    t.boolean "inactive", default: false
    t.string "city"
    t.bigint "country_id"
    t.decimal "latitude", default: "0.0"
    t.decimal "longitude", default: "0.0"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["country_id"], name: "index_ports_on_country_id"
    t.index ["deleted_at"], name: "index_ports_on_deleted_at"
    t.index ["slug"], name: "index_ports_on_slug", unique: true
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_products_on_deleted_at"
  end

  create_table "quote_comments", force: :cascade do |t|
    t.bigint "maritime_quote_id"
    t.bigint "user_id"
    t.text "body"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_quote_comments_on_deleted_at"
    t.index ["maritime_quote_id"], name: "index_quote_comments_on_maritime_quote_id"
    t.index ["user_id"], name: "index_quote_comments_on_user_id"
  end

  create_table "quote_contact_emails", force: :cascade do |t|
    t.string "code"
    t.string "company"
    t.string "email"
    t.string "phone"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.string "first_name"
    t.string "last_name"
    t.index ["deleted_at"], name: "index_quote_contact_emails_on_deleted_at"
    t.index ["slug"], name: "index_quote_contact_emails_on_slug"
  end

  create_table "quote_optional_services", force: :cascade do |t|
    t.bigint "maritime_quote_id"
    t.bigint "optional_service_id"
    t.decimal "charge"
    t.boolean "applied_charge", default: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_quote_optional_services_on_deleted_at"
    t.index ["maritime_quote_id"], name: "index_quote_optional_services_on_maritime_quote_id"
    t.index ["optional_service_id"], name: "index_quote_optional_services_on_optional_service_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_roles_on_deleted_at"
    t.index ["slug"], name: "index_roles_on_slug", unique: true
  end

  create_table "sender_sender_types", force: :cascade do |t|
    t.bigint "sender_id"
    t.bigint "sender_type_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_sender_sender_types_on_deleted_at"
    t.index ["sender_id"], name: "index_sender_sender_types_on_sender_id"
    t.index ["sender_type_id"], name: "index_sender_sender_types_on_sender_type_id"
  end

  create_table "sender_types", force: :cascade do |t|
    t.string "name"
    t.boolean "inactive", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_sender_types_on_deleted_at"
  end

  create_table "senders", force: :cascade do |t|
    t.string "senderable_type"
    t.bigint "senderable_id"
    t.string "contact"
    t.string "name"
    t.string "rfc"
    t.string "email"
    t.string "phone"
    t.string "address"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_senders_on_deleted_at"
    t.index ["senderable_type", "senderable_id"], name: "index_senders_on_senderable"
  end

  create_table "service_modes", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.bigint "variable"
    t.bigint "service_modes_id"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_service_modes_on_deleted_at"
    t.index ["service_modes_id"], name: "index_service_modes_on_service_modes_id"
    t.index ["slug"], name: "index_service_modes_on_slug", unique: true
  end

  create_table "service_types", force: :cascade do |t|
    t.bigint "incoterm_id"
    t.string "operation_type"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_service_types_on_deleted_at"
    t.index ["incoterm_id"], name: "index_service_types_on_incoterm_id"
  end

  create_table "services", force: :cascade do |t|
    t.string "code"
    t.text "description"
    t.integer "service_type"
    t.decimal "price", precision: 10, scale: 4, default: "0.0"
    t.boolean "has_cost", default: false
    t.bigint "currency_id"
    t.bigint "tax_id"
    t.bigint "service_mode_id"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["currency_id"], name: "index_services_on_currency_id"
    t.index ["deleted_at"], name: "index_services_on_deleted_at"
    t.index ["service_mode_id"], name: "index_services_on_service_mode_id"
    t.index ["slug"], name: "index_services_on_slug", unique: true
    t.index ["tax_id"], name: "index_services_on_tax_id"
  end

  create_table "shipment_activities", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_shipment_activities_on_deleted_at"
  end

  create_table "states", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.bigint "country_id", null: false
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["country_id"], name: "index_states_on_country_id"
    t.index ["deleted_at"], name: "index_states_on_deleted_at"
    t.index ["slug"], name: "index_states_on_slug", unique: true
  end

  create_table "suppliers", force: :cascade do |t|
    t.string "iata_code"
    t.string "caat_code"
    t.string "scac_number"
    t.string "name"
    t.string "rfc"
    t.string "website"
    t.decimal "credit_limit", precision: 10, scale: 4, default: "0.0"
    t.bigint "payment_term_id"
    t.bigint "currency_id"
    t.bigint "cfdi_id"
    t.bigint "way_pay_id"
    t.bigint "payment_method_id"
    t.bigint "parent_entity_id"
    t.boolean "has_credit", default: false
    t.boolean "authorized_credit", default: false
    t.boolean "advanced_parameters"
    t.boolean "inactive", default: false
    t.boolean "carrier", default: false
    t.bigint "status", default: 0
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["cfdi_id"], name: "index_suppliers_on_cfdi_id"
    t.index ["currency_id"], name: "index_suppliers_on_currency_id"
    t.index ["deleted_at"], name: "index_suppliers_on_deleted_at"
    t.index ["parent_entity_id"], name: "index_suppliers_on_parent_entity_id"
    t.index ["payment_method_id"], name: "index_suppliers_on_payment_method_id"
    t.index ["payment_term_id"], name: "index_suppliers_on_payment_term_id"
    t.index ["slug"], name: "index_suppliers_on_slug"
    t.index ["way_pay_id"], name: "index_suppliers_on_way_pay_id"
  end

  create_table "system_modules", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_system_modules_on_deleted_at"
    t.index ["slug"], name: "index_system_modules_on_slug", unique: true
  end

  create_table "tariff_rates", force: :cascade do |t|
    t.string "hs_code"
    t.string "nico"
    t.text "description"
    t.string "umt"
    t.decimal "import_tax"
    t.decimal "export_tax"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_tariff_rates_on_deleted_at"
  end

  create_table "taxes", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.decimal "value", precision: 10, scale: 4, default: "0.0"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_taxes_on_deleted_at"
    t.index ["slug"], name: "index_taxes_on_slug", unique: true
  end

  create_table "team_members", force: :cascade do |t|
    t.bigint "team_id"
    t.bigint "employee_id"
    t.bigint "role", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_team_members_on_deleted_at"
    t.index ["employee_id"], name: "index_team_members_on_employee_id"
    t.index ["team_id"], name: "index_team_members_on_team_id"
  end

  create_table "teams", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_teams_on_deleted_at"
    t.index ["slug"], name: "index_teams_on_slug", unique: true
  end

  create_table "translations", force: :cascade do |t|
    t.string "key"
    t.bigint "language_id"
    t.text "value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_translations_on_deleted_at"
    t.index ["language_id"], name: "index_translations_on_language_id"
  end

  create_table "transshipments", force: :cascade do |t|
    t.bigint "maritime_rate_id", null: false
    t.bigint "port_id", null: false
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["deleted_at"], name: "index_transshipments_on_deleted_at"
    t.index ["maritime_rate_id"], name: "index_transshipments_on_maritime_rate_id"
    t.index ["port_id"], name: "index_transshipments_on_port_id"
  end

  create_table "units", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.bigint "unit_type"
    t.integer "value", default: 0
    t.bigint "unit_system"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_units_on_deleted_at"
    t.index ["slug"], name: "index_units_on_slug", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.bigint "role_id"
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer "invitation_limit"
    t.string "invited_by_type"
    t.bigint "invited_by_id"
    t.integer "invitations_count", default: 0
    t.datetime "accepted_terms_at"
    t.datetime "accepted_privacy_at"
    t.datetime "announcements_read_at"
    t.datetime "last_seen_at"
    t.boolean "admin", default: false
    t.boolean "approved", default: false
    t.boolean "accept_terms_of_service_and_privacy_policies", default: false
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.string "unique_session_id"
    t.index ["deleted_at"], name: "index_users_on_deleted_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["invitation_token"], name: "index_users_on_invitation_token"
    t.index ["invitations_count"], name: "index_users_on_invitations_count"
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id"
    t.index ["invited_by_type", "invited_by_id"], name: "index_users_on_invited_by"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["role_id"], name: "index_users_on_role_id"
    t.index ["slug"], name: "index_users_on_slug", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type"
    t.string "{:null=>false}"
    t.bigint "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  create_table "warehouses", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_warehouses_on_deleted_at"
    t.index ["slug"], name: "index_warehouses_on_slug", unique: true
  end

  create_table "way_pays", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.text "description"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_way_pays_on_deleted_at"
    t.index ["slug"], name: "index_way_pays_on_slug", unique: true
  end

  add_foreign_key "access_roles", "accesses"
  add_foreign_key "access_roles", "roles"
  add_foreign_key "access_roles", "system_modules"
  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "activity_registers", "users"
  add_foreign_key "addresses", "states"
  add_foreign_key "agents", "cfdis"
  add_foreign_key "agents", "currencies"
  add_foreign_key "agents", "payment_methods"
  add_foreign_key "agents", "payment_terms"
  add_foreign_key "agents", "way_pays"
  add_foreign_key "approvals", "contacts", column: "respond_user_id"
  add_foreign_key "approvals", "employees", column: "request_user_id"
  add_foreign_key "assign_types", "address_types"
  add_foreign_key "assign_types", "addresses"
  add_foreign_key "bank_accounts", "clients"
  add_foreign_key "bookings", "maritime_quotes"
  add_foreign_key "branches", "organizations"
  add_foreign_key "client_documents", "clients"
  add_foreign_key "client_documents", "documents"
  add_foreign_key "client_products", "clients"
  add_foreign_key "client_products", "products"
  add_foreign_key "clients", "cfdis"
  add_foreign_key "clients", "clients", column: "parent_entity_id"
  add_foreign_key "clients", "currencies"
  add_foreign_key "clients", "middlemen"
  add_foreign_key "clients", "payment_methods"
  add_foreign_key "clients", "payment_terms"
  add_foreign_key "clients", "teams"
  add_foreign_key "clients", "way_pays"
  add_foreign_key "comments", "users"
  add_foreign_key "contacts", "users"
  add_foreign_key "container_dimensions", "containers"
  add_foreign_key "container_dimensions", "units", column: "imperial_measurement_id"
  add_foreign_key "container_dimensions", "units", column: "metric_measurement_id"
  add_foreign_key "criteria", "containers"
  add_foreign_key "criteria", "currencies"
  add_foreign_key "criteria", "ports", column: "destiny_id"
  add_foreign_key "criteria", "ports", column: "origin_id"
  add_foreign_key "custom_quotes", "employees"
  add_foreign_key "custom_quotes", "maritime_rates", column: "rate_id"
  add_foreign_key "customs", "service_modes"
  add_foreign_key "departments", "branches"
  add_foreign_key "employees", "departments"
  add_foreign_key "employees", "users"
  add_foreign_key "favorites", "users"
  add_foreign_key "freight_types", "service_modes"
  add_foreign_key "maritime_quote_units", "containers"
  add_foreign_key "maritime_quote_units", "maritime_quotes"
  add_foreign_key "maritime_quote_units", "packaging_types"
  add_foreign_key "maritime_quote_units", "products"
  add_foreign_key "maritime_quote_units", "services"
  add_foreign_key "maritime_quotes", "clients"
  add_foreign_key "maritime_quotes", "freight_types"
  add_foreign_key "maritime_quotes", "incoterms"
  add_foreign_key "maritime_quotes", "maritime_rates"
  add_foreign_key "maritime_quotes", "organizations"
  add_foreign_key "maritime_quotes", "payment_terms"
  add_foreign_key "maritime_rates", "cargo_types"
  add_foreign_key "maritime_rates", "containers"
  add_foreign_key "maritime_rates", "currencies"
  add_foreign_key "maritime_rates", "freight_types"
  add_foreign_key "maritime_rates", "ports", column: "destination_id"
  add_foreign_key "maritime_rates", "ports", column: "origin_id"
  add_foreign_key "maritime_rates", "suppliers", column: "shipper_id"
  add_foreign_key "maritime_rates", "suppliers", column: "shipping_line_id"
  add_foreign_key "maritime_shipment_documents", "maritime_shipments"
  add_foreign_key "maritime_shipments", "clients"
  add_foreign_key "maritime_shipments", "employees", column: "operator_id"
  add_foreign_key "maritime_shipments", "employees", column: "sales_executive_id"
  add_foreign_key "maritime_shipments", "employees", column: "seller_id"
  add_foreign_key "maritime_shipments", "incoterms"
  add_foreign_key "maritime_shipments", "maritime_quotes"
  add_foreign_key "maritime_shipments", "ports", column: "destination_id"
  add_foreign_key "maritime_shipments", "ports", column: "origin_id"
  add_foreign_key "maritime_shipments", "suppliers", column: "shipping_line_id"
  add_foreign_key "maritime_shipments", "teams"
  add_foreign_key "maritime_tracking_events", "maritime_shipments", column: "maritime_shipments_id"
  add_foreign_key "maritime_tracking_events", "ports", column: "location_id"
  add_foreign_key "maritime_tracking_events", "shipment_activities", column: "activity_id"
  add_foreign_key "middlemen", "cfdis"
  add_foreign_key "middlemen", "currencies"
  add_foreign_key "middlemen", "payment_methods"
  add_foreign_key "middlemen", "payment_terms"
  add_foreign_key "middlemen", "way_pays"
  add_foreign_key "multiservices", "service_modes"
  add_foreign_key "multiservices", "suppliers"
  add_foreign_key "ports", "countries"
  add_foreign_key "quote_comments", "maritime_quotes"
  add_foreign_key "quote_comments", "users"
  add_foreign_key "quote_optional_services", "maritime_quotes"
  add_foreign_key "quote_optional_services", "optional_services"
  add_foreign_key "sender_sender_types", "sender_types"
  add_foreign_key "sender_sender_types", "senders"
  add_foreign_key "service_modes", "service_modes", column: "service_modes_id"
  add_foreign_key "service_types", "incoterms"
  add_foreign_key "services", "currencies"
  add_foreign_key "services", "service_modes"
  add_foreign_key "services", "taxes"
  add_foreign_key "states", "countries"
  add_foreign_key "suppliers", "cfdis"
  add_foreign_key "suppliers", "currencies"
  add_foreign_key "suppliers", "payment_methods"
  add_foreign_key "suppliers", "payment_terms"
  add_foreign_key "suppliers", "suppliers", column: "parent_entity_id"
  add_foreign_key "suppliers", "way_pays"
  add_foreign_key "team_members", "employees"
  add_foreign_key "team_members", "teams"
  add_foreign_key "translations", "languages"
  add_foreign_key "transshipments", "maritime_rates"
  add_foreign_key "transshipments", "ports"
  add_foreign_key "users", "roles"
end
