class CreateClientDocuments < ActiveRecord::Migration[6.1]
  def change
    create_table :client_documents do |t|
      t.references :client, null: false, foreign_key: true
      t.references :document, null: false, foreign_key: true
      t.timestamp :deleted_at

      t.timestamps
    end
    add_index :client_documents, :deleted_at
  end
end
