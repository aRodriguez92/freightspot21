class CreateAccessRoles < ActiveRecord::Migration[6.1]
  def change
    create_table :access_roles do |t|
      t.references :access, null: false, foreign_key: true
      t.references :role, null: false, foreign_key: true
      t.references :system_module, null: false, foreign_key: true

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :access_roles, :deleted_at
  end
end
