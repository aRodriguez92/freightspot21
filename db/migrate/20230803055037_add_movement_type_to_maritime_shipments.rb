class AddMovementTypeToMaritimeShipments < ActiveRecord::Migration[6.1]
  def change
    add_column :maritime_shipments, :movement_type, :bigint, default: 0
  end
end
