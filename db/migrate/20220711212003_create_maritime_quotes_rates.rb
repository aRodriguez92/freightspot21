class CreateMaritimeQuotesRates < ActiveRecord::Migration[6.1]
  def change
    create_table :maritime_quotes_rates , id: false  do |t|
      t.belongs_to :maritime_quote
      t.belongs_to :maritime_rate
    end
  end
end