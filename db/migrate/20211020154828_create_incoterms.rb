class CreateIncoterms < ActiveRecord::Migration[6.1]
  def change
    create_table :incoterms do |t|
      t.string :code
      t.string :description
      t.boolean :inactive, default: false
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :incoterms, :deleted_at
    add_index :incoterms, :slug
  end
end
