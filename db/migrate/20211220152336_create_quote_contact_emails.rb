class CreateQuoteContactEmails < ActiveRecord::Migration[6.1]
  def change
    create_table :quote_contact_emails do |t|
      t.string :code
      t.string :fullname
      t.string :company
      t.string :email
      t.string :phone
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :quote_contact_emails, :deleted_at
    add_index :quote_contact_emails, :slug
  end
end
