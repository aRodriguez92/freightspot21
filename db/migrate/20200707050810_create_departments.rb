class CreateDepartments < ActiveRecord::Migration[6.1]
  def change
    create_table :departments do |t|
      t.string :name
      t.references :branch, null: false, foreign_key: true
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :departments, :deleted_at
    add_index :departments, :slug, unique: true
  end
end
