class CreateMaritimeRateImports < ActiveRecord::Migration[6.1]
  def change
    create_table :maritime_rate_imports do |t|
      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :maritime_rate_imports, :deleted_at
  end
end
