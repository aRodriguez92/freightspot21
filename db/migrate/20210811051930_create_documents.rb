class CreateDocuments < ActiveRecord::Migration[6.1]
  def change
    create_table :documents do |t|
      t.string :name
      t.string :document_type
      t.string :phase
      t.boolean :obligatory
      t.boolean :active, default: true
      t.string :variable
      t.boolean :client, default: true
      t.boolean :supplier, default: true
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :documents, :deleted_at
    add_index :documents, :slug, unique: true
  end
end
