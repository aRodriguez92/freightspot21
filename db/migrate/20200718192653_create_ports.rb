class CreatePorts < ActiveRecord::Migration[6.1]
  def change
    create_table :ports do |t|
      t.string :identifier
      t.string :name
      t.integer :transportation_method
      t.boolean :inactive, default: false
      t.string :city
      t.references :country, foreign_key: true

      t.decimal :latitude, default: 0.0
      t.decimal :longitude, default: 0.0
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :ports, :deleted_at
    add_index :ports, :slug, unique: true
  end
end
