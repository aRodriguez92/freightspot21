class CreateCharges < ActiveRecord::Migration[6.1]
  def change
    create_table :charges do |t|
      t.string :code
      t.string :name
      t.boolean :active, default: true
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :charges, :deleted_at
    add_index :charges, :slug
  end
end
