class CreateContainerDimensions < ActiveRecord::Migration[6.1]
  def change
    create_table :container_dimensions do |t|
      t.string :name
      t.decimal :metric_value
      t.belongs_to :metric_measurement, foreign_key: { to_table: :units }
      t.decimal :imperial_value
      t.belongs_to :imperial_measurement, foreign_key: { to_table: :units }
      t.references :container, foreign_key: true
      t.string :slug, unique: true

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :container_dimensions, :deleted_at
    add_index :container_dimensions, :slug
  end
end
