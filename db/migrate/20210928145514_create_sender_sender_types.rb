class CreateSenderSenderTypes < ActiveRecord::Migration[6.1]
  def change
    create_table :sender_sender_types do |t|
      t.references :sender, foreign_key: true
      t.references :sender_type, foreign_key: true

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :sender_sender_types, :deleted_at
  end
end
