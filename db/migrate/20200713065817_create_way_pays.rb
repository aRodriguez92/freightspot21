class CreateWayPays < ActiveRecord::Migration[6.1]
  def change
    create_table :way_pays do |t|
      t.string :code
      t.string :name
      t.text :description
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :way_pays, :deleted_at
    add_index :way_pays, :slug, unique: true
  end
end
