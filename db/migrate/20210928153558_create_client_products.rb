class CreateClientProducts < ActiveRecord::Migration[6.1]
  def change
    create_table :client_products do |t|
      t.string :hs_code
      t.references :client, foreign_key: true
      t.references :product, foreign_key: true
      t.decimal :tariff_fraction
      t.text :description

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :client_products, :deleted_at
  end
end
