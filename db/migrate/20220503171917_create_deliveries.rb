class CreateDeliveries < ActiveRecord::Migration[6.1]
  def change
    create_table :deliveries do |t|
      t.references :deliverable, polymorphic: true
      t.string :street
      t.string :interior_number
      t.string :outdoor_number
      t.string :colony
      t.string :city
      t.string :state
      t.string :country
      t.string :zip_code

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :deliveries, :deleted_at
  end
end
