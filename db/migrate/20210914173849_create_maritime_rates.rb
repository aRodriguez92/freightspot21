class CreateMaritimeRates < ActiveRecord::Migration[6.1]
  def change
    create_table :maritime_rates do |t|
      t.belongs_to :origin, foreign_key: { to_table: :ports }
      t.belongs_to :destination, foreign_key: { to_table: :ports }
      t.belongs_to :shipper, foreign_key: { to_table: :suppliers }
      t.belongs_to :shipping_line, foreign_key: { to_table: :suppliers }
      t.references :freight_type, foreign_key: true
      t.references :container, foreign_key: true
      t.references :cargo_type, null: false, foreign_key: true
      t.bigint :number_of_transshipments
      t.integer :transit_time
      t.timestamp :rate_validity_start
      t.timestamp :rate_validity_end
      t.timestamp :departure_date
      t.decimal :freight_rate
      t.references :currency, foreign_key: true
      t.integer :free_days
      t.boolean :guarantee_letter
      t.text :remarks

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :maritime_rates, :deleted_at
  end
end
