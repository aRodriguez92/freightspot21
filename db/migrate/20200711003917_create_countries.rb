class CreateCountries < ActiveRecord::Migration[6.1]
  def change
    create_table :countries do |t|
      t.string :code
      t.string :name
      t.string :phonecode
      t.boolean :prohibited, default: false
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :countries, :deleted_at
    add_index :countries, :slug, unique: true
  end
end
