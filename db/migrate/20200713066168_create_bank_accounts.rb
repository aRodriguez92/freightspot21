class CreateBankAccounts < ActiveRecord::Migration[6.1]
  def change
    create_table :bank_accounts do |t|
      t.string :bank
      t.string :account_number
      t.string :owner
      t.references :client, null: false, foreign_key: true
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :bank_accounts, :deleted_at
    add_index :bank_accounts, :slug, unique: true
  end
end
