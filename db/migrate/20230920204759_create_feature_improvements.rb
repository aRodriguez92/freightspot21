class CreateFeatureImprovements < ActiveRecord::Migration[6.1]
  def change
    create_table :feature_improvements do |t|
      t.integer :changelog_id
      t.string :title
      t.string :description
      t.bigint :category

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :feature_improvements, :deleted_at
  end
end
