class CreateApprovals < ActiveRecord::Migration[6.1]
  def change
    create_table :approvals do |t|
      t.references :approvalable, polymorphic: true
      t.belongs_to :request_user, foreign_key: { to_table: :employees }
      t.belongs_to :respond_user, foreign_key: { to_table: :contacts }
      t.jsonb :preview_changes, null: false, default: {}
      t.jsonb :recent_changes, null: false, default: {}
      t.boolean :approved

      t.timestamps
      t.timestamp :deleted_at
      t.timestamp :requested_at
      t.timestamp :rejected_at
    end
    add_index :approvals, :deleted_at
  end
end
