class CreateAddressTypes < ActiveRecord::Migration[6.1]
  def change
    create_table :address_types do |t|
      t.string :name
      t.boolean :inactive, default: false
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :address_types, :deleted_at
    add_index :address_types, :slug
  end
end
