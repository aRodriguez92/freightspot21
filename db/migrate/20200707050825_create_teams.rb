class CreateTeams < ActiveRecord::Migration[6.1]
  def change
    create_table :teams do |t|
      t.string :name
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :teams, :deleted_at
    add_index :teams, :slug, unique: true
  end
end
