class CreatePaymentTerms < ActiveRecord::Migration[6.1]
  def change
    create_table :payment_terms do |t|
      t.string :description
      t.integer :amount_days_pay
      t.boolean :inactive, default: false
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :payment_terms, :deleted_at
    add_index :payment_terms, :slug, unique: true
  end
end
