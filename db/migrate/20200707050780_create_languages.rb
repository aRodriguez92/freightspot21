class CreateLanguages < ActiveRecord::Migration[6.1]
  def change
    create_table :languages do |t|
      t.string :code
      t.string :name
      t.boolean :default, default: false
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :languages, :deleted_at
    add_index :languages, :slug, unique: true
  end
end
