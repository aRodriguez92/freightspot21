class CreateTeamMembers < ActiveRecord::Migration[6.1]
  def change
    create_table :team_members do |t|
      t.references :team, foreign_key: true
      t.references :employee, foreign_key: true
      t.bigint :role, default: 0

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :team_members, :deleted_at
  end
end
