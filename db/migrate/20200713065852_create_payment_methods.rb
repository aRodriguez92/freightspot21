class CreatePaymentMethods < ActiveRecord::Migration[6.1]
  def change
    create_table :payment_methods do |t|
      t.string :code
      t.string :name
      t.text :description
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end
    
    add_index :payment_methods, :deleted_at
    add_index :payment_methods, :slug, unique: true
  end
end
