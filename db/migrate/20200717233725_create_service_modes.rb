class CreateServiceModes < ActiveRecord::Migration[6.1]
  def change
    create_table :service_modes do |t|
      t.string :code
      t.string :name
      t.bigint :variable
      t.references :service_modes, foreign_key: true
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :service_modes, :deleted_at
    add_index :service_modes, :slug, unique: true
  end
end
