class AddMiddlemenToClients < ActiveRecord::Migration[6.1]
  def change
    add_reference :clients, :middleman, foreign_key: true
  end
end
