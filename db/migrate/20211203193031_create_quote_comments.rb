class CreateQuoteComments < ActiveRecord::Migration[6.1]
  def change
    create_table :quote_comments do |t|
      t.references :maritime_quote, foreign_key: true
      t.references :user, foreign_key: true
      t.text :body

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :quote_comments, :deleted_at
  end
end
