class CreateCargoTypes < ActiveRecord::Migration[6.1]
  def change
    create_table :cargo_types do |t|
      t.string :code
      t.string :name
      t.timestamp :deleted_at
      t.string :slug

      t.timestamps
    end
    add_index :cargo_types, :deleted_at
    add_index :cargo_types, :slug
  end
end
