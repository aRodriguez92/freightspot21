class CreateEmployees < ActiveRecord::Migration[6.1]
  def change
    create_table :employees do |t|
      t.string :first_name
      t.string :last_name
      t.string :phone
      t.string :extension
      t.string :mobile
      t.string :origin
      t.string :city
      t.timestamp :birth_date
      t.string :job
      t.timestamp :join_date
      t.references :user, null: false, foreign_key: true
      t.references :department, foreign_key: true
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :employees, :deleted_at
    add_index :employees, :slug, unique: true
  end
end
