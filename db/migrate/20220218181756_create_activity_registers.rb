class CreateActivityRegisters < ActiveRecord::Migration[6.1]
  def change
    create_table :activity_registers do |t|
      t.references :activity_registerable, polymorphic: true
      t.references :user, foreign_key: true
      t.text :activity

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :activity_registers, :deleted_at
  end
end
