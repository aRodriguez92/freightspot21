class CreateMaritimeTrackingEvents < ActiveRecord::Migration[6.1]
  def change
    create_table :maritime_tracking_events do |t|
      t.references :maritime_shipments, foreign_key: true
      t.belongs_to :activity, foreign_key: { to_table: :shipment_activities }
      t.bigint :location_type
      t.belongs_to :location, foreign_key: { to_table: :ports }
      t.timestamp :tracking_event_at
      t.bigint :vehicle
      t.string :voyage_name
      t.string :voyage_number

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :maritime_tracking_events, :deleted_at
  end
end
