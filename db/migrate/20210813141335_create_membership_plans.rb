class CreateMembershipPlans < ActiveRecord::Migration[6.1]
  def change
    create_table :membership_plans do |t|
      t.string :code
      t.string :name
      t.decimal :price
      t.text :description
      t.boolean :active
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :membership_plans, :deleted_at
    add_index :membership_plans, :slug
  end
end
