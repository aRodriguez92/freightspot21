class CreateWarehouses < ActiveRecord::Migration[6.1]
  def change
    create_table :warehouses do |t|
      t.string :name
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :warehouses, :deleted_at
    add_index :warehouses, :slug, unique: true
  end
end
