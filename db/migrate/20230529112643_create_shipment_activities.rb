class CreateShipmentActivities < ActiveRecord::Migration[6.1]
  def change
    create_table :shipment_activities do |t|
      t.string :name

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :shipment_activities, :deleted_at
  end
end
