class AddRemarksToMaritimeShipments < ActiveRecord::Migration[6.1]
  def change
    add_column :maritime_shipments, :remarks, :text
  end
end
