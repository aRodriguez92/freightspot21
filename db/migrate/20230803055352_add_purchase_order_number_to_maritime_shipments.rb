class AddPurchaseOrderNumberToMaritimeShipments < ActiveRecord::Migration[6.1]
  def change
    add_column :maritime_shipments, :purchase_order_number, :string
  end
end
