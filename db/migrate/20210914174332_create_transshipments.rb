class CreateTransshipments < ActiveRecord::Migration[6.1]
  def change
    create_table :transshipments do |t|
      t.references :maritime_rate, null: false, foreign_key: true
      t.references :port, null: false, foreign_key: true
      t.timestamp :deleted_at

      t.timestamps
    end

    add_index :transshipments, :deleted_at
  end
end
