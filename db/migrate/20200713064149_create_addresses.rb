class CreateAddresses < ActiveRecord::Migration[6.1]
  def change
    create_table :addresses do |t|
      t.references :addressable, polymorphic: true
      t.string :name
      t.string :street
      t.string :interior_number
      t.string :outdoor_number
      t.string :colony
      t.string :city
      t.string :country
      t.references :state, foreign_key: true
      t.string :zip_code
      t.boolean :principal, default: false
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :addresses, :deleted_at
    add_index :addresses, :slug
  end
end
