class ChangeQuoteContactEmail < ActiveRecord::Migration[6.1]
  def change
    change_table :quote_contact_emails do |t|
      t.remove :fullname
      t.string :first_name
      t.string :last_name
    end
  end
end
