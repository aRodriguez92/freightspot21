class CreateMaritimeQuotes < ActiveRecord::Migration[6.1]
  def change
    create_table :maritime_quotes do |t|
      t.string :number
      t.text :description
      t.decimal :declared_value, precision: 30, scale: 4, default: 0
      t.timestamp :quoted_at
      t.timestamp :expired_at
      t.integer :free_day_of_delay
      t.decimal :total_pieces, precision: 10, scale: 4, default: 0
      t.decimal :total_weight, precision: 10 , scale: 4, default: 0
      t.decimal :total_volume, precision: 20, scale: 4, default: 0
      t.decimal :total_chargeable_weight, precision: 20, scale: 4, default: 0
      t.decimal :total_cost, precision: 20, scale: 4, default: 0
      t.integer :origin_route_id
      t.integer :destination_route_id

      #references
      t.references :maritime_rate, foreign_key: true
      t.references :client, foreign_key: true
      t.references :payment_term, foreign_key: true
      t.references :organization, foreign_key: true
      t.references :incoterm, foreign_key: true
      t.references :freight_type, foreign_key: true

      # Booleans
      t.boolean :danger_item, default: false
      t.boolean :guarantee_letter, default: false
      t.boolean :is_active_shipment, default: false

      # Enums
      t.bigint :frequency
      t.bigint :operation_type
      t.bigint :service_mode
      t.bigint :service_type
      t.bigint :status, default: 0

      # FriendlyId
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :maritime_quotes, :deleted_at
    add_index :maritime_quotes, :slug
  end
end
