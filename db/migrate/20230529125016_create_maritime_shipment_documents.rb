class CreateMaritimeShipmentDocuments < ActiveRecord::Migration[6.1]
  def change
    create_table :maritime_shipment_documents do |t|
      t.references :maritime_shipment, foreign_key: true
      t.bigint :document_type

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :maritime_shipment_documents, :deleted_at
  end
end
