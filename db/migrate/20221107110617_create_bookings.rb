class CreateBookings < ActiveRecord::Migration[6.1]
  def change
    create_table :bookings do |t|
      t.references :maritime_quote, null: false, foreign_key: true
      t.bigint :status, default: 1
      t.bigint :movement_type, default: 1

      t.timestamps
      t.timestamp :deleted_at
    end
  end
end
