class CreateMaritimeShipmentImports < ActiveRecord::Migration[6.1]
  def change
    create_table :maritime_shipment_imports do |t|

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :maritime_shipment_imports, :deleted_at
  end
end
