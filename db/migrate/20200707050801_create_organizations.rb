class CreateOrganizations < ActiveRecord::Migration[6.1]
  def change
    create_table :organizations do |t|
      t.string :name
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :organizations, :deleted_at
    add_index :organizations, :slug, unique: true
  end
end
