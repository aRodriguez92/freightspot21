class CreateIncidents < ActiveRecord::Migration[6.1]
  def change
    create_table :incidents do |t|
      t.references :incidentable, polymorphic: true, null: false
      t.string :activity
      t.text :comment
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :incidents, :deleted_at
    add_index :incidents, :slug, unique: true
  end
end
