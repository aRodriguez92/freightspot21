class CreateUnits < ActiveRecord::Migration[6.1]
  def change
    create_table :units do |t|
      t.string :code
      t.string :name
      t.bigint :unit_type
      t.integer :value, default: 0
      t.bigint :unit_system
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :units, :deleted_at
    add_index :units, :slug, unique: true
  end
end
