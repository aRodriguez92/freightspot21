class CreateStates < ActiveRecord::Migration[6.1]
  def change
    create_table :states do |t|
      t.string :code
      t.string :name
      t.references :country, null: false, foreign_key: true
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :states, :deleted_at
    add_index :states, :slug, unique: true
  end
end
