class AddArrivalTimeToMaritimeShipments < ActiveRecord::Migration[6.1]
  def change
    add_column :maritime_shipments, :arrival_time, :timestamp
    add_column :maritime_shipments, :earliest_to_latest, :timestamp
  end
end
