class CreateAccesses < ActiveRecord::Migration[6.1]
  def change
    create_table :accesses do |t|
      t.string :name
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :accesses, :deleted_at
    add_index :accesses, :slug, unique: true
  end
end
