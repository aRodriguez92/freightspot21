class CreateClients < ActiveRecord::Migration[6.1]
  def change
    create_table :clients do |t|
      t.string :name
      t.string :website
      t.string :rfc
      t.bigint :annual_international_freight_shipments
      t.boolean :has_credit, default: false
      t.boolean :authorized_credit, default: false
      t.boolean :advanced_parameters
      t.decimal :credit_limit, precision: 10, scale: 4, default: 0
      t.references :payment_term, foreign_key: true
      t.references :currency, foreign_key: true
      t.references :cfdi, foreign_key: true
      t.references :way_pay, foreign_key: true
      t.references :payment_method, foreign_key: true
      t.belongs_to :parent_entity, foreign_key: { to_table: :clients }
      t.boolean :inactive, default: false
      t.bigint :status, default: 0
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :clients, :deleted_at
    add_index :clients, :slug, unique: true
  end
end
