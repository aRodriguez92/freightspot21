class CreateTranslations < ActiveRecord::Migration[6.1]
  def change
    create_table :translations do |t|
      t.string :key
      t.references :language, foreign_key: true
      t.text :value

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :translations, :deleted_at
  end
end
