class CreatePhones < ActiveRecord::Migration[6.1]
  def change
    create_table :phones do |t|
      t.references :phoneable, polymorphic: true
      t.string :code
      t.string :number
      t.string :extension
      t.integer :type_phone
      t.boolean :principal, default: false
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :phones, :deleted_at
    add_index :phones, :slug, unique: true
  end
end
