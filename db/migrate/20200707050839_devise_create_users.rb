# frozen_string_literal: true

class DeviseCreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      ## Database authenticatable
      t.string :username
      t.string :email,              null: false, default: ""
      t.string :encrypted_password, null: false, default: ""
      t.references :role, foreign_key: true

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip

      ## Confirmable
      # t.string   :confirmation_token
      # t.datetime :confirmed_at
      # t.datetime :confirmation_sent_at
      # t.string   :unconfirmed_email # Only if using reconfirmable

      ## Invitable
      t.string     :invitation_token
      t.timestamp   :invitation_created_at
      t.timestamp   :invitation_sent_at
      t.timestamp   :invitation_accepted_at
      t.integer    :invitation_limit
      t.references :invited_by, polymorphic: true
      t.integer    :invitations_count, default: 0

      ## Lockable
      # t.integer  :failed_attempts, default: 0, null: false # Only if lock strategy is :failed_attempts
      # t.string   :unlock_token # Only if unlock strategy is :email or :both
      # t.datetime :locked_at

      t.timestamp :accepted_terms_at
      t.timestamp :accepted_privacy_at
      t.timestamp :announcements_read_at
      t.timestamp :last_seen_at
      t.boolean :admin, default: false
      t.boolean :approved, default: false
      t.boolean :accept_terms_of_service_and_privacy_policies, default: false
      t.string :slug

      t.timestamps null: false
      t.timestamp :deleted_at
    end

    add_index :users, :deleted_at
    add_index :users, :email,                unique: true
    add_index :users, :invitations_count
    add_index :users, :invitation_token
    add_index :users, :invited_by_id
    add_index :users, :reset_password_token, unique: true
    add_index :users, :slug, unique: true
    add_index :users, :username,                unique: true
    # add_index :users, :confirmation_token,   unique: true
    # add_index :users, :unlock_token,         unique: true
  end
end
