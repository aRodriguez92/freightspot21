class CreateTariffRates < ActiveRecord::Migration[6.1]
  def change
    create_table :tariff_rates do |t|
      t.string :hs_code
      t.string :nico
      t.text :description
      t.string :umt
      t.decimal :import_tax
      t.decimal :export_tax
      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :tariff_rates, :deleted_at
  end
end
