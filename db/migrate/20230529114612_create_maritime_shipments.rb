class CreateMaritimeShipments < ActiveRecord::Migration[6.1]
  def change
    create_table :maritime_shipments do |t|
      t.string :bl_number
      t.string :container_number
      t.string :number
      t.string :qr_code
      t.string :bar_code
      t.belongs_to :origin, foreign_key: { to_table: :ports }
      t.belongs_to :destination, foreign_key: { to_table: :ports }
      t.references :maritime_quote, foreign_key: true
      t.references :incoterm, foreign_key: true
      t.references :client, foreign_key: true
      t.references :team, foreign_key: true
      t.belongs_to :shipping_line, foreign_key: { to_table: :suppliers }
      t.belongs_to :operator, foreign_key: { to_table: :employees }
      t.belongs_to :sales_executive, foreign_key: { to_table: :employees }
      t.belongs_to :seller, foreign_key: { to_table: :employees }
      t.bigint :status, default: 0

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :maritime_shipments, :deleted_at
  end
end
