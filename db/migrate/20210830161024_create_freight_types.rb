class CreateFreightTypes < ActiveRecord::Migration[6.1]
  def change
    create_table :freight_types do |t|
      t.string :code
      t.string :name
      t.references :service_mode, foreign_key: true
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :freight_types, :deleted_at
    add_index :freight_types, :slug
  end
end
