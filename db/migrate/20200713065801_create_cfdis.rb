class CreateCfdis < ActiveRecord::Migration[6.1]
  def change
    create_table :cfdis do |t|
      t.string :code
      t.string :name
      t.text :description
      t.boolean :physical, default: false
      t.boolean :moral, default: false
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :cfdis, :deleted_at
    add_index :cfdis, :slug, unique: true
  end
end
