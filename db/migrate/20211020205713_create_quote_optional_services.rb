class CreateQuoteOptionalServices < ActiveRecord::Migration[6.1]
  def change
    create_table :quote_optional_services do |t|
      t.references :maritime_quote, foreign_key: true
      t.references :optional_service, foreign_key: true
      t.decimal :charge
      t.boolean :applied_charge, default: true

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :quote_optional_services, :deleted_at
  end
end
