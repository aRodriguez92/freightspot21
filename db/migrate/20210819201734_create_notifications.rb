class CreateNotifications < ActiveRecord::Migration[6.1]
  def change
    create_table :notifications do |t|
      t.references :recipient, polymorphic: true, null: false
      t.string :type, null: false
      t.jsonb :params
      t.datetime :read_at

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :notifications, :deleted_at
    add_index :notifications, :read_at
  end
end
