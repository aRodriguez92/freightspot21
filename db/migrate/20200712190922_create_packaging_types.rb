class CreatePackagingTypes < ActiveRecord::Migration[6.1]
  def change
    create_table :packaging_types do |t|
      t.string :code
      t.string :name
      t.text :definition
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end
    add_index :packaging_types, :deleted_at
    add_index :packaging_types, :slug, unique: true
  end
end
