class CreateAssignTypes < ActiveRecord::Migration[6.1]
  def change
    create_table :assign_types do |t|
      t.references :address, null: false, foreign_key: true
      t.references :address_type, null: false, foreign_key: true
      t.string :other
      t.timestamp :deleted_at

      t.timestamps
    end
    add_index :assign_types, :deleted_at
  end
end
