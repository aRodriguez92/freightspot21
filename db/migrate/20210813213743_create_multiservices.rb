class CreateMultiservices < ActiveRecord::Migration[6.1]
  def change
    create_table :multiservices do |t|
      t.references :supplier, null: false, foreign_key: true
      t.references :service_mode, null: false, foreign_key: true

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :multiservices, :deleted_at
  end
end
