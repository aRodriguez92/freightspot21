class AddReasonToMaritimeQuote < ActiveRecord::Migration[6.1]
  def change
    add_column :maritime_quotes, :reason_of_declined, :string
  end
end
