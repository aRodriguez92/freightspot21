class CreateSystemModules < ActiveRecord::Migration[6.1]
  def change
    create_table :system_modules do |t|
      t.string :name
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :system_modules, :deleted_at
    add_index :system_modules, :slug, unique: true
  end
end
