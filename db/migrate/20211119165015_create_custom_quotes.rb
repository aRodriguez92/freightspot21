class CreateCustomQuotes < ActiveRecord::Migration[6.1]
  def change
    create_table :custom_quotes do |t|
      t.belongs_to :rate, foreign_key: { to_table: :maritime_rates }
      t.references :employee, foreign_key: true
      t.string :customer
      t.string :email
      t.string :phone_number
      t.string :company_name

      t.timestamp :deleted_at

      t.timestamps
    end
    add_index :custom_quotes, :deleted_at
  end
end
