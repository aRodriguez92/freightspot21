class CreateChangelogs < ActiveRecord::Migration[6.1]
  def change
    create_table :changelogs do |t|
      t.string :version_number
      t.text :description

      t.timestamps
      t.timestamp :released_at
      t.timestamp :deleted_at
    end

    add_index :changelogs, :deleted_at
  end
end
