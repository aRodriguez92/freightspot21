class CreateContacts < ActiveRecord::Migration[6.1]
  def change
    create_table :contacts do |t|
      t.references :contactable, polymorphic: true
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :area
      t.string :job
      t.timestamp :birth_date
      t.boolean :principal, default: false
      t.references :user, foreign_key: true
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :contacts, :deleted_at
    add_index :contacts, :slug, unique: true
  end
end
