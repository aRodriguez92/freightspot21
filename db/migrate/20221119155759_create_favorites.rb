class CreateFavorites < ActiveRecord::Migration[6.1]
  def change
    create_table :favorites do |t|
      t.references :favoritable, polymorphic: true
      t.references :user, null: false, foreign_key: true

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :favorites, :deleted_at
  end
end
