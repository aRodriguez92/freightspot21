class CreateCustoms < ActiveRecord::Migration[6.1]
  def change
    create_table :customs do |t|
      t.string :code
      t.string :custom
      t.string :section
      t.decimal :rate
      t.decimal :fcl_expo_rate
      t.decimal :fcl_impo_rate
      t.decimal :lcl_expo_rate
      t.decimal :lcl_impo_rate
      t.boolean :active?, default: true
      t.references :service_mode, foreign_key: true
      t.string :slug

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :customs, :deleted_at
    add_index :customs, :slug, unique: true
  end
end
