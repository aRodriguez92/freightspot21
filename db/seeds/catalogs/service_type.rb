# enum type_service: [:door_door, :door_port, :port_port, :port_door]
# INCOTERMS:
# IMPO:
# EXW- Puerta a Puerta
ServiceType.create!(incoterm_id: 1, operation_type: 'import', name: 'door_door')
# FCA- Puerto a Puerta
ServiceType.create!(incoterm_id: 2, operation_type: 'import', name: 'port_door')
# CPT- Puerto Puerta
ServiceType.create!(incoterm_id: 3, operation_type: 'import', name: 'port_door')
# CIP- Puerto Puerta
ServiceType.create!(incoterm_id: 4, operation_type: 'import', name: 'port_door')
# DAP- Puerta a Puerta
ServiceType.create!(incoterm_id: 5, operation_type: 'import', name: 'door_door')
# DPU- Puerta a Puerta
ServiceType.create!(incoterm_id: 6, operation_type: 'import', name: 'door_door')
# DDP- Puerta a Puerta
ServiceType.create!(incoterm_id: 7, operation_type: 'import', name: 'door_door')
# FAS- Puerto a Puerta
ServiceType.create!(incoterm_id: 8, operation_type: 'import', name: 'port_door')
# FOB- Puerto a Puerta
ServiceType.create!(incoterm_id: 9, operation_type: 'import', name: 'port_door')
# CFR- Puerto a Puerta
ServiceType.create!(incoterm_id: 10, operation_type: 'import', name: 'port_door')
# CIF- Puerto a Puerta
ServiceType.create!(incoterm_id: 11, operation_type: 'import', name: 'port_door')

# EXPO:
# EXW- Puerta a Puerta
ServiceType.create!(incoterm_id: 1, operation_type: 'export', name: 'door_door')
# FCA- Puerta a Puerto
ServiceType.create!(incoterm_id: 2, operation_type: 'export', name: 'door_port')
# CPT- Puerta a Puerto
ServiceType.create!(incoterm_id: 3, operation_type: 'export', name: 'door_port')
# CIP- Puerta a Puerto
ServiceType.create!(incoterm_id: 4, operation_type: 'export', name: 'door_port')
# DAP- Puerta a Puerta
ServiceType.create!(incoterm_id: 5, operation_type: 'export', name: 'door_door')
# DPU- Puerta a Puerta
ServiceType.create!(incoterm_id: 6, operation_type: 'export', name: 'door_door')
# DDP- Puerta a Puerta
ServiceType.create!(incoterm_id: 7, operation_type: 'export', name: 'door_door')
# FAS- Puerta a Puerto
ServiceType.create!(incoterm_id: 8, operation_type: 'export', name: 'door_port')
# FOB- Puerta a Puerto
ServiceType.create!(incoterm_id: 9, operation_type: 'export', name: 'door_port')
# CFR- Puerta a Puerto
ServiceType.create!(incoterm_id: 10, operation_type: 'export', name: 'door_port')
# CIF- Puerta a Puerto
ServiceType.create!(incoterm_id: 11, operation_type: 'export', name: 'door_port')
