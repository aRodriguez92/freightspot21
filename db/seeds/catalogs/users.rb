rol_admin = Role.find_by(name: 'Administrador')
rol_super_admin = Role.find_by(name: 'Super Administrador')
rol_user = Role.find_by(name: 'Usuario')
rol_client = Role.find_by(name: 'Cliente')

User.create!(username: 'lilo', email: 'lili@freightspot.com', admin: true, password: '4xyI%$BLm23$', role_id: rol_admin.id,
             accept_terms_of_service_and_privacy_policies: true)
User.create!(username: 'arodriguez', email: 'arodriguez@freightspot.com', admin: true, password: 'I4ugD%rZd83V', role_id: rol_super_admin.id,
             accept_terms_of_service_and_privacy_policies: true)
User.create!(username: 'juan', email: 'juan@freightspot.com', admin: true, password: 'V%vIAE0@344q', role_id: rol_admin.id,
             accept_terms_of_service_and_privacy_policies: true)
User.create!(username: 'ana', email: 'ana@freightspot.com', admin: true, password: 's%u75kk6Skc3', role_id: rol_user.id,
             accept_terms_of_service_and_privacy_policies: true)
User.create!(username: 'jvidales', email: 'jvidales@freightspot.com', admin: true, password: '7CmyKS921&9G', role_id: rol_user.id,
             accept_terms_of_service_and_privacy_policies: true)
User.create!(username: 'test-user3', email: 'test-user3@freightspot.com', admin: false, password: 'WiOf6!I84n!!', role_id: rol_user.id,
             accept_terms_of_service_and_privacy_policies: true)
User.create!(username: 'test-user4', email: 'test-user4@freightspot.com', admin: false, password: 'y6u&pj@K!00Y', role_id: rol_user.id,
             accept_terms_of_service_and_privacy_policies: true)
User.create!(username: 'test-user5', email: 'test-user5@freightspot.com', admin: false, password: '26Zdx05$2X6T', role_id: rol_user.id,
             accept_terms_of_service_and_privacy_policies: true)
User.create!(username: 'jalvaro', email: 'jalvaro@freightspot.com', admin: false, password: 'JMlw&0t9IbX1', role_id: rol_client.id,
             accept_terms_of_service_and_privacy_policies: true)
User.find_each(&:save)
