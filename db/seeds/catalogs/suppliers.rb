# rol_client = Role.find_by(name: 'Cliente')

maritimo = ServiceMode.find_by(code: 'FM')
aereo = ServiceMode.find_by(code: 'FA')
customs = ServiceMode.find_by(code: 'SS')
terrestre = ServiceMode.find_by(code: 'FT')

fast_forward = Supplier.new
fast_forward.name = 'Fast Forward'
fast_forward.website = 'www.fastforward.mx'
fast_forward.currency_id = 2
fast_forward.carrier = true
fast_forward.save!

aeromexico = Supplier.new
aeromexico.name = 'Aeromexico'
aeromexico.website = 'www.aeromexico.com'
aeromexico.currency_id = 2
aeromexico.carrier = true
aeromexico.save!

Supplier.find_each(&:save)

Multiservice.create!(supplier_id: fast_forward.id, service_mode_id: maritimo.id)
Multiservice.create!(supplier_id: fast_forward.id, service_mode_id: terrestre.id)

Multiservice.create!(supplier_id: aeromexico.id, service_mode_id: aereo.id)
Multiservice.create!(supplier_id: aeromexico.id, service_mode_id: customs.id)

Multiservice.find_each(&:save)
