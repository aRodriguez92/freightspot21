# Servicios de Seguros
OptionalService.create!(name: 'insured_merchandise', cost: 75.00, category: 'insurance', active: true)
## Servicios de Peaje
# OptionalService.create(name: 'weighing_process', cost: 555.00, category: 'weighing', active: true)
## Servicios Adicionalesß
# OptionalService.create(name: 'bl_national', cost: 555.00, category: 'aditional', active: true)
# OptionalService.create(name: 'bl_international', cost: 555.00, category: 'aditional', active: true)
# OptionalService.create(name: 'packing_list', cost: 555.00, category: 'aditional', active: true)
# OptionalService.create(name: 'title_validation', cost: 555.00, category: 'aditional', active: true)
# OptionalService.create(name: 'export_certificate', cost: 555.00, category: 'aditional', active: true)
# OptionalService.create(name: 'origin_certificate', cost: 555.00, category: 'aditional', active: true)
# Servicios Aduanales
OptionalService.create!(name: 'customs_clearance', cost: 0.00, category: 'aduanal', active: true)
# servicios Terrestres
OptionalService.create!(name: 'safe_keeping', cost: 0.00, category: 'land', active: true)
# OptionalService.create(name: 'residential_pickup', cost: 555.00, category: 'land', active: true)
OptionalService.create!(name: 'collection_at_origin', cost: 0.00, category: 'land_service', active: true)
OptionalService.create!(name: 'delivery_at_destination', cost: 0.00, category: 'land_service', active: true)
