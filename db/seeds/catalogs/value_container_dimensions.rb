unit_kg = Unit.find_by(code: 'kg')
unit_lbs = Unit.find_by(code: 'lbs')
unit_ft = Unit.find_by(code: 'ft')
unit_ft3 = Unit.find_by(code: 'ft³')
unit_m = Unit.find_by(code: 'm')
unit_m3 = Unit.find_by(code: 'm³')

container1 = Container.find_by(code: "20' Standard")
container2 = Container.find_by(code: "40' Standard")
container3 = Container.find_by(code: "40' HC")

############### Dimensions for Container 1 ###############
ContainerDimension.create!(
  name: 'Tare Weight',
  metric_value: 2250,
  metric_measurement_id: unit_kg.id,
  imperial_value: 4960.40,
  imperial_measurement_id: unit_lbs.id,
  container_id: container1.id
)

ContainerDimension.create!(
  name: 'Payload Capacity',
  metric_value: 28_230,
  metric_measurement_id: unit_kg.id,
  imperial_value: 62_236.49,
  imperial_measurement_id: unit_lbs.id,
  container_id: container1.id
)

ContainerDimension.create!(
  name: 'Maximum Gross Weight',
  metric_value: 30_480,
  metric_measurement_id: unit_kg.id,
  imperial_value: 67_196.89,
  imperial_measurement_id: unit_lbs.id,
  container_id: container1.id
)

ContainerDimension.create!(
  name: 'Cubic Capacity',
  metric_value: 33.08,
  metric_measurement_id: unit_m3.id,
  imperial_value: 1168.26,
  imperial_measurement_id: unit_ft3.id,
  container_id: container1.id
)

ContainerDimension.create!(
  name: 'Internal Length',
  metric_value: 5.89,
  metric_measurement_id: unit_m.id,
  imperial_value: 19.32,
  imperial_measurement_id: unit_ft.id,
  container_id: container1.id
)

ContainerDimension.create!(
  name: 'Internal Width',
  metric_value: 2.35,
  metric_measurement_id: unit_m.id,
  imperial_value: 7.70,
  imperial_measurement_id: unit_ft.id,
  container_id: container1.id
)

ContainerDimension.create!(
  name: 'Internal Height',
  metric_value: 2.39,
  metric_measurement_id: unit_m.id,
  imperial_value: 7.84,
  imperial_measurement_id: unit_ft.id,
  container_id: container1.id
)

ContainerDimension.create!(
  name: 'External Length',
  metric_value: 6.05,
  metric_measurement_id: unit_m.id,
  imperial_value: 19.84,
  imperial_measurement_id: unit_ft.id,
  container_id: container1.id
)

ContainerDimension.create!(
  name: 'External Width',
  metric_value: 2.43,
  metric_measurement_id: unit_m.id,
  imperial_value: 7.97,
  imperial_measurement_id: unit_ft.id,
  container_id: container1.id
)

ContainerDimension.create!(
  name: 'External Height',
  metric_value: 2.59,
  metric_measurement_id: unit_m.id,
  imperial_value: 8.49,
  imperial_measurement_id: unit_ft.id,
  container_id: container1.id
)

ContainerDimension.create!(
  name: 'Door Opening Width',
  metric_value: 2.34,
  metric_measurement_id: unit_m.id,
  imperial_value: 7.67,
  imperial_measurement_id: unit_ft.id,
  container_id: container1.id
)

ContainerDimension.create!(
  name: 'Door Opening Height',
  metric_value: 2.36,
  metric_measurement_id: unit_m.id,
  imperial_value: 7.74,
  imperial_measurement_id: unit_ft.id,
  container_id: container1.id
)
############### Dimensions for Container 1 ###############

############### Dimensions for Container 2 ###############
ContainerDimension.create!(
  name: 'Tare Weight',
  metric_value: 3780,
  metric_measurement_id: unit_kg.id,
  imperial_value: 8333.47,
  imperial_measurement_id: unit_lbs.id,
  container_id: container2.id
)

ContainerDimension.create!(
  name: 'Payload Capacity',
  metric_value: 26_700,
  metric_measurement_id: unit_kg.id,
  imperial_value: 58_863.42,
  imperial_measurement_id: unit_lbs.id,
  container_id: container2.id
)

ContainerDimension.create!(
  name: 'Maximum Gross Weight',
  metric_value: 30_480,
  metric_measurement_id: unit_kg.id,
  imperial_value: 67_196.89,
  imperial_measurement_id: unit_lbs.id,
  container_id: container2.id
)

ContainerDimension.create!(
  name: 'Cubic Capacity',
  metric_value: 67.56,
  metric_measurement_id: unit_m3.id,
  imperial_value: 2386.11,
  imperial_measurement_id: unit_ft3.id,
  container_id: container2.id
)

ContainerDimension.create!(
  name: 'Internal Length',
  metric_value: 12.03,
  metric_measurement_id: unit_m.id,
  imperial_value: 39.46,
  imperial_measurement_id: unit_ft.id,
  container_id: container2.id
)

ContainerDimension.create!(
  name: 'Internal Width',
  metric_value: 2.35,
  metric_measurement_id: unit_m.id,
  imperial_value: 7.70,
  imperial_measurement_id: unit_ft.id,
  container_id: container2.id
)

ContainerDimension.create!(
  name: 'Internal Height',
  metric_value: 2.39,
  metric_measurement_id: unit_m.id,
  imperial_value: 7.84,
  imperial_measurement_id: unit_ft.id,
  container_id: container2.id
)

ContainerDimension.create!(
  name: 'External Length',
  metric_value: 12.19,
  metric_measurement_id: unit_m.id,
  imperial_value: 39.99,
  imperial_measurement_id: unit_ft.id,
  container_id: container2.id
)

ContainerDimension.create!(
  name: 'External Width',
  metric_value: 2.43,
  metric_measurement_id: unit_m.id,
  imperial_value: 7.97,
  imperial_measurement_id: unit_ft.id,
  container_id: container2.id
)

ContainerDimension.create!(
  name: 'External Height',
  metric_value: 2.59,
  metric_measurement_id: unit_m.id,
  imperial_value: 8.49,
  imperial_measurement_id: unit_ft.id,
  container_id: container2.id
)

ContainerDimension.create!(
  name: 'Door Opening Width',
  metric_value: 2.34,
  metric_measurement_id: unit_m.id,
  imperial_value: 7.67,
  imperial_measurement_id: unit_ft.id,
  container_id: container2.id
)

ContainerDimension.create!(
  name: 'Door Opening Height',
  metric_value: 2.38,
  metric_measurement_id: unit_m.id,
  imperial_value: 7.80,
  imperial_measurement_id: unit_ft.id,
  container_id: container2.id
)
############### Dimensions for Container 2 ###############

############### Dimensions for Container 3 ###############
ContainerDimension.create!(
  name: 'Tare Weight',
  metric_value: 4020,
  metric_measurement_id: unit_kg.id,
  imperial_value: 8862.58,
  imperial_measurement_id: unit_lbs.id,
  container_id: container3.id
)

ContainerDimension.create!(
  name: 'Payload Capacity',
  metric_value: 26_460,
  metric_measurement_id: unit_kg.id,
  imperial_value: 58_334.31,
  imperial_measurement_id: unit_lbs.id,
  container_id: container3.id
)

ContainerDimension.create!(
  name: 'Maximum Gross Weight',
  metric_value: 30_480,
  metric_measurement_id: unit_kg.id,
  imperial_value: 67_196.89,
  imperial_measurement_id: unit_lbs.id,
  container_id: container3.id
)

ContainerDimension.create!(
  name: 'Cubic Capacity',
  metric_value: 76.04,
  metric_measurement_id: unit_m3.id,
  imperial_value: 2685.62,
  imperial_measurement_id: unit_ft3.id,
  container_id: container3.id
)

ContainerDimension.create!(
  name: 'Internal Length',
  metric_value: 12.03,
  metric_measurement_id: unit_m.id,
  imperial_value: 39.46,
  imperial_measurement_id: unit_ft.id,
  container_id: container3.id
)

ContainerDimension.create!(
  name: 'Internal Width',
  metric_value: 2.35,
  metric_measurement_id: unit_m.id,
  imperial_value: 7.70,
  imperial_measurement_id: unit_ft.id,
  container_id: container3.id
)

ContainerDimension.create!(
  name: 'Internal Height',
  metric_value: 2.69,
  metric_measurement_id: unit_m.id,
  imperial_value: 8.82,
  imperial_measurement_id: unit_ft.id,
  container_id: container3.id
)

ContainerDimension.create!(
  name: 'External Length',
  metric_value: 12.19,
  metric_measurement_id: unit_m.id,
  imperial_value: 39.99,
  imperial_measurement_id: unit_ft.id,
  container_id: container3.id
)

ContainerDimension.create!(
  name: 'External Width',
  metric_value: 2.43,
  metric_measurement_id: unit_m.id,
  imperial_value: 7.97,
  imperial_measurement_id: unit_ft.id,
  container_id: container3.id
)

ContainerDimension.create!(
  name: 'External Height',
  metric_value: 2.89,
  metric_measurement_id: unit_m.id,
  imperial_value: 9.48,
  imperial_measurement_id: unit_ft.id,
  container_id: container3.id
)

ContainerDimension.create!(
  name: 'Door Opening Width',
  metric_value: 2.34,
  metric_measurement_id: unit_m.id,
  imperial_value: 7.67,
  imperial_measurement_id: unit_ft.id,
  container_id: container3.id
)

ContainerDimension.create!(
  name: 'Door Opening Height',
  metric_value: 2.6,
  metric_measurement_id: unit_m.id,
  imperial_value: 8.53,
  imperial_measurement_id: unit_ft.id,
  container_id: container3.id
)
############### Dimensions for Container 3 ###############
