Language.create!(code: 'es', name: 'Español', default: true)
Language.create!(code: 'en', name: 'Ingles', default: false)
Language.find_each(&:save)
