PackagingType.create!(code: 'AE', name: 'Aerosol', definition: 'A gas-tight, pressure-resistant container with a valve and propellant.
When the valve is opened, propellant forces the product from the container in a fine or coarse spray pattern or stream. (e.g., a
spray can dispensing paint, furniture polish, etc, under pressure). It does not include atomizers, because atomizers do not rely
on a pressurised container to propel product from the container.')
PackagingType.create!(code: 'AMM', name: 'Ammo Pack', definition: 'Ammo Pack')
PackagingType.create!(code: 'AMP', name: 'Ampoule', definition: 'A relatively small container made from glass or plastic tubing, the end of which is
drawn into a stem and closed by fusion after filling. The bottom may be flat, convex, or drawn out. An ampule is opened by breaking the stem.')
PackagingType.create!(code: 'AT', name: 'Atomizer', definition: 'A device for reducing a liquid to a fine spray. (e.g., medicine, perfume, etc). An
atomizer does not rely on a pressurised container for the propellant. Usually air is provided by squeezing a rubber bulb attached to the atomizer.')
PackagingType.create!(code: 'ATH', name: 'Attachment', definition: 'In containers and shipping devices, a component that can be added to provide
additional functionality or security as required by the contents or method of transportation/handling')
PackagingType.create!(code: 'BAG', name: 'Bag', definition: 'A preformed, flexible container, generally enclosed on all but one side, which forms an
opening that may or may not be sealed after filling.')
PackagingType.create!(code: 'GBG', name: 'Bag-In-Box or BIB', definition: 'A type of container for the storage and transportation of liquids.
It consists of a strong bladder, usually made of aluminium PET film or other plastics seated inside a corrugated fibreboard box. The box and
internal bag can be fused together. In most cases there is nozzle or valve fixed to the bag. The nozzle can be connected easily to
a dispensing installation or the valve allows for convenient dispensing.')
PackagingType.create!(code: 'BAL', name: 'Bale', definition: 'Bale')
PackagingType.create!(code: 'BDG', name: 'Banding', definition: 'Something that binds, ties, or encircles the package/container to secure and maintain
unit integrity')
PackagingType.create!(code: 'BRG', name: 'Barge', definition: 'Barge')
PackagingType.create!(code: 'BBL', name: 'Barrel', definition: 'A cylindrical packaging whose bottom end is permanently fixed to the body and top
end (head) is either removable or nonremovable.')
PackagingType.create!(code: 'BSK', name: 'Basket or hamper', definition: 'A semi rigid container usually open at the top traditionally used for
gathering, shipping and marketing agricultural products.')
PackagingType.create!(code: 'BEM', name: 'Beam', definition: 'Beam')
PackagingType.create!(code: 'BLT', name: 'Belting', definition: 'As pertains to containers and shipping devices, a method of securing the contents to
the conveyance device (or securing components of the shipping device to each other) using one or more bands of flexible material having high-tensile
strength and a buckle or ratchet device for removing slack and maintaining tension')
PackagingType.create!(code: 'BIN', name: 'Bin', definition: 'Bin')
PackagingType.create!(code: 'BIC', name: 'Bing Chest', definition: 'Bing Chest')
PackagingType.create!(code: 'BME', name: 'Blister Pack', definition: 'A type of packaging in which the item is secured between a preformed (usually
transparent plastic) dome or “bubble” and a paperboard surface or “carrier.” Attachment may be by stapling, heatsealing, gluing, or other means.
In other instances, the blister folds over the product in clam-shell fashion to form an enclosing container. Blisters are most usually thermoformed
from polyvinyl chloride; however, almost any thermoplastic can be thermoformed into a blister.')
PackagingType.create!(code: 'BOB', name: 'Bobbin', definition: 'Bobbin')
PackagingType.find_each(&:save)
