State.create!(name: 'Badahšan', country_id: 1, code: 'BDS')
State.create!(name: 'Bādgīs', country_id: 1, code: 'BDG')
State.create!(name: 'Baġlān', country_id: 1, code: 'BGL')
State.create!(name: 'Balh', country_id: 1, code: 'BAL')
State.create!(name: 'Bāmiyān', country_id: 1, code: 'BAM')
State.create!(name: 'Daikondi', country_id: 1, code: 'DAY')
State.create!(name: 'Farāh', country_id: 1, code: 'FRA')
State.create!(name: 'Fāryāb', country_id: 1, code: 'FYB')
State.create!(name: 'Ġaznī', country_id: 1, code: 'GHA')
State.create!(name: 'Ġawr', country_id: 1, code: 'GHO')
State.create!(name: 'Jost', country_id: 1, code: 'KHO')
State.create!(name: 'Helmand', country_id: 1, code: 'HEL')
State.create!(name: 'Herāt', country_id: 1, code: 'HER')
State.create!(name: 'Jawzjān', country_id: 1, code: 'JOW')
State.create!(name: 'Kabul', country_id: 1, code: 'KAB')
State.create!(name: 'Kandahar', country_id: 1, code: 'KAN')
State.create!(name: 'Kāpīsā', country_id: 1, code: 'KAP')
State.create!(name: 'Kunar', country_id: 1, code: 'KNR')
State.create!(name: 'Laġmān', country_id: 1, code: 'LAG')
State.create!(name: 'Lawgar', country_id: 1, code: 'LOW')
State.create!(name: 'Nangarhar', country_id: 1, code: 'NAN')
State.create!(name: 'Nimruz', country_id: 1, code: 'NIM')
State.create!(name: 'Nūristān', country_id: 1, code: 'NUR')
State.create!(name: 'Paktiyā', country_id: 1, code: 'PIA')
State.create!(name: 'Paktīkā', country_id: 1, code: 'PKA')
State.create!(name: 'Panjshīr', country_id: 1, code: 'PAN')
State.create!(name: 'Parvān', country_id: 1, code: 'PAR')
State.create!(name: 'Qundūz', country_id: 1, code: 'KDZ')
State.create!(name: 'Samangān', country_id: 1, code: 'SAM')
State.create!(name: 'Sar-e Pul', country_id: 1, code: 'SAR')
State.create!(name: 'Tahār', country_id: 1, code: 'TAK')
State.create!(name: 'Urūzgān', country_id: 1, code: 'ORU')
State.create!(name: 'Vardak', country_id: 1, code: 'ORU')
State.create!(name: 'Zābul', country_id: 1, code: 'ZAB')
