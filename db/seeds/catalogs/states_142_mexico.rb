# States of Mexico
State.create!(name: 'Aguascalientes', country_id: 142, code: 'AGU')
State.create!(name: 'Baja California', country_id: 142, code: 'BCN')
State.create!(name: 'Baja California Sur', country_id: 142, code: 'BCS')
State.create!(name: 'Campeche', country_id: 142, code: 'CAM')
State.create!(name: 'Chiapas', country_id: 142, code: 'CHP')
State.create!(name: 'Chihuahua', country_id: 142, code: 'CHH')
State.create!(name: 'Ciudad de México', country_id: 142, code: 'CMX')
State.create!(name: 'Coahuila', country_id: 142, code: 'COA')
State.create!(name: 'Colima', country_id: 142, code: 'COL')
State.create!(name: 'Durango', country_id: 142, code: 'DUR')
State.create!(name: 'Guanajuato', country_id: 142, code: 'GUA')
State.create!(name: 'Guerrero', country_id: 142, code: 'GRO')
State.create!(name: 'Hidalgo', country_id: 142, code: 'HID')
State.create!(name: 'Jalisco', country_id: 142, code: 'HID')
State.create!(name: 'México', country_id: 142, code: 'MEX')
State.create!(name: 'Michoacán', country_id: 142, code: 'MIC')
State.create!(name: 'Morelos', country_id: 142, code: 'MOR')
State.create!(name: 'Nayarit', country_id: 142, code: 'NAY')
State.create!(name: 'Nuevo León', country_id: 142, code: 'NLE')
State.create!(name: 'Oaxaca', country_id: 142, code: 'OAX')
State.create!(name: 'Puebla', country_id: 142, code: 'PUE')
State.create!(name: 'Querétaro', country_id: 142, code: 'QUE')
State.create!(name: 'Quintana Roo', country_id: 142, code: 'ROO')
State.create!(name: 'San Luis Potosí', country_id: 142, code: 'SLP')
State.create!(name: 'Sinaloa', country_id: 142, code: 'SIN')
State.create!(name: 'Sonora', country_id: 142, code: 'SON')
State.create!(name: 'Tamaulipas', country_id: 142, code: 'TAM')
State.create!(name: 'Tabasco', country_id: 142, code: 'TAB')
State.create!(name: 'Tlaxcala', country_id: 142, code: 'TLA')
State.create!(name: 'Veracruz', country_id: 142, code: 'VER')
State.create!(name: 'Yucatán', country_id: 142, code: 'YUC')
State.create!(name: 'Zacatecas', country_id: 142, code: 'ZAC')
