WayPay.create!(code: '001', name: 	'Efectivo', description: 'Consiste en el pago realizado con dinero directamente en el establecimiento.')
WayPay.create!(code: '002', name: 	'Cheque Nominativo',
               description: 'La empresa o persona que paga el servicio emite un cheque a nombre del beneficiado.')
WayPay.create!(code: '003', name: 	'Transferencia Electrónica de Fondos SPEI',
               description: '	Transferencia electrónica de banco a banco sin importar si son cuentahabiente de la misma institución.')
WayPay.create!(code: '004', name: 	'Tarjeta de Crédito',
               description: '	El pago se realiza con tarjeta de crédito y regularmente se especifica en la factura la terminación de la tarjeta.')
WayPay.create!(code: '005', name: 	'Monedero Electrónico',
               description: '	Al ser un monedero autorizado por el SAT, se puede realizar el pago con el monedero electrónico.')
WayPay.create!(code: '006', name: 	'Dinero Electrónico', description: '	El dinero electrónico es el capital almacenado en un hardware.')
WayPay.create!(code: '008', name: 	'Vales de Despensa', description: '	Vales que el contribuyente es acreedor por parte de una empresa.')
WayPay.create!(code: '012', name: 	'Dación en Pago', description: '	Consiste en el pago por medio de un bien sobre lo que se adquirió.')
WayPay.create!(code: '013', name: 	'Pago por Subrogación',
               description: '	Cuando quien realiza el pago lo está realizando por medio de un derecho de éste, ejemplo: una herencia.')
WayPay.create!(code: '014', name: 	'Pago por Consignación',
               description: '	Consiste en el pago por medio del tribunal correspondiente. Solo un juez dará finalización a esta acción.')
WayPay.create!(code: '015', name: 	'Condonación', description: '	Cuando la deuda es perdonada.')
WayPay.create!(code: '017', name: 	'Compensación', description: '	Tanto deudor como acreedor tienen la misma obligación de pago.')
WayPay.create!(code: '023', name: 	'Novación', description: '	Se trata de un pago sobre un cambio, es decir, que la obligación inicial cambia.')
WayPay.create!(code: '024', name: 	'Confusión', description: '	Cuando el pago lo realizará el mismo que deberá pagarla.')
WayPay.create!(code: '025', name: 	'Remisión de Deuda',
               description: '	Si se estipula que el deudor ya no pagará, deberá estar especificado por una remisión de deuda.')
WayPay.create!(code: '026', name: 	'Prescripción o Caducidad', description: '	Cuando después de un determinado tiempo por ley no procede el pago.')
WayPay.create!(code: '027', name: 	'A Satisfacción del Acreedor',
               description: '	Cuando el acreedor en convenio con el deudor han firmado de no pago.')
WayPay.create!(code: '028', name: 	'Tarjeta de Débito',
               description: '	Sin importar la institución de la que se trate la tarjeta, éste siempre deberá tener fondos.')
WayPay.create!(code: '029', name: 	'Tarjeta de Servicios', description: '	Mientras la tarjeta esté emitida por el sistema financiero mexicano.')
WayPay.create!(code: '030', name: 	'Aplicación de Anticipos',
               description: '	Se trata de pagos anticipados, estos deberán verse reflejado en complemento de factura.')
WayPay.create!(code: '031', name: 	'Intermediario Pagos', description: '	Una tercera persona realizará el pago.')
WayPay.create!(code: '099', name: 	'Por Definir',
               description: '	Esta opción se especificará para que el contribuyente dé el trato contable que le interese.')
WayPay.find_each(&:save)
