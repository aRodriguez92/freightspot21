PaymentMethod.create!(code: 'PUE', name: 'Pago en Una Exhibición',
                      description: 'Consta de la realización del pago al contado al momento de la transacción, sin importar la forma de pago en la
este se realice.')
PaymentMethod.create!(code: 'PIP', name: 'Pago Inicial y Parcialidades',
                      description: 'Esta opción debe ser elegida si el caso es que se genera un pago inicial al momento de la transacción y el resto
se genera en uno o varios pagos posteriores.')
PaymentMethod.create!(code: 'PPD', name: 'Pago en Parcialidades o Diferido',
                      description: 'Esta opción se genera cuando se realiza un pago póstumo a la fecha de la transacción o emisión del CFDI.')
PaymentMethod.find_each(&:save)
