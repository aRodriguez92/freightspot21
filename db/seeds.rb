# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, name:{ name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', name:movie: movies.first)
#
Dir[Rails.root.join('db/seeds/catalogs/*.rb')].each do |seed|
  puts "Loading #{seed} ...."
  load seed
  puts "DONE\n"
end

Rails.logger.debug "\n\nGenerando una sucursal..."
branch = Branch.create!(name: 'Sucursal #1', organization_id: 1)
Branch.find_each(&:save)
Rails.logger.debug "\nSucursal generada exitosamente."

Rails.logger.debug "\n\nGenerando departamentos..."
department1 = Department.create!(name: 'Tecnologias', branch_id: branch.id)
department2 = Department.create!(name: 'Recursos Humanos', branch_id: branch.id)
department3 = Department.create!(name: 'Ventas', branch_id: branch.id)
Department.find_each(&:save)
Rails.logger.debug "\nDepartamentos generados exitosamente."

Rails.logger.debug "\n\nBuscando usuarios..."
user1 = User.find_by(username: 'arodriguez')
Rails.logger.debug "\nUsuario arodriguez encontrado."
user2 = User.find_by(username: 'lilo')
Rails.logger.debug "\nUsuario lilo encontrado."
user3 = User.find_by(username: 'juan')
Rails.logger.debug "\nUsuario juan encontrado."
user4 = User.find_by(username: 'ana')
Rails.logger.debug "\nUsuario ana encontrado."
user5 = User.find_by(username: 'test-user3')
Rails.logger.debug "\nUsuario test-user3 encontrado."
user6 = User.find_by(username: 'test-user4')
Rails.logger.debug "\nUsuario test-user4 encontrado."
user7 = User.find_by(username: 'test-user5')
Rails.logger.debug "\nUsuario test-user5 encontrado."
user8 = User.find_by(username: 'jvidales')
Rails.logger.debug "\nUsuario jvidales encontrado."

Rails.logger.debug "\n\nGenerando informacion de los empleados..."
Employee.create!(first_name: 'Ivan Ali', last_name: 'Rodriguez Vasquez', mobile: '+529531075019', origin: 'Mexico', city: 'CDMX',
                 birth_date: '1992/09/17', job: 'Developer', join_date: '2020/07/01', user_id: user1.id, department_id: department1.id)
Employee.create!(first_name: 'Jesus', last_name: 'Vidales', mobile: '+529531182702', origin: 'Mexico', city: 'Oaxaca',
                 birth_date: '1986/10/30', job: 'Developer', join_date: '2020/07/01', user_id: user8.id, department_id: department1.id)
Employee.create!(first_name: 'karenia', last_name: 'Luna', mobile: '+525555832737', origin: 'Mexico', city: 'CDMX',
                 birth_date: '1984/04/10', job: 'Boss', join_date: '2017/03/01', user_id: user2.id, department_id: department2.id)
Employee.create!(name: 'Test User1', mobile: '+525555832737', origin: 'Mexico', city: 'CDMX', birth_date: '1984/04/10',
                 job: 'Boss', join_date: '2017/03/01', user_id: user3.id, department_id: department2.id)
Employee.create!(name: 'Test User2', mobile: '+525555832737', origin: 'Mexico', city: 'CDMX', birth_date: '1984/04/10',
                 job: 'Boss', join_date: '2017/03/01', user_id: user4.id, department_id: department3.id)
Employee.create!(name: 'Test User3', mobile: '+525555832737', origin: 'Mexico', city: 'CDMX', birth_date: '1984/04/10',
                 job: 'Boss', join_date: '2017/03/01', user_id: user5.id, department_id: department3.id)
Employee.create!(name: 'Test User4', mobile: '+525555832737', origin: 'Mexico', city: 'CDMX', birth_date: '1984/04/10',
                 job: 'Boss', join_date: '2017/03/01', user_id: user6.id, department_id: department2.id)
Employee.create!(name: 'Test User5', mobile: '+525555832737', origin: 'Mexico', city: 'CDMX', birth_date: '1984/04/10',
                 job: 'Boss', join_date: '2017/03/01', user_id: user7.id, department_id: department1.id)
Rails.logger.debug "\nInformacion de empleados generada..."

middleman = Middleman.create!(rfc: 'ROVI9209LI3', name: 'Ivan Ali Rodriguez Vasquez', website: 'www.example.com', currency_id: 1, payment_term_id: 1)
Middleman.find_each(&:save)
Address.create!(addressable_type: 'Middleman', addressable_id: middleman.id, street: 'SALINA CRUZ', interior_number: '6', country: 'MEXICO',
                zip_code: '70110', principal: true, colony: 'MODERNA', state_id: 208, name: 'Addres #1')
Phone.create!(phoneable_type: 'Middleman', phoneable_id: middleman.id, code: '52', number: '9711009012', principal: true, type_phone: 0)
contact_middleman = Contact.new(contactable_type: 'Middleman', contactable_id: middleman.id, name: 'William Nolasco de la Cruz',
                                email: 'wnolasco@gmail.com', principal: true, birth_date: '1992-08-11 19:08:27.108077')
contact_middleman.save!
Address.create!(addressable_type: 'Contact', addressable_id: contact_middleman.id, street: 'Gomez Farias', interior_number: '12', country: 'MEXICO',
                zip_code: '70110', principal: true, colony: 'MODERNA', state_id: 208, name: 'Addres #1')
Phone.create!(phoneable_type: 'Contact', phoneable_id: contact_middleman.id, code: '+52', number: '9531202393', principal: true, type_phone: 0)

client = Client.create!(name: 'IBAÑES S.A DE C.V', website: 'www.ibañes.com', currency_id: 1, payment_term_id: 1, middleman_id: middleman.id)
Client.find_each(&:save)
Address.create!(addressable_type: 'Client', addressable_id: client.id, street: 'SALINA CRUZ', interior_number: '6', country: 'MEXICO',
                zip_code: '70110', principal: true, colony: 'MODERNA', state_id: 208, name: 'Addres #1')
Phone.create!(phoneable_type: 'Client', phoneable_id: client.id, code: '52', number: '9711009012', principal: true, type_phone: 0)
user_client = User.find_by(username: 'jalvaro')
Rails.logger.debug "\nUsuario jalvaro encontrado."
contact = Contact.create!(contactable_type: 'Client', contactable_id: client.id, name: 'Jose Alvaro Ramirez Santiago', email: 'jalvaro@gmail.com',
                          principal: true, birth_date: '1990-08-13 19:08:27.108077', job: 'Jefe', area: 'IT', user_id: user_client.id)
Address.create!(addressable_type: 'Contact', addressable_id: contact.id, street: 'SALINA CRUZ', interior_number: '6', country: 'MEXICO',
                zip_code: '70110', principal: true, colony: 'MODERNA', state_id: 208, name: 'Addres #1')
Phone.create!(phoneable_type: 'Contact', phoneable_id: contact.id, code: '+52', number: '9711009012', principal: true, type_phone: 0)
Address.find_each(&:save)
Phone.find_each(&:save)

Rails.logger.debug "\n\nBuscando acceso administrativo ..."
access = Access.find_by(name: 'manage')
Rails.logger.debug "\n\nAcceso administrativo encontrado ..."

Rails.logger.debug "\n\nBuscando módulos del sistema ..."
agents_module = SystemModule.find_by(name: 'Agent')
clients_module = SystemModule.find_by(name: 'Client')
criteria_module = SystemModule.find_by(name: 'Criterium')
containers_module = SystemModule.find_by(name: 'Container')
documents_module = SystemModule.find_by(name: 'Document')
middlemen_module = SystemModule.find_by(name: 'Middleman')
packaging_types_module = SystemModule.find_by(name: 'PackagingType')
ports_module = SystemModule.find_by(name: 'Port')
quotes_module = SystemModule.find_by(name: 'Quote')
roles_module = SystemModule.find_by(name: 'Role')
services_module = SystemModule.find_by(name: 'Service')
shipments_module = SystemModule.find_by(name: 'Shipment')
suppliers_module = SystemModule.find_by(name: 'Supplier')
tariffs_module = SystemModule.find_by(name: 'Tariff')
taxes_module = SystemModule.find_by(name: 'Tax')
users_module = SystemModule.find_by(name: 'User')
Rails.logger.debug "\n\nMódulos encontrado ..."

Rails.logger.debug "\n\nBuscando rol de Administrador ..."
rol_admin = Role.find_by(name: 'Administrador')
Rails.logger.debug "\n\nRol encontrado ..."

Rails.logger.debug "\n\nBuscando rol de Super Administrador ..."
rol_super_admin = Role.find_by(name: 'Super Administrador')
Rails.logger.debug "\n\nRol encontrado ..."

Rails.logger.debug "\n\nGenerando los accesos del sistema ..."
AccessRole.create!(role_id: rol_admin.id, access_id: access.id, system_module_id: agents_module.id)
AccessRole.create!(role_id: rol_admin.id, access_id: access.id, system_module_id: clients_module.id)
AccessRole.create!(role_id: rol_admin.id, access_id: access.id, system_module_id: criteria_module.id)
AccessRole.create!(role_id: rol_admin.id, access_id: access.id, system_module_id: middlemen_module.id)
AccessRole.create!(role_id: rol_admin.id, access_id: access.id, system_module_id: containers_module.id)
AccessRole.create!(role_id: rol_admin.id, access_id: access.id, system_module_id: quotes_module.id)
AccessRole.create!(role_id: rol_admin.id, access_id: access.id, system_module_id: documents_module.id)
AccessRole.create!(role_id: rol_admin.id, access_id: access.id, system_module_id: shipments_module.id)
AccessRole.create!(role_id: rol_admin.id, access_id: access.id, system_module_id: taxes_module.id)
AccessRole.create!(role_id: rol_admin.id, access_id: access.id, system_module_id: suppliers_module.id)
AccessRole.create!(role_id: rol_admin.id, access_id: access.id, system_module_id: ports_module.id)
AccessRole.create!(role_id: rol_admin.id, access_id: access.id, system_module_id: roles_module.id)
AccessRole.create!(role_id: rol_admin.id, access_id: access.id, system_module_id: services_module.id)
AccessRole.create!(role_id: rol_admin.id, access_id: access.id, system_module_id: tariffs_module.id)
AccessRole.create!(role_id: rol_admin.id, access_id: access.id, system_module_id: packaging_types_module.id)
AccessRole.create!(role_id: rol_admin.id, access_id: access.id, system_module_id: users_module.id)
Rails.logger.debug "\nAccesos generados ..."

Rails.logger.debug "\n\nGenerando los accesos del sistema para el super Admin ..."
AccessRole.create!(role_id: rol_super_admin.id, access_id: access.id, system_module_id: agents_module.id)
AccessRole.create!(role_id: rol_super_admin.id, access_id: access.id, system_module_id: clients_module.id)
AccessRole.create!(role_id: rol_super_admin.id, access_id: access.id, system_module_id: criteria_module.id)
AccessRole.create!(role_id: rol_super_admin.id, access_id: access.id, system_module_id: middlemen_module.id)
AccessRole.create!(role_id: rol_super_admin.id, access_id: access.id, system_module_id: containers_module.id)
AccessRole.create!(role_id: rol_super_admin.id, access_id: access.id, system_module_id: quotes_module.id)
AccessRole.create!(role_id: rol_super_admin.id, access_id: access.id, system_module_id: documents_module.id)
AccessRole.create!(role_id: rol_super_admin.id, access_id: access.id, system_module_id: shipments_module.id)
AccessRole.create!(role_id: rol_super_admin.id, access_id: access.id, system_module_id: taxes_module.id)
AccessRole.create!(role_id: rol_super_admin.id, access_id: access.id, system_module_id: suppliers_module.id)
AccessRole.create!(role_id: rol_super_admin.id, access_id: access.id, system_module_id: ports_module.id)
AccessRole.create!(role_id: rol_super_admin.id, access_id: access.id, system_module_id: roles_module.id)
AccessRole.create!(role_id: rol_super_admin.id, access_id: access.id, system_module_id: services_module.id)
AccessRole.create!(role_id: rol_super_admin.id, access_id: access.id, system_module_id: tariffs_module.id)
AccessRole.create!(role_id: rol_super_admin.id, access_id: access.id, system_module_id: packaging_types_module.id)
AccessRole.create!(role_id: rol_super_admin.id, access_id: access.id, system_module_id: users_module.id)
Rails.logger.debug "\nAccesos generados ..."


Rails.logger.debug "\n\nBuscando modalidad de servicios..."
maritime = ServiceMode.find_by(code: 'FM')
aduanal = ServiceMode.find_by(code: 'SS')
terrestre = ServiceMode.find_by(code: 'FT')
Rails.logger.debug "\nModalidad de servicios encontrados ..."

# Maritimo
Rails.logger.debug "\n\nGenerando tipo de fletes maritimos..."
FreightType.create!(code: 'FCL', name: 'Full Container Load', service_mode_id: maritime.id)
FreightType.create!(code: 'LCL', name: 'Less than Container Load', service_mode_id: maritime.id)
Rails.logger.debug "\nTipos de fletes maritimos ..."

# Terrestre
Rails.logger.debug "\n\nGenerando tipo de fletes terrestres..."
FreightType.create!(code: 'LTL', name: 'Less Than Truckload', service_mode_id: terrestre.id)
FreightType.create!(code: 'FTL', name: 'Full Truck Load', service_mode_id: terrestre.id)
Rails.logger.debug "\nTipos de fletes terrestres ..."

# Aduanal
Rails.logger.debug "\n\nGenerando tipo de fletes aduanales..."
FreightType.create!(code: 'INT', name: 'Internas', service_mode_id: aduanal.id)
FreightType.create!(code: 'FRONT', name: 'Fronteras', service_mode_id: aduanal.id)
FreightType.create!(code: 'MART', name: 'Maritimas', service_mode_id: aduanal.id)
Rails.logger.debug "\nTipos de fletes aduanales ..."
