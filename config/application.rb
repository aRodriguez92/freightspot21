require_relative 'boot'

require 'rails/all'
require 'csv'
require "action_cable/engine"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Freightspot
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1

    config.generators do |g|
      g.assets false
      g.helper false
      g.stylesheets false
      g.test_framework :rspec
      g.fixture_replacement :factory_bot, dir: 'spec/factories'
    end

    config.active_record.default_timezone = :utc
    config.autoload_paths << Rails.root.join('lib')
    config.i18n.available_locales = %i[es en]
    config.i18n.default_locale = :es
    config.i18n.load_path += Dir[Rails.root.join('config/locales/**/*.{rb,yml}')]
    config.middleware.use WickedPdf::Middleware

    config.active_record.yaml_column_permitted_classes = [BigDecimal, Symbol, Date, Time, ActiveSupport::TimeWithZone, ActiveSupport::TimeZone]

    config.time_zone = 'Mexico City'
    config.to_prepare do
      Devise::SessionsController.layout 'login'
    end
  end
end
