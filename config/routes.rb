Rails.application.routes.draw do
  namespace :customer do
    post 'change_language', to: 'application#change_language'
    get 'shipments/detail'
    get 'favorites/update'
    get 'statistics/count_by_status'
    get 'statistics/count_by_year'
    get 'statistics/count_by_month'
    get 'statistics/count_by_freight_type'
    resources :statistics
    resources :bookings
    resources :billings
    resources :client_products
    resources :products
    resources :quotes do
      member do
        get 'approvals/:approval_id', action: :approvals, as: 'approvals'
        get 'approve/:approval_id', action: :approve, as: 'approve'
        get 'reject/:approval_id', action: :reject, as: 'reject'
      end
      collection do
        get 'search_freight_types/:service_mode', action: :search_freight_types, as: 'search_freight_types'
        get 'set_freight_type/:freight_type', action: :set_freight_type, as: 'set_freight_type'
        get 'search_service_type_by_incoterm/:incoterm_id/:operation_type', action: :search_service_type_by_incoterm, as: 'search_service_type_by_incoterm'
        get 'search_service_type_by_operation_type/:operation_type/:incoterm_id', action: :search_service_type_by_operation_type, as: 'search_service_type_by_operation_type'
        get 'get_port_coordinates/:port_id', action: :get_port_coordinates, as: 'get_port_coordinates'
        get 'get_optional_service/:optional_service_id', action: :get_optional_service, as: 'get_optional_service'
        get  'get_customs_clearance_cost/:customs_clearance', action: :get_customs_clearance_cost, as: 'get_customs_clearance_cost'
        get  'get_destination_ports/:origin_port', action: :get_destination_ports, as: 'get_destination_ports'
        get :tariffs
        patch :update
      end
    end
    resources :maritime_quotes do
      resources :quote_comments
    end
    resources :maritime_fcl_quotes do
      collection do
        get 'search_freight_types/:service_mode', action: :search_freight_types, as: 'search_freight_types'
        get 'set_freight_type/:freight_type', action: :set_freight_type, as: 'set_freight_type'
        get 'search_service_type_by_incoterm/:incoterm_id/:operation_type', action: :search_service_type_by_incoterm, as: 'search_service_type_by_incoterm'
        get 'search_service_type_by_operation_type/:operation_type/:incoterm_id', action: :search_service_type_by_operation_type, as: 'search_service_type_by_operation_type'
        get 'get_port_coordinates/:port_id', action: :get_port_coordinates, as: 'get_port_coordinates'
        get 'get_optional_service/:optional_service_id', action: :get_optional_service, as: 'get_optional_service'

        get :tariffs
      end
    end
    resources :maritime_lcl_quotes
    resources :reports
    resources :maritime_shipments
    resources :profile, only: %i[edit update]
    root 'dashboard#dashboard'
  end

  resources :user_steps
  match "user_steps/client", to: "user_steps#client", via: "post"

  namespace :admin do
    get 'dashboard/changelog'

    resources :changelogs do
      resources :comments
    end
    resources :tariff_rates
    resources :maritime_shipments do
      collection do
        get 'download_sample'
        post 'import'
      end
    end
    resources :bookings
    resources :customs
    resources :teams
    resources :logs do
      member do
        get 'restore'
      end
    end
    resources :charges
    resources :maritime_lcl_rates do
      collection do
        get 'download_sample'
        post 'import'
      end
    end
    resources :maritime_fcl_rates do
      collection do
        get 'download_sample'
        post 'import'
      end
    end
    resources :profile, only: %i[edit update]
    resources :clients do
      collection do
        get 'filter_states/:country_id', action: :filter_states, as: 'filter_states'
      end
      member do
        # get :update_readonly
        get 'update_readonly/:readonly', action: :update_readonly, as: 'update_readonly'

        get :new_address
        get :edit_address
        get :show_address
        delete :destroy_address
        post :add_address
        post :update_address

        get :new_phone
        get :edit_phone
        get :show_phone
        delete :destroy_phone
        post :add_phone
        post :update_phone

        get :new_contact
        get :edit_contact
        get :show_contact
        delete :destroy_contact
        post :add_contact
        post :update_contact

        get :new_sender
        get :edit_sender
        get :show_sender
        delete :destroy_sender
        post :add_sender
        post :update_sender

        get :new_client_product
        get :edit_client_product
        get :show_client_product
        delete :destroy_client_product
        post :add_client_product
        post :update_client_product

        get :new_client_document
        get :edit_client_document
        get :show_client_document
        get :download_client_document
        delete :destroy_client_document
        post :add_client_document
        patch :update_client_document
        put :update_client_document

        get :import_csv_file
      end
    end
    resources :suppliers do
      collection do
        get 'filter_states/:country_id', action: :filter_states, as: 'filter_states'
      end

      member do
        get :new_address
        get :edit_address
        get :show_address
        delete :destroy_address
        post :add_address
        post :update_address

        get :new_phone
        get :edit_phone
        get :show_phone
        delete :destroy_phone
        post :add_phone
        post :update_phone

        get :new_contact
        get :edit_contact
        get :show_contact
        delete :destroy_contact
        post :add_contact
        post :update_contact
        # get :update_readonly
        get 'update_readonly/:readonly', action: :update_readonly, as: 'update_readonly'
      end
    end
    resources :membership_plans
    resources :services
    resources :criteria
    resources :packaging_types
    resources :taxes
    resources :documents
    resources :ports
    resources :products
    resources :access_roles do
      collection do
        get 'add_access/:role_id/:access_id/:system_module_id', action: :add_access, as: 'add_permission'
        get 'remove_access/:role_id/:access_id/:system_module_id', action: :remove_access, as: 'remove_permission'
      end
    end
    resources :roles do
      member do
        get :assign
      end
    end
    resources :users
    resources :containers
    resources :shipments
    resources :quotes do
      member do
        get :authorize
        post :declined
        delete :destroy
      end
    end

    resources :incidents, only: [:create]

    resources :maritime_quotes do
      resources :quote_comments
    end

    get 'icons/icons_fontawesome_brand'
    get 'icons/icons_fontawesome_duotone'
    get 'icons/icons_fontawesome_light'
    get 'icons/icons_fontawesome_regular'
    get 'icons/icons_fontawesome_solid'
    get 'icons/icons_nextgen_base'
    get 'icons/icons_nextgen_general'
    get 'icons/icons_stack_generate'
    get 'icons/icons_stack_showcase'
    get 'icons/icons_webfonts_faq'

    get 'ui/ui_accordion'
    get 'ui/ui_alerts'
    get 'ui/ui_badges'
    get 'ui/ui_breadcrumbs'
    get 'ui/ui_button_group'
    get 'ui/ui_buttons'
    get 'ui/ui_cards'
    get 'ui/ui_carousel'
    get 'ui/ui_collapse'
    get 'ui/ui_dropdowns'
    get 'ui/ui_list_filter'
    get 'ui/ui_modal'
    get 'ui/ui_navbars'
    get 'ui/ui_pagination'
    get 'ui/ui_panels'
    get 'ui/ui_popovers'
    get 'ui/ui_progress_bars'
    get 'ui/ui_scrollspy'
    get 'ui/ui_side_panel'
    get 'ui/ui_spinners'
    get 'ui/ui_tabs_accordions'
    get 'ui/ui_tabs_pills'
    get 'ui/ui_toasts'
    get 'ui/ui_tooltips'
    get 'ui/ui_tooltips_popovers'

    get 'intel/intel_analytics_dashboard'
    get 'intel/intel_introduction'
    get 'intel/intel_marketing_dashboard'
    get 'intel/intel_privacy'

    get 'settings/settings_how_it_works'
    get 'settings/settings_layout_options'
    get 'settings/settings_saving_db'
    get 'settings/settings_skin_options'

    get 'utilities/utilities_borders'
    get 'utilities/utilities_clearfix'
    get 'utilities/utilities_color_pallet'
    get 'utilities/utilities_display_property'
    get 'utilities/utilities_flexbox'
    get 'utilities/utilities_fonts'
    get 'utilities/utilities_helpers'
    get 'utilities/utilities_position'
    get 'utilities/utilities_responsive_grid'
    get 'utilities/utilities_sizing'
    get 'utilities/utilities_spacing'
    get 'utilities/utilities_typography'

    get 'tables/tables_basic'
    get 'tables/tables_generate_style'

    get 'datatables/datatables_alteditor'
    get 'datatables/datatables_autofill'
    get 'datatables/datatables_basic'
    get 'datatables/datatables_buttons'
    get 'datatables/datatables_colreorder'
    get 'datatables/datatables_columnfilter'
    get 'datatables/datatables_export'
    get 'datatables/datatables_fixedcolumns'
    get 'datatables/datatables_fixedheader'
    get 'datatables/datatables_keytable'
    get 'datatables/datatables_responsive'
    get 'datatables/datatables_responsive_alt'
    get 'datatables/datatables_rowgroup'
    get 'datatables/datatables_rowreorder'
    get 'datatables/datatables_scroller'
    get 'datatables/datatables_select'

    get 'statistics/statistics_c3'
    get 'statistics/statistics_chartist'
    get 'statistics/statistics_chartjs'
    get 'statistics/statistics_dygraph'
    get 'statistics/statistics_easypiechart'
    get 'statistics/statistics_flot'
    get 'statistics/statistics_peity'
    get 'statistics/statistics_sparkline'

    get 'plugins/plugins_appcore'
    get 'plugins/plugins_bootbox'
    get 'plugins/plugins_faq'
    get 'plugins/plugins_i18next'
    get 'plugins/plugins_navigation'
    get 'plugins/plugins_pacejs'
    get 'plugins/plugins_slimscroll'
    get 'plugins/plugins_smartpanels'
    get 'plugins/plugins_throttle'
    get 'plugins/plugins_waves'

    get 'page/page_chat'
    get 'page/page_confirmation'
    get 'page/page_contacts'
    get 'page/page_error'
    get 'page/page_error_404'
    get 'page/page_error_announced'
    get 'page/page_forget'
    get 'page/page_forum_discussion'
    get 'page/page_forum_list'
    get 'page/page_forum_threads'
    get 'page/page_inbox_general'
    get 'page/page_inbox_read'
    get 'page/page_inbox_write'
    get 'page/page_invoice'
    get 'page/page_locked'
    get 'page/page_login'
    get 'page/page_login_alt'
    get 'page/page_profile'
    get 'page/page_projects'
    get 'page/page_register'
    get 'page/page_search'

    get 'form/form_basic_inputs'
    get 'form/form_checkbox_radio'
    get 'form/form_input_groups'
    get 'form/form_validation'

    get 'form_plugins/form_plugins_colorpicker'
    get 'form_plugins/form_plugins_datepicker'
    get 'form_plugins/form_plugins_daterange_picker'
    get 'form_plugins/form_plugins_dropzone'
    get 'form_plugins/form_plugins_imagecropper'
    get 'form_plugins/form_plugins_inputmask'
    get 'form_plugins/form_plugins_ionrangeslider'
    get 'form_plugins/form_plugins_markdown'
    get 'form_plugins/form_plugins_nouislider'
    get 'form_plugins/form_plugins_select2'
    get 'form_plugins/form_plugins_summernote'
    get 'form_plugins/form_plugins_wizard'

    get 'miscellaneous/miscellaneous_fullcalendar'
    get 'miscellaneous/miscellaneous_lightgallery'
    get 'miscellaneous/miscellaneous_treeview'
    get 'notifications/notifications_sweetalert2'
    get 'notifications/notifications_toastr'

    post 'change_language', to: 'application#change_language'
    root to: 'dashboard#index'
  end

  mount LetterOpenerWeb::Engine, at: '/letter_opener' if Rails.env.development?

  devise_for :users, controllers: { sessions: 'users/sessions', registrations: 'users/registrations' }

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :quote_contact_emails, only: [:create]
  post 'welcome/contact_us'
  post 'welcome/request_quote'
  post 'welcome/quote'
  get 'welcome/get_destination_ports/:origin_id', to: 'welcome#get_destination_ports', as: 'welcome_get_destination_ports'

  get 'intel/intel_analytics_dashboard'
  get 'intel/intel_introduction'
  get 'intel/intel_marketing_dashboard'
  get 'intel/intel_privacy'

  post 'requests/quote'
  get 'welcome/blank'

  resources :maritime_quotes do
    collection do
      get 'optional_service/:optional_service_id', action: :optional_service, as: 'optional_service'
    end
  end

  resources :welcome do
    collection do
      get 'optional_services/:rate_id', action: :optional_services, as: 'optional_services'
      get :search_rates
      get :preview_quote
    end
  end
  root 'welcome#index'
end
