source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.0.6'

gem 'acts-as-taggable-on', '~> 8.1' # A tagging plugin for Rails applications that allows for custom tagging along dynamic contexts
gem 'apexcharts'
gem 'audited'
gem 'aws-sdk-s3'
gem 'bootsnap', '>= 1.4.2', require: false # Boot large ruby/rails apps faster
gem 'cable_ready', '~> 4.5'
gem 'cancancan'
gem 'caxlsx_rails'
gem 'chartkick'
gem 'cocoon'
gem 'coffee-rails'
gem 'devise', '~> 4.8' # Flexible authentication solution for Rails
gem 'devise_invitable', '~> 2.0.5' # Allows invitations to be sent for joining
gem 'devise-security'
gem 'enum_help'
gem 'flag-icons-rails'
gem 'friendly_id'
gem 'google-cloud-translate'
gem 'groupdate'
gem 'i18n'
gem 'i18n-tasks', require: false
gem 'image_processing'
gem 'intl-tel-input-rails'
gem 'jbuilder', '~> 2.11' # Create JSON structures via a Builder-style DSL
gem 'local_time'
gem 'mapbox-gl-rails'
gem 'mapbox-rails'
gem 'mapbox-sdk'
gem 'mini_magick'
gem 'money'
gem 'name_of_person'
gem 'noticed'
gem 'paper_trail'
gem 'paranoia'
gem 'pg', '1.2.0' # Pg is the Ruby interface to the PostgreSQL RDBMS
gem 'puma', '~> 5.5.2' # Puma is a simple, fast, threaded, and highly concurrent HTTP 1.1 server
gem 'rails', '6.1.7.4' # Ruby on Rails
gem 'rails-i18n'
gem 'redis', '~> 4.4.0' # Redis ruby client
gem 'roo'
gem 'roo-xls'
gem 'sass-rails', '>= 6'
gem 'simple_form'
gem 'slim-rails'
gem 'stripe', '~> 5.39' # Ruby library for the Stripe API
gem 'toastr-rails'
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
gem 'valid_email2'
gem 'webpacker', '~> 4.0'
gem 'wicked'
gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'

group :development do
  gem 'better_errors', '~> 2.9' # Provides a better error page for Rails and other Rack apps
  gem 'binding_of_caller'
  gem 'brakeman', '~> 5.1', require: false # Brakeman detects security vulnerabilities in Ruby on Rails applications via static analysis
  gem 'bundler-audit', '~> 0.9' # bundler-audit provides patch-level verification for Bundled apps
  # gem 'guard-livereload', '~> 2.5', require: false # Guard::LiveReload automatically reloads your browser when 'view' files are modified
  # gem 'i18n-tasks', '~> 0.9.34' # Helpers to find and manage missing and unused translations
  gem 'letter_opener_web'
  gem 'listen', '~> 3.7', require: false # Helps 'listen' to file system modifications events (also used by other gems like guard)
  gem 'rack-mini-profiler'
  gem 'spring'
  gem 'web-console', '~> 4.1' # Rails Console on the Browser
end

group :development, :test do
  gem 'annotate'
  gem 'bullet', '~> 6.1' # help to kill N+1 queries and unused eager loading
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'capybara', '~> 3.35.3' # Capybara is an integration testing tool for rack based web applications
  gem 'database_cleaner'
  gem 'faker', '~> 2.19' # A library for generating fake data such as names, addresses, and phone numbers
  gem 'rails-controller-testing'
  gem 'rspec-rails', '~> 5.0' # rspec-rails is a testing framework for Rails 3+
  gem 'rubocop', '~> 1.22', require: false # Automatic Ruby code style checking tool
  gem 'rubocop-gitlab-security'
  gem 'rubocop-performance', '~> 1.11', require: false # A collection of RuboCop cops to check for performance optimizations in Ruby code
  gem 'rubocop-rails', '~> 2.12', require: false # Automatic Rails code style checking tool
  gem 'rubocop-rspec', '~> 2.5', require: false # Code style checking for RSpec files
end

group :test do
  # factory_bot is a fixtures replacement with a straightforward definition syntax, support for multiple build strategies
  gem 'factory_bot_rails', '~> 6.2'
  gem 'fuubar'
  gem 'selenium-webdriver'
  gem 'shoulda-matchers', '~> 5.0.0' # Simple one-liner tests for common Rails functionality
  # Code coverage with a powerful configuration library and automatic merging of coverage across test suites
  gem 'simplecov', '~> 0.21.2', require: false
  gem 'webdrivers', '~> 4.6' # Run Selenium tests more easily with install and updates for all supported webdrivers
end
